<?php
	$url_redirect="https://autenticar.sanjuan.gob.ar/?callback=ukrl1gWrDigzKWwmrIGjbnuvF4C6iGCGhHqX69SSSZxafWa6o8hyqYjaosY8MtATbZBYYXj9BhyPZPnAMf384AUx4dqKNOBJiJYXNIspZdxQJ1rPuVnIf1bBMA4h3mIokHKNMcm7gqtK3%2F9Fq1FEVQhOrLsM2ndPk%2F4gavvTM8E%3D";
	/*if((!isset($_GET["sessionToken"])) || (empty($_GET["sessionToken"]))) {
		// Hago el redirect al login
		header("HTTP/1.1 302 Moved Temporarily");
		header("Location: $url_redirect"); 
		exit();
	}*/
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Ministerio de Salud - Parte Diario de Consultas e Internación</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link type="text/css" rel="stylesheet" href="../css/custom-bootstrap.min.css">
    
    <link type="text/css" rel="stylesheet" href="../css/patron.css">
    <link type="text/css" rel="stylesheet" href="../css/patron-iconos.css">
    <link rel="stylesheet" type="text/css" href="../css/salud/estilo_salud.css" />
    <link href="../bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="../https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="../img/favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Parte Diario de Consultas e Internación - Ministerio de Salud de San Juan">
    <meta name="keywords" content="salud, parte diario, consultas, internaciones, gobierno, san juan">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js" type="text/javascript" charset="utf-8" async defer></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" 
    	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    <!-- select2 -->
    <link href="../select2/css/select2.min.css" rel="stylesheet" />
    <link href="../select2/css/select2-bootstrap4.min.css" rel="stylesheet" />
	<script src="../select2/js/select2.min.js"></script>
	<script src="../select2/js/i18n/es.js"></script>
	<script src="../js/notify.js"></script>

	<style type="text/css" media="screen">
		.select2-container{
			width: 100%!important;
		}
		.select2-search--dropdown .select2-search__field {
			width: 98%;
		}
		.icant {
			text-align: center;
			font-weight: bold;
			font-size:14px;
		}
	</style>
	<script src="js/jwt-decode.js"></script>
</head>
<body>

	<header>
	    <nav class="navbar navbar-light navbar-expand-md fixed-top">
	        <div class="container">
	            <a href="http://www.sanjuan.gob.ar" class="navbar-brand padre-img1">
	                <img src="../img/logo.png" alt="Isologo Gobierno de San Juan" width="150">
	            </a>
	            <a href="http://salud.sanjuan.gob.ar" class="navbar-brand padre-img2">
	                <img src="../img/logosalud.png" alt="Ministerio de Salud" width="150" class="img2">
	            </a>
	            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#menu-principal"
	                aria-controls="menu-principal" aria-expanded="false" aria-label="Desplegar menú de navegación">
	                <span class="navbar-toggler-icon"></span>
	            </button>
	            <div class="collapse navbar-collapse" id="menu-principal">
	                <ul class="navbar-nav ml-auto">  
	                    <!--<li class="nav-item"><a href="#" class="nav-link active">Inicio</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 1</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 2</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 3</a></li>
	                    -->
	                </ul>
	            </div>
	        </div>
	    </nav>
	</header>
	<main role="main" style="display: block;width: 100%;">
	    <section class="jumbotron jumbotron-home text-light text-center" 
	    		style="background-image:linear-gradient(rgba(89,19,28,0.9),rgba(0,0,0,0)),url('../img/doctor.jpg')">
	        <div class="container">
	            <nav class="row">
	                <div class="col-12 col-md-8 mx-auto"><!--col-md-8 m-auto-->
	                    <h3 class="pb-2">Parte diario de Consultas e Internación</h3>
	                </div>
	            </nav>
	        </div>
	    </section>
	</main> 
	<div class="container-fluid" id="sy-contenido">
        <section class="container">
        	<br>
			<div class="alert alert-secondary mb-4" role="alert">
				Bienvenido: <strong><?php echo $_GET["cidNombre"]; ?></strong>
			</div>
			<h4 class="text-center">Parte Registrado</h4>
			<p class="mb-4">&nbsp;</p>
			<form name="formparte" id="formparte" method="post" action="cargar_d.php">
				<input type="hidden" id="users_cd" name="users_cd" value="0" />
				
				<?php include('views/views_form_uti_adultos.php');?>
				<?php include('views/views_form_uti_pediatrico.php');?>
				<?php include('views/views_form_consultas_dia_informado.php');?>
				<?php include('views/views_form_internados.php');?>
				<div class="form-row">
					<div class="form-group col-md-12">
						<button type="submit" id="button_cargar" class="btn btn-primary">Cargar Información</button>
					</div>						
				</div>

				<div class="form-row">
					<p>&nbsp;</p>
				</div>
			</form>
		</section>
	</div>
	</body>
<?php include('views/footer.php');?>
<script type="text/javascript">
	traer_parte(191);
</script>
<?php //include('token_CD.php');?>