<?php
	//este archivo es para el formulario de derivaciones de pacientes
	// Conecta a PostgreSQL
	require 'classPgSql.php';
	$pg = new PgSql();
	
	$sql = "SELECT id, cod_establecimiento, nombre 
			FROM partediario.establecimientos
			WHERE mostrar = '1'";
	$json = [];
	foreach($pg->getRows($sql) as $row) 
	{
		$cod_establecimiento =str_replace(' ', '', $row->cod_establecimiento);
		$json[] = ['cod_establecimiento'=>$cod_establecimiento, 'nombre'=>$row->nombre];

		
	}
	echo json_encode($json);
?>