

<!-- Modal -->
<div class="modal fade bd-example-modal-xl"  id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalScrollableTitle">Partes Enviados a Nacion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
            
        <div class="row" id="div_job_registrados">
          <div class="col-lg-3">
            <table class="table table-striped">
                  <thead class="thead-dark">
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Fecha y Hora</th>
                      <th scope="col">Acciones</th>
                    </tr>
                  </thead>
                  <tbody id="tbody_job">
                  </tbody>
            </table>

          </div>
          
        </div>
        <div class="row">
          
          <div class="col-lg-12" id="div_job_x_id_detalle">
            <table class="table table-striped">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">Centro de Salud</th>
                          <th scope="col">UTI PEDIATRIA</th>
                          <th scope="col">UTI ADULTO</th>
                        </tr>
                      </thead>
                      <tbody id="table_job_x_id_detalle">
                      </tbody>
            </table>
            <button onclick="ver_all_job()" class="btn btn-secondary" >Volver</button>
          </div>
          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button"  class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      ...
    </div>
  </div>
</div>