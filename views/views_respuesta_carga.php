<!DOCTYPE html>
<html lang="es">
<head>
    <title>Ministerio de Salud - Parte Diario de Consultas e Internación</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link type="text/css" rel="stylesheet" href="css/custom-bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/patron.css">
    <link type="text/css" rel="stylesheet" href="css/patron-iconos.css">
    <link rel="stylesheet" type="text/css" href="css/salud/estilo_salud.css" />
    <link href="bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Parte Diario de Consultas e Internación - Ministerio de Salud de San Juan">
    <meta name="keywords" content="salud, parte diario, consultas, internaciones, gobierno, san juan">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js" type="text/javascript" charset="utf-8" async defer></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" 
    	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    <!-- select2 -->
    <link href="select2/css/select2.min.css" rel="stylesheet" />
    <link href="select2/css/select2-bootstrap4.min.css" rel="stylesheet" />
	<script src="select2/js/select2.min.js"></script>
	<script src="select2/js/i18n/es.js"></script>

	<style type="text/css" media="screen">
		.select2-container{
			width: 100%!important;
		}
		.select2-search--dropdown .select2-search__field {
			width: 98%;
		}
		.icant {
			text-align: center;
			font-weight: bold;
			font-size:14px;
		}
	</style>
</head>

<body>

	<header>
	    <nav class="navbar navbar-light navbar-expand-md fixed-top">
	        <div class="container">
	            <a href="http://www.sanjuan.gob.ar" class="navbar-brand padre-img1">
	                <img src="img/logo.png" alt="Isologo Gobierno de San Juan" width="150">
	            </a>
	            <a href="http://salud.sanjuan.gob.ar" class="navbar-brand padre-img2">
	                <img src="img/logosalud.png" alt="Ministerio de Salud" width="150" class="img2">
	            </a>
	            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#menu-principal"
	                aria-controls="menu-principal" aria-expanded="false" aria-label="Desplegar menú de navegación">
	                <span class="navbar-toggler-icon"></span>
	            </button>
	            <div class="collapse navbar-collapse" id="menu-principal">
	                <ul class="navbar-nav ml-auto">  
	                    <!--<li class="nav-item"><a href="#" class="nav-link active">Inicio</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 1</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 2</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 3</a></li>
	                    -->
	                </ul>
	            </div>
	        </div>
	    </nav>
	</header>
	<main role="main" style="display: block;width: 100%;">
	    <section class="jumbotron jumbotron-home text-light text-center" 
	    		style="background-image:linear-gradient(rgba(89,19,28,0.9),rgba(0,0,0,0)),url('img/doctor.jpg')">
	        <div class="container">
	            <nav class="row">
	                <div class="col-12 col-md-8 mx-auto"><!--col-md-8 m-auto-->
	                    <h3 class="pb-2">Parte diario de Consultas e Internación</h3>
	                </div>
	            </nav>
	        </div>
	    </section>
	</main>    

    <div class="container-fluid" id="sy-contenido">

        <section class="container">

        	<br>
			<h4 class="text-center">Datos del Parte Cargados</h4>
			<p class="mb-2">&nbsp;</p>

			<div class="row mb-4">
				<div class="col-2">
					&nbsp;				  	
				</div>
				<div class="col-8">					
					<a href="Javascript:void(0);" rel="btn" onclick="location.href='https://covid19salud.sanjuan.gob.ar';" 
					class="btn btn-block btn-primary">Cargar Datos de Otro Parte</a>
				</div>
				<div class="col-2">
					&nbsp;
				</div>
			</div>
			
			<div class="row">
				<div class="col-2">
					&nbsp;				  	
				</div>
				<div class="col-8">
					<div class="alert alert-info my-0 mx-0 py-4 px-4 text-center" style="font-size:14px;" role="alert">				
						<strong><?php echo $mensaje; ?></strong>
					</div>
				</div>
				<div class="col-2">
					&nbsp;				  	
				</div>
			</div>			

			<div class="mt-5">
				<?php 
					/*
					echo "<pre>";
					print_r($_POST);
					echo "</pre>";
					*/
				?>				
			</div>

		</section>

    </div>

    <footer class="container-fluid bg-primary py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md">
                    <p>
                        <a href="http://salud.sanjuan.gob.ar" target="_self" title="Ministerio de Salud">
                            <img src="img/logosalud.png" alt="Gobierno de San Juan" width="100%">
                        </a>
                    </p>
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <ul class="list-unstyled text-small">
                                    <li class="titulosclaros corregir p-0">
                                    	Dirección: – Av. Libertador Gral. San Martín 750 Oeste - Capital.
                                	</li>
                                    <li class="titulosclaros corregir p-0">
                                    	Centro Cívico, 3° Piso, Núcleo 1.
                                    </li>
                                    <li class="titulosclaros corregir p-0 w3-padding-top">
                                    	<i class="fa fa-phone fa-1x"></i> Conmutador: 
                                    	<a href="tel:2644305000">264 430 5000</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-phone fa-1x"></i> Teléfono: 
                                        <a href="tel:2644306125">264 430 7290</a>.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md">
                    <h5>Teléfonos Útiles</h5>
                    <ul class="list-unstyled text-small">
                        <li>
                            <a href="tel:911">
                                &nbsp;<i class="fa fa-phone fa-1x"></i> <strong>911</strong>&nbsp;
                                Emergencias
                            </a>
                        </li>
                        <li>
                            <a href="tel:101">
                                &nbsp;<i class="fa fa-phone fa-1x"></i> <strong>101</strong>&nbsp;
                                Policía
                            </a>
                        </li>
                        <li>
                            <a href="tel:107">
                                &nbsp;<i class="fa fa-phone fa-1x"></i> <strong>107</strong>&nbsp;
                                Emergencias Médicas
                            </a>
                        </li>
                        <a href="tel:100">
                            &nbsp;<i class="fa fa-phone fa-1x"></i> <strong>100</strong>&nbsp;
                            Bomberos
                        </a>
                    </ul>
                </div>
                <div class="col-12 col-md">
                    <h5>Trámites y servicios</h5>
                    <ul class="list-unstyled text-small">
                        <li><a href="https://webmail.sanjuan.gov.ar/owa">Acceso a Webmail </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- ARCHIVOS BOOTSTRAP JAVASCRIPT -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>

    <script type="text/javascript">
    	jQuery(document).ready(function($) {
    		
    	});
    </script>
</body>
</html>