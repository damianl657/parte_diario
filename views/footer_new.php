<!-- ARCHIVOS BOOTSTRAP JAVASCRIPT -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js" type="text/javascript" charset="utf-8"></script>
<script src="moment/moment.js"></script>
<script src="bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="bootstrap-datepicker/bootstrap-datepicker.es.min.js"></script>   
<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.1/js.cookie.min.js" integrity="sha256-oE03O+I6Pzff4fiMqwEGHbdfcW7a3GRRxlL+U49L5sA=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

<script type="text/javascript">
		var cookieName = "sjcamasrespiradores19";
        var minutos = 30;
		paramUrl = getParameterByName("sessionToken");
        cookieVal = Cookies.get(cookieName);
        if ((cookieVal == null) && (paramUrl == "")) {
        }
        else {
            if (paramUrl != "") {
                tk = parseJwt(paramUrl);
                ciudadano = JSON.parse(tk.data);
                $("#cidNombre").html(ciudadano.nombre_apellido);
                $("#campousuario").val(ciudadano.nombre_apellido);
                $("#divLogin").hide();
                $("#divForm").show();
                if (cookieVal == null) {
                    Cookies.set(cookieName, paramUrl, {
                        expires: minutos
                    });                    
                }
                window.history.replaceState(null, null, window.location.pathname);
            }
            else {
                if (cookieVal != null) {
                    tk = parseJwt(cookieVal);
                    ciudadano = JSON.parse(tk.data);
                    $("#cidNombre").html(ciudadano.nombre_apellido);
                    $("#campousuario").val(ciudadano.nombre_apellido);
                    $("#divLogin").hide();
                    $("#divForm").show();
                }
            }
        }

        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return (results === null) ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        function parseJwt(token) {
            var base64Url = token.split(".")[1];
            var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
            var jsonPayload = decodeURIComponent(
                atob(base64)
                .split("")
                .map(function (c) {
                    return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
                })
                .join("")
                );
            return JSON.parse(jsonPayload);
        }

        setTimeout(function () {
            $.confirm({
                title: '¡Atención!',
                content: 'Su sesión ha caducado. Debe volver a ingresar en el sistema.',
                type: 'blue',
                columnClass: 'col-lg-6',
                typeAnimated: true,
                buttons: {
                    bCerrar: {
                        text: 'Aceptar', // text for button
                        btnClass: 'btn-info', // class for the button
                        isHidden: false, // initially not hidden
                        isDisabled: false, // initially not disabled
                        action: function(bCerrarBtn){
                        }
                    },
                }
            });
            salirSesion();
        }, minutos * 60 * 1000);

        function salirSesion() {
            Cookies.remove(cookieName);
            cleanHistorial();
            $("#bAcceder").trigger("click");
        }

        function cleanHistorial() {
            history.pushState({ data: true }, "Titulo", "index.html");
        }
        $("#bAcceder").click(function(event) {
                $("#bAcceder").attr('disabled', 'disabled');
                var urlAT = "https://serviciosweb.sanjuan.gob.ar/camasyrespiradores/at.php";
                var reqAT = $.ajax({
                    url: urlAT,
                    beforeSend: function(){
                    },
                    type: "GET",
                    dataType: "json"
                });
                reqAT.done(function(data) {
                    console.log(encodeURIComponent(data.sessionToken));                    
                    $("#bAcceder").removeAttr('disabled');
                    if (data.resultadoWS.exito == true) {
                        console.log(data.sessionToken);
                        window.location.href = 'https://autenticar3.sanjuan.gob.ar?afCallbackUrl='+encodeURIComponent(data.sessionToken);
                        return true;
                    }
                    else {
                        $("#bAcceder").removeAttr('disabled');
                    }
                });
                reqAT.fail(function( jqXHR, textStatus ) {
                    console.log(textStatus);
                    $.confirm({
                        title: '¡Error!',
                        content: 'Error al obtener Token. Por favor consulte con el Administrador',
                        type: 'red',
                        columnClass: 'col-lg-6',
                        typeAnimated: true,
                        buttons: {
                            bCerrar: {
                                text: 'Cerrar', // text for button
                                btnClass: 'btn-primary', // class for the button
                                isHidden: false, // initially not hidden
                                isDisabled: false, // initially not disabled
                                action: function(bCerrarBtn){
                                }
                            },
                        }
                    });
                    $("#bAcceder").removeAttr('disabled');
                    return false;
                });
            });
        
        /* ====================================== JQREADY =========== */
        /* ====================================== JQREADY =========== */
        /* ====================================== JQREADY =========== */        
        
        jQuery(document).ready(function($) {
            $("#btnSalir").click(function () {
                salirSesion();
            });
        });
</script>
<?php include('views/funciones.php');?>