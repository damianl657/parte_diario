<!-- FORM UTI PEDIATRIA -->
<div class="card " id="form_uti_pediatrico">
	<div class="card-header bg-success text-white"><strong><center>SERVICIO DE UTI PEDIATR&Iacute;A</center></strong>
	</div>
	<div class="card-body">
		<div class="row">
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Disponibilidad de camas en UTI Pediatr&iacute;a: <small class="text-muted form-text">Cantitad total de camas en UTI</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="uti_allocated_children" id="uti_allocated_children">
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de internados total en UTI Pediatr&iacute;a:</label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="uti_hospitalized_children_count" id="uti_hospitalized_children_count">
						</div>
					</div>
				</div>
		</div>
		<hr>
		<div class="row">
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de egresos en UTI Pediatr&iacute;a por alta médica: <small class="text-muted form-text">Desde el último reporte</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="uti_discharged_children_count" id="uti_discharged_children_count" placeholder="">
						</div>	
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de egresos en UTI Pediatr&iacute;a derivados a otras instituciones:<small class="text-muted form-text">Desde el último reporte</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="uti_discharged_derivative_children_count" id="uti_discharged_derivative_children_count">
						</div>
					</div>
				</div>
		</div>
		<hr>
		<div class="row">
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de egresos en UTI Pediatr&iacute;a por fallecimiento: <small class="text-muted form-text">Desde el último reporte</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="uti_discharged_dead_children_count" id="uti_discharged_dead_children_count">
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Disponibilidad de camas con gases para Pediatr&iacute;a: <small class="text-muted form-text">Cantidad total de camas con gases para Pediatr&iacute;a</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="uti_allocated_children_gas" id="uti_allocated_children_gas">
						</div>	
					</div>
				</div>
		</div>
		<div class="row">
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de camas con gases ocupadas para Pediatr&iacute;a: <small class="text-muted form-text">Cantidad total de camas con gases en uso de pacientes Pediatr&iacute;a</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="uti_gas_unavailable_children_count" id="uti_gas_unavailable_children_count">
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de camas con gases liberadas para Pediatr&iacute;a: <small class="text-muted form-text">Desde el último reporte</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="uti_gas_available_children_count" id="uti_gas_available_children_count">
						</div>	
					</div>
				</div>
		</div>
		<hr>
		<div class="row">
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Disponibilidad de respiradores para Pediatr&iacute;a: <small class="text-muted form-text">Cantidad total de respiradores para Pediatr&iacute;a</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="respirators_allocated_children" id="respirators_allocated_children">
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de respiradores ocupados para Pediatr&iacute;a: <small class="text-muted form-text">Cantidad total de respiradores en uso de pacientes Pediatr&iacute;a</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="respirators_unavailable_children_count" id="respirators_unavailable_children_count">
						</div>	
					</div>
				</div>
		</div>
		<div class="row">
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de respiradores liberados para Pediatr&iacute;a: <small class="text-muted form-text">Desde el último reporte</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="respirators_available_children_count" id="respirators_available_children_count">
						</div>
					</div>
				</div>
				
		</div>
	</div>
</div>

						
							