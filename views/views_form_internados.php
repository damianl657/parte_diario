<div class="form-row">
					<div class="form-group col-md-12">
						<div class="alert alert-info mt-3" role="alert">Cantidad de pacientes internados por enfermedades respiratorias según diagnóstico por grupo etario</div>
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-sm table-striped">
								<thead>
									<tr>
										<th width="200">Diagnóstico de Internación (Enfermedades respiratorias)</th>
										<th class="text-center">menores de 1 año</th>
										<th class="text-center">1 a 4 años</th>
										<th class="text-center">5 a 14 años</th>
										<th class="text-center">15 a 19 años</th>
										<th class="text-center">20 a 39 años</th>
										<th class="text-center">40 a 64 años</th>
										<th class="text-center">65 y mas años</th>
										<th class="text-center">total</th>

									</tr>
								</thead>
								<tbody>
									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf1" id="selenf1">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf1 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf1_1" id="enf1_1">
										</td>
										<td>
											<input type="text" class="icant sumenf1 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf1_14" id="enf1_14">
										</td>
										<td>
											<input type="text" class="icant sumenf1 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf1_514" id="enf1_514">
										</td>
										<td>
											<input type="text" class="icant sumenf1 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf1_1519" id="enf1_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf1 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf1_2039" id="enf1_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf1 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf1_4064" id="enf1_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf1 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf1_65" id="enf1_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf1hid" id="totenf1hid" value="0">
												<strong><span id="totenf1">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf2" id="selenf2">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf2 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf2_1" id="enf2_1">
										</td>
										<td>
											<input type="text" class="icant sumenf2 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf2_14" id="enf2_14">
										</td>
										<td>
											<input type="text" class="icant sumenf2 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf2_514" id="enf2_514">
										</td>
										<td>
											<input type="text" class="icant sumenf2 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf2_1519" id="enf2_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf2 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf2_2039" id="enf2_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf2 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf2_4064" id="enf2_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf2 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf2_65" id="enf2_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf2hid" id="totenf2hid" value="0">
												<strong><span id="totenf2">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf3" id="selenf3">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf3 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf3_1" id="enf3_1">
										</td>
										<td>
											<input type="text" class="icant sumenf3 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf3_14" id="enf3_14">
										</td>
										<td>
											<input type="text" class="icant sumenf3 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf3_514" id="enf3_514">
										</td>
										<td>
											<input type="text" class="icant sumenf3 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf3_1519" id="enf3_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf3 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf3_2039" id="enf3_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf3 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf3_4064" id="enf3_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf3 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf3_65" id="enf3_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf3hid" id="totenf3hid" value="0">
												<strong><span id="totenf3">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf4" id="selenf4">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf4 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf4_1" id="enf4_1">
										</td>
										<td>
											<input type="text" class="icant sumenf4 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf4_14" id="enf4_14">
										</td>
										<td>
											<input type="text" class="icant sumenf4 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf4_514" id="enf4_514">
										</td>
										<td>
											<input type="text" class="icant sumenf4 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf4_1519" id="enf4_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf4 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf4_2039" id="enf4_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf4 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf4_4064" id="enf4_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf4 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf4_65" id="enf4_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf4hid" id="totenf4hid" value="0">
												<strong><span id="totenf4">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf5" id="selenf5">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf5 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf5_1" id="enf5_1">
										</td>
										<td>
											<input type="text" class="icant sumenf5 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf5_14" id="enf5_14">
										</td>
										<td>
											<input type="text" class="icant sumenf5 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf5_514" id="enf5_514">
										</td>
										<td>
											<input type="text" class="icant sumenf5 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf5_1519" id="enf5_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf5 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf5_2039" id="enf5_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf5 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf5_4064" id="enf5_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf5 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf5_65" id="enf5_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf5hid" id="totenf5hid" value="0">
												<strong><span id="totenf5">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf6" id="selenf6">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf6 sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf6_1" id="enf6_1">
										</td>
										<td>
											<input type="text" class="icant sumenf6 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf6_14" id="enf6_14">
										</td>
										<td>
											<input type="text" class="icant sumenf6 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf6_514" id="enf6_514">
										</td>
										<td>
											<input type="text" class="icant sumenf6 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf6_1519" id="enf6_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf6 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf6_2039" id="enf6_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf6 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf6_4064" id="enf6_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf6 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf6_65" id="enf6_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf6hid" id="totenf6hid" value="0">
												<strong><span id="totenf6">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf7" id="selenf7">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf7_1" id="enf7_1">
										</td>
										<td>
											<input type="text" class="icant sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf7_14" id="enf7_14">
										</td>
										<td>
											<input type="text" class="icant sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf7_514" id="enf7_514">
										</td>
										<td>
											<input type="text" class="icant sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf7_1519" id="enf7_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf7_2039" id="enf7_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf7_4064" id="enf7_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf7_65" id="enf7_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf7hid" id="totenf7hid" value="0">
												<strong><span id="totenf7">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf8" id="selenf8">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf8 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf8_1" id="enf8_1">
										</td>
										<td>
											<input type="text" class="icant sumenf8 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf8_14" id="enf8_14">
										</td>
										<td>
											<input type="text" class="icant sumenf8 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf8_514" id="enf8_514">
										</td>
										<td>
											<input type="text" class="icant sumenf8 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf8_1519" id="enf8_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf8 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf8_2039" id="enf8_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf8 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf8_4064" id="enf8_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf8 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf8_65" id="enf8_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf8hid" id="totenf8hid" value="0">
												<strong><span id="totenf8">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf9" id="selenf9">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf9 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf9_1" id="enf9_1">
										</td>
										<td>
											<input type="text" class="icant sumenf9 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf9_14" id="enf9_14">
										</td>
										<td>
											<input type="text" class="icant sumenf9 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf9_514" id="enf9_514">
										</td>
										<td>
											<input type="text" class="icant sumenf9 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf9_1519" id="enf9_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf9 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf9_2039" id="enf9_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf9 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf9_4064" id="enf9_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf9 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf9_65" id="enf9_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf9hid" id="totenf9hid" value="0">
												<strong><span id="totenf9">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf10" id="selenf10">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf10 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf10_1" id="enf10_1">
										</td>
										<td>
											<input type="text" class="icant sumenf10 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf10_14" id="enf10_14">
										</td>
										<td>
											<input type="text" class="icant sumenf10 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf10_514" id="enf10_514">
										</td>
										<td>
											<input type="text" class="icant sumenf10 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf10_1519" id="enf10_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf10 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf10_2039" id="enf10_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf10 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf10_4064" id="enf10_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf10 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf10_65" id="enf10_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf10hid" id="totenf10hid" value="0">
												<strong><span id="totenf10">0</span></strong>
											</div>
										</td>
									</tr>


									<tr>
										<td>Total</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totint1hid" id="totint1hid" value="0">
												<strong><span id="totint1">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totint14hid" id="totint14hid" value="0">
												<strong><span id="totint14">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totint514hid" id="totint514hid" value="0">
												<strong><span id="totint514">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totint1519hid" id="totint1519hid" value="0">
												<strong><span id="totint1519">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totint2039hid" id="totint2039hid" value="0">
												<strong><span id="totint2039">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totint4064hid" id="totint4064hid" value="0">
												<strong><span id="totint4064">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totint65hid" id="totint65hid" value="0">
												<strong><span id="totint65">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totinternadoshid" id="totinternadoshid" value="0">
												<strong><span id="totinternados">0</span></strong>
											</div>
										</td>
									</tr>
								</tbody>
							</table>							
						</div>						
					</div>
				</div>