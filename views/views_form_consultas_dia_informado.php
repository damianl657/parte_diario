<div class="form-row">
					<div class="form-group col-md-12">
						<div class="alert alert-info mt-3" role="alert">Cantidad de consultas del día informado</div>
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-sm table-striped">
								<thead>
									<tr>
										<th width="120">Servicio donde se realizó la consulta</th>
										<th class="text-center">menores de 1 año</th>
										<th class="text-center">1 a 4 años</th>
										<th class="text-center">5 a 14 años</th>
										<th class="text-center">15 a 19 años</th>
										<th class="text-center">20 a 39 años</th>
										<th class="text-center">40 a 64 años</th>
										<th class="text-center">65 y mas años</th>
										<th class="text-center">total</th>

									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Pediatría</td>
										<td>
											<input type="text" class="icant sumped form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="ped1" id="ped1">
										</td>
										<td>
											<input type="text" class="icant sumped form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="ped14" id="ped14">
										</td>
										<td>
											<input type="text" class="icant sumped form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="ped514" id="ped514">
										</td>
										<td>
											<input type="text" class="icant sumped form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="ped1519" id="ped1519">
										</td>
										<td>
											<input type="text" class="icant sumped form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="ped2039" id="ped2039">
										</td>
										<td>
											<input type="text" class="icant sumped form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="ped4064" id="ped4064">
										</td>
										<td>
											<input type="text" class="icant sumped form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="ped65" id="ped65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totpedhid" id="totpedhid" value="0">
												<strong><span id="totped">0</span></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td>Clínica Médica</td>
										<td>
											<input type="text" class="icant sumcm form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="cm1" id="cm1">
										</td>
										<td>
											<input type="text" class="icant sumcm form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="cm14" id="cm14">
										</td>
										<td>
											<input type="text" class="icant sumcm form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="cm514" id="cm514">
										</td>
										<td>
											<input type="text" class="icant sumcm form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="cm1519" id="cm1519">
										</td>
										<td>
											<input type="text" class="icant sumcm form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="cm2039" id="cm2039">
										</td>
										<td>
											<input type="text" class="icant sumcm form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="cm4064" id="cm4064">
										</td>
										<td>
											<input type="text" class="icant sumcm form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="cm65" id="cm65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcmhid" id="totcmhid" value="0">
												<strong><span id="totcm">0</span></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td>Guardia</td>
										<td>
											<input type="text" class="icant sumguar form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="guar1" id="guar1">
										</td>
										<td>
											<input type="text" class="icant sumguar form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="guar14" id="guar14">
										</td>
										<td>
											<input type="text" class="icant sumguar form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="guar514" id="guar514">
										</td>
										<td>
											<input type="text" class="icant sumguar form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="guar1519" id="guar1519">
										</td>
										<td>
											<input type="text" class="icant sumguar form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="guar2039" id="guar2039">
										</td>
										<td>
											<input type="text" class="icant sumguar form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="guar4064" id="guar4064">
										</td>
										<td>
											<input type="text" class="icant sumguar form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="guar65" id="guar65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totguarhid" id="totguarhid" value="0">
												<strong><span id="totguar">0</span></strong>
											</div>
										</td>
									</tr>
									<tr  id="tr_consultorio_covid">
										<td>COVID-19</td>
										<td>
											<input type="text" class="icant sumcovid form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="covid1" id="covid1">
										</td>
										<td>
											<input type="text" class="icant sumcovid form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="covid14" id="covid14">
										</td>
										<td>
											<input type="text" class="icant sumcovid form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="covid514" id="covid514">
										</td>
										<td>
											<input type="text" class="icant sumcovid form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="covid1519" id="covid1519">
										</td>
										<td>
											<input type="text" class="icant sumcovid form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="covid2039" id="covid2039">
										</td>
										<td>
											<input type="text" class="icant sumcovid form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="covid4064" id="covid4064">
										</td>
										<td>
											<input type="text" class="icant sumcovid form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="covid65" id="covid65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcovidhid" id="totcovidhid" value="0">
												<strong><span id="totcovid">0</span></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td>Total</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcon1hid" id="totcon1hid" value="0">
												<strong><span id="totcon1">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcon14hid" id="totcon14hid" value="0">
												<strong><span id="totcon14">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcon514hid" id="totcon514hid" value="0">
												<strong><span id="totcon514">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcon1519hid" id="totcon1519hid" value="0">
												<strong><span id="totcon1519">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcon2039hid" id="totcon2039hid" value="0">
												<strong><span id="totcon2039">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcon4064hid" id="totcon4064hid" value="0">
												<strong><span id="totcon4064">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcon65hid" id="totcon65hid" value="0">
												<strong><span id="totcon65">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totconsultashid" id="totconsultashid" value="0">
												<strong><span id="totconsultas">0</span></strong>
											</div>
										</td>
									</tr>
								</tbody>
							</table>							
						</div>
						
					</div>
</div>