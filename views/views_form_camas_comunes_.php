<div class="card " id="form_camas_adultos_comunes">
				  	<div class="card-header bg-primary text-white"><strong><center>Camas Adultos</center></strong>
				  	</div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="row">
									<div class="col-lg-12">
										<div class="alert alert-info text-center" role="alert">
											DISPONIBLES							  	
										</div>									  	
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Generales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasGenDispAdult" id="camasGenDispAdult">
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Especiales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasEspDispAdult" id="camasEspDispAdult">
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Críticas:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasCritDispAdult" id="camasCritDispAdult">
									</div>									
								</div>

							</div>
							<div class="col-lg-6">
								<div class="row">
									<div class="col-lg-12">
										<div class="alert alert-warning text-center" role="alert">
											OCUPADAS							  	
										</div>									  	
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Generales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasGenOcupAdult" id="camasGenOcupAdult">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Especiales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasEspOcupAdult" id="camasEspOcupAdult">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Críticas:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasCritOcupAdult" id="camasCritOcupAdult">
									</div>								
								</div>
							</div>
						</div>
						
					</div>
					
</div>
<div class="card " id="form_camas_adultos_comunesCovid">
				  	<div class="card-header bg-primary text-white"><strong><center>Camas Adultos COVID</center></strong>
				  	</div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="row">
									<div class="col-lg-12">
										<div class="alert alert-info text-center" role="alert">
											DISPONIBLES							  	
										</div>									  	
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Generales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasGenDispAdultCovid" id="camasGenDispAdultCovid">
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Especiales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasEspDispAdultCovid" id="camasEspDispAdultCovid">
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Críticas:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasCritDispAdultCovid" id="camasCritDispAdultCovid">
									</div>									
								</div>

							</div>
							<div class="col-lg-6">
								<div class="row">
									<div class="col-lg-12">
										<div class="alert alert-warning text-center" role="alert">
											OCUPADAS							  	
										</div>									  	
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Generales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasGenOcupAdultCovid" id="camasGenOcupAdultCovid">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Especiales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasEspOcupAdultCovid" id="camasEspOcupAdultCovid">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Críticas:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasCritOcupAdultCovid" id="camasCritOcupAdultCovid">
									</div>								
								</div>
							</div>
						</div>
						
					</div>
					
</div>
				
<div class="card " id="form_camas_pediatricas_comunes">
				  	<div class="card-header bg-success text-white"><strong><center>Camas Pedi&aacute;tricas</center></strong></div>
					<div class="card-body">

						<div class="row">
							<div class="col-lg-6">
								<div class="row">
									<div class="col-lg-12">
										<div class="alert alert-info text-center" role="alert">
											DISPONIBLES							  	
										</div>									  	
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Generales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasGenDispPed" id="camasGenDispPed">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Especiales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasEspDispPed" id="camasEspDispPed">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Críticas:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasCritDispPed" id="camasCritDispPed">
									</div>

								</div>
							  	
							</div>
							<div class="col-lg-6">
							  	<div class="row">
									<div class="col-lg-12">
										<div class="alert alert-warning text-center" role="alert">
											OCUPADAS							  	
										</div>									  	
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Generales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasGenOcupPed" id="camasGenOcupPed">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Especiales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasEspOcupPed" id="camasEspOcupPed">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Críticas:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasCritOcupPed" id="camasCritOcupPed">
									</div>

								</div>

							</div>
						</div>

					</div>
					
</div>
<div class="card " id="form_camas_pediatricas_comunesCovid">
				  	<div class="card-header bg-success text-white"><strong><center>Camas Pedi&aacute;tricas COVID</center></strong></div>
					<div class="card-body">

						<div class="row">
							<div class="col-lg-6">
								<div class="row">
									<div class="col-lg-12">
										<div class="alert alert-info text-center" role="alert">
											DISPONIBLES							  	
										</div>									  	
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Generales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasGenDispPedCovid" id="camasGenDispPedCovid">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Especiales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasEspDispPedCovid" id="camasEspDispPedCovid">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Críticas:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasCritDispPedCovid" id="camasCritDispPedCovid">
									</div>

								</div>
							  	
							</div>
							<div class="col-lg-6">
							  	<div class="row">
									<div class="col-lg-12">
										<div class="alert alert-warning text-center" role="alert">
											OCUPADAS							  	
										</div>									  	
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Generales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasGenOcupPedCovid" id="camasGenOcupPedCovid">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Especiales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasEspOcupPedCovid" id="camasEspOcupPedCovid">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Críticas:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class=" form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasCritOcupPedCovid" id="camasCritOcupPedCovid">
									</div>

								</div>

							</div>
						</div>

					</div>
					
</div>