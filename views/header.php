<?php
	$url_redirect="https://autenticar.sanjuan.gob.ar/?callback=ukrl1gWrDigzKWwmrIGjbnuvF4C6iGCGhHqX69SSSZxafWa6o8hyqYjaosY8MtATbZBYYXj9BhyPZPnAMf384AUx4dqKNOBJiJYXNIspZdxQJ1rPuVnIf1bBMA4h3mIokHKNMcm7gqtK3%2F9Fq1FEVQhOrLsM2ndPk%2F4gavvTM8E%3D";
	/*if((!isset($_GET["sessionToken"])) || (empty($_GET["sessionToken"]))) {
		// Hago el redirect al login
		header("HTTP/1.1 302 Moved Temporarily");
		header("Location: $url_redirect"); 
		exit();
	}*/
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Ministerio de Salud - Parte Diario de Consultas e Internación</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link type="text/css" rel="stylesheet" href="css/custom-bootstrap.min.css">
  
    <link type="text/css" rel="stylesheet" href="css/patron.css">
    <link type="text/css" rel="stylesheet" href="css/patron-iconos.css">
    <link rel="stylesheet" type="text/css" href="css/salud/estilo_salud.css" />
    <link href="bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Parte Diario de Consultas e Internación - Ministerio de Salud de San Juan">
    <meta name="keywords" content="salud, parte diario, consultas, internaciones, gobierno, san juan">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js" type="text/javascript" charset="utf-8" async defer></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" 
    	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    <!-- select2 -->
    <link href="select2/css/select2.min.css" rel="stylesheet" />
    <link href="select2/css/select2-bootstrap4.min.css" rel="stylesheet" />
	<script src="select2/js/select2.min.js"></script>
	<script src="select2/js/i18n/es.js"></script>
	<script src="js/notify.js"></script>

	<style type="text/css" media="screen">
		.select2-container{
			width: 100%!important;
		}
		.select2-search--dropdown .select2-search__field {
			width: 98%;
		}
		.icant {
			text-align: center;
			font-weight: bold;
			font-size:14px;
		}
	</style>
	<script src="js/jwt-decode.js"></script>
	
</head>