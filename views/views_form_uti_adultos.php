<!-- FORM UTI ADULTOS -->
<div class="card " id="form_uti_adultos">
	<div class="card-header bg-primary text-white"><strong><center>SERVICIO DE UTI ADULTOS</center></strong>
	</div>
	<div class="card-body">
		<div class="row">
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Disponibilidad de camas en UTI adultos: <small class="text-muted form-text">Cantitad total de camas en UTI</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control"  required
												maxlength="4" data-parsley-type="digits" name="uti_allocated_adult" id="uti_allocated_adult">
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de internados total en UTI adultos:</label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control"  required
												maxlength="4" data-parsley-type="digits" name="uti_hospitalized_adult_count" id="uti_hospitalized_adult_count">
						</div>
					</div>
				</div>
		</div>
		<hr>
		<div class="row">
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de egresos en UTI adultos por alta médica: <small class="text-muted form-text">Desde el último reporte</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control"  required
												maxlength="4" data-parsley-type="digits" name="uti_discharged_adult_count" id="uti_discharged_adult_count" placeholder="">
						</div>	
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de egresos en UTI adultos derivados a otras instituciones:<small class="text-muted form-text">Desde el último reporte</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control"  required
												maxlength="4" data-parsley-type="digits" name="uti_discharged_derivative_adult_count" id="uti_discharged_derivative_adult_count">
						</div>
					</div>
				</div>
		</div>
		<hr>
		<div class="row">
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de egresos en UTI adultos por fallecimiento: <small class="text-muted form-text">Desde el último reporte</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control"  required
												maxlength="4" data-parsley-type="digits" name="uti_discharged_dead_adult_count" id="uti_discharged_dead_adult_count">
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Disponibilidad de camas con gases para adultos: <small class="text-muted form-text">Cantidad total de camas con gases para adultos</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control"  required
												maxlength="4" data-parsley-type="digits" name="uti_allocated_adult_gas" id="uti_allocated_adult_gas">
						</div>	
					</div>
				</div>
		</div>
		<div class="row">
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de camas con gases ocupadas para adultos: <small class="text-muted form-text">Cantidad total de camas con gases en uso de pacientes adultos</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control"  required
												maxlength="4" data-parsley-type="digits" name="uti_gas_unavailable_adult_count" id="uti_gas_unavailable_adult_count">
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de camas con gases liberadas para adultos: <small class="text-muted form-text">Desde el último reporte</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control"  required
												maxlength="4" data-parsley-type="digits" name="uti_gas_available_adult_count" id="uti_gas_available_adult_count">
						</div>	
					</div>
				</div>
		</div>
		<hr>
		<div class="row">
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Disponibilidad de respiradores para adultos: <small class="text-muted form-text">Cantidad total de respiradores para adultos</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control"  required
												maxlength="4" data-parsley-type="digits" name="respirators_allocated_adult" id="respirators_allocated_adult">
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de respiradores ocupados para adultos: <small class="text-muted form-text">Cantidad total de respiradores en uso de pacientes adultos</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control"  required
												maxlength="4" data-parsley-type="digits" name="respirators_unavailable_adult_count" id="respirators_unavailable_adult_count">
						</div>	
					</div>
				</div>
		</div>
		<div class="row">
				<div class="col-lg-6">
					<div class="form-row">
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label>Cantidad de respiradores liberados para adultos: <small class="text-muted form-text">Desde el último reporte</small></label>
						</div>
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<input type="text" class=" form-control"  required
												maxlength="4" data-parsley-type="digits" name="respirators_available_adult_count" id="respirators_available_adult_count">
						</div>
					</div>
				</div>
				
		</div>
	</div>
</div>

						
							