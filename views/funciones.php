<script type="text/javascript">
	$("#form_camas_pediatricas_comunes").hide();
	$("#form_camas_adultos_comunes").hide();
	$("#form_camas_pediatricas_comunesCovid").hide();
	$("#form_camas_adultos_comunesCovid").hide();
	$("#div_respiradoresdisponibles").hide();

	$("#form_uti_adultos").hide();
	$("#form_uti_pediatrico").hide();
	$("#tr_consultorio_covid").hide();
	
	//$("input").prop('disabled', true);
	function predeterminar_valores_camas_comunes()
	{
		$('#camasdisponibles').val(0);
		$('#respiradoresdisponibles').val(0);
		$('#camasGenDispAdult').val(0);
		$('#camasGenOcupAdult').val(0);
		$('#camasEspDispAdult').val(0);
		$('#camasEspOcupAdult').val(0);
		$('#camasCritDispAdult').val(0);
		$('#camasCritOcupAdult').val(0);
		$('#camasGenDispPed').val(0);
		$('#camasGenOcupPed').val(0);
		$('#camasEspDispPed').val(0);
		$('#camasEspOcupPed').val(0);
		$('#camasCritDispPed').val(0);
		$('#camasCritOcupPed').val(0);

		//covid nuevas
		$('#camasGenDispAdultCovid').val(0);
		$('#camasGenOcupAdultCovid').val(0);
		$('#camasEspDispAdultCovid').val(0);
		$('#camasEspOcupAdultCovid').val(0);
		$('#camasCritDispAdultCovid').val(0);
		$('#camasCritOcupAdultCovid').val(0);
		$('#camasGenDispPedCovid').val(0);
		$('#camasGenOcupPedCovid').val(0);
		$('#camasEspDispPedCovid').val(0);
		$('#camasEspOcupPedCovid').val(0);
		$('#camasCritDispPedCovid').val(0);
		$('#camasCritOcupPedCovid').val(0);
	}
	function predeterminar_valores_uti()
	{
		$('#respirators_allocated_children').val(0); 
		$('#respirators_available_children_count').val(0);
		$('#respirators_unavailable_children_count').val(0);
		$('#uti_allocated_children').val(0);
		$('#uti_allocated_children_gas').val(0); 
		$('#uti_discharged_children_count').val(0);
		$('#uti_discharged_dead_children_count').val(0);
		$('#uti_discharged_derivative_children_count').val(0);
		$('#uti_gas_available_children_count').val(0); 
		$('#uti_gas_unavailable_children_count').val(0);
		$('#uti_hospitalized_children_count').val(0); 

		$('#respirators_allocated_adult').val(0);
		$('#respirators_available_adult_count').val(0);
		$('#respirators_unavailable_adult_count').val(0);
		$('#uti_allocated_adult').val(0);
		$('#uti_allocated_adult_gas').val(0);
		$('#uti_discharged_adult_count').val(0);
		$('#uti_discharged_dead_adult_count').val(0);
		$('#uti_discharged_derivative_adult_count').val(0);
		$('#uti_gas_available_adult_count').val(0);
		$('#uti_gas_unavailable_adult_count').val(0);
		$('#uti_hospitalized_adult_count').val(0);
	}
	function buscar_formularios_x_centro_salud()
	{	
		$("#form_camas_pediatricas_comunes").hide();
		$("#form_camas_adultos_comunes").hide();
		$("#div_respiradoresdisponibles").hide();

		$("#form_camas_pediatricas_comunesCovid").hide();
		$("#form_camas_adultos_comunesCovid").hide();

		$("#form_uti_adultos").hide();
		$("#form_uti_pediatrico").hide();
		$("#tr_consultorio_covid").hide();
		$.notify("Espere por favor ...", "info");
		$.ajax({
			          	url: "ajax_traer_tipo_form_x_establecimiento.php",
				        dataType: 'json',
				        delay: 250,
				        type: "GET",
			          
			          	data:{cod_establecimiento:$('select[id=nombreestablecimiento]').val()},
			          	beforeSend: function() 
			          	{
							
						},
						success: function(data)
				        {
				        	if(data.status == 1)
				          	{
				          		if((data.result.uti == 1) && (data.result.camas == 1))
				          		{
				          			$("#corresponde_uti").val(1);
				          			$("#corresponde_camas").val(1); 

				          			$("#form_camas_pediatricas_comunes").show();
				          			$("#form_camas_adultos_comunes").show();
				          			$("#div_respiradoresdisponibles").show();

				          			$("#form_camas_pediatricas_comunesCovid").show();
									$("#form_camas_adultos_comunesCovid").show();

				          			$("#form_uti_adultos").show();
				          			$("#form_uti_pediatrico").show();

				          		}
				          		else if(data.result.uti == 1)
				          		{
				          			//ocultar camas comunes y predeterminar valores
				          			predeterminar_valores_camas_comunes();
				          			$("#corresponde_uti").val(1);
				          			$("#corresponde_camas").val(0);  
				          			
				          			$("#form_camas_pediatricas_comunes").hide();
				          			$("#form_camas_adultos_comunes").hide();
				          			$("#div_respiradoresdisponibles").hide();

				          			$("#form_camas_pediatricas_comunesCovid").hide();
									$("#form_camas_adultos_comunesCovid").hide();

				          			$("#form_uti_adultos").show();
				          			$("#form_uti_pediatrico").show();
				          			$("[name='respiradoresdisponibles']").prop("required", false);
				          		}
				          		else if(data.result.camas == 1)
				          		{
				          			//ocultar uti y predeterminar valores
				          			predeterminar_valores_uti();
				          			$("#corresponde_camas").val(1); 
				          			$("#corresponde_uti").val(0);
				          			//ocultar todo uti
				          			$("#form_uti_adultos").hide();
				          			$("#form_uti_pediatrico").hide();
				          			//mostrar los campos antiguos
				          			$("#form_camas_pediatricas_comunes").show();
				          			$("#form_camas_adultos_comunes").show();
				          			$("#div_respiradoresdisponibles").show();

				          			$("#form_camas_pediatricas_comunesCovid").show();
									$("#form_camas_adultos_comunesCovid").show();
				          			$("[name='respiradoresdisponibles']").prop("required", true);
				          		}
				          		else
				          		{
				          			$("#form_camas_pediatricas_comunes").hide();
				          			$("#form_camas_adultos_comunes").hide();
				          			$("#div_respiradoresdisponibles").hide();

				          			$("#form_camas_pediatricas_comunesCovid").hide();
									$("#form_camas_adultos_comunesCovid").hide();

				          			$("#form_uti_adultos").hide();
				          			$("#form_uti_pediatrico").hide();
				          		}

				          		if(data.result.consultorio_covid == 1)
				          		{
				          			$("#tr_consultorio_covid").show();
				          		}

				          		//console.log(data.result.uti); 
				          		//console.log(data.result.camas); 
				          	}
				        }
				});
    }
	
	function buscar_parte_diario()
    		{
    			$('#div_partes_cargados').empty();
    			$.notify("Buscando Partes Diarios", "info");
    			$.ajax({
			          	url: "ajax_buscar_partes.php",
				        dataType: 'json',
				        delay: 250,
				        type: "GET",
			          
			          	data:{cod_establecimiento:$('select[id=nombreestablecimiento]').val(), fecha: $('#campofecha').val()},
			          	beforeSend: function() 
			          	{
							
						},
				        success: function(data)
				        { 
				          //	console.log(data.result); 
						  	ultimo_id = 0;
				          	if(data.status == 1)
				          	{
				          		var partes = data.result;
				          		var table = '<center><br><h4 class="text-center"><center>Partes Registrados</center></h4><br></center><table class="table table-striped">'+
								  '<thead class="thead-dark">'+
								    '<tr>'+
								      '<th scope="col">#</th>'+
								      '<th scope="col">Fecha</th>'+
								      '<th scope="col">Fecha de Carga</th>'+
								      '<th scope="col">Centro de Salud</th>'+
								      '<th scope="col">Acciones</th>'+
								    '</tr>'+
								  '</thead>'+
								  '<tbody>';
								
				          		for(var i=0; i < partes.length ; i++)
				          		{
				          			var fecha =  partes[i]['fecha'].substr(8,2)+'/'+partes[i]['fecha'].substr(5,2)+'/'+partes[i]['fecha'].substr(0,4);
				          			var fecha_carga =  partes[i]['fecha_carga'].substr(8,2)+'/'+partes[i]['fecha_carga'].substr(5,2)+'/'+partes[i]['fecha_carga'].substr(0,4)+' - '+partes[i]['fecha_carga'].substr(11,5) ;
				          			//console.log(partes[i]);
				          			table = table + '<tr>'+
								      '<th scope="row">'+partes[i]['idparte']+'</th>'+
								      '<td>'+fecha+'</td>'+
								      '<td>'+fecha_carga+' hs</td>'+
								      '<td>'+partes[i]['nombre']+'</td>'+
								      '<td><strong> <a class="btn btn-outline-info" onclick="traer_parte('+partes[i]['idparte']+')">Ver Parte</a> </strong></td>'+
								/*     '<td><strong> <a class="btn btn-outline-info" href="consulta_parte/'+partes[i]['idparte']+'" target="_blank" >Ver Parte</a> </strong></td>'+*/
								    '</tr>';
									ultimo_id = partes[i]['idparte'];
				          		}
				          		table = table +'</tbody></table>';
				          		$('#div_partes_cargados').html('<div class="container">'+table+'</div>');

				          	}

							if(ultimo_id > 0)//quiere decir q hay un parte
							{
								//traer_parte_de_precarga(ultimo_id);
							}
				        }
				    });
				
				//traer ultimo parte del centro de salud
				$.ajax({
					url: "ajax_traer_ultimo_parte_caps.php",
				        dataType: 'json',
				        delay: 250,
				        type: "GET",
			          
			          	data:{cod_establecimiento:$('select[id=nombreestablecimiento]').val(), fecha: $('#campofecha').val()},
			          	beforeSend: function() 
			          	{
							
						},
				        success: function(data)
						{
							if(data.status == 1)
				          	{
								
				          		//$.notify("Se Encontraron Registros", "success");
				          		if(data.result)
				          		{
									//console.log('aa');
				          			$('#camasdisponibles').val(data.result.camas);
					         		$('#respiradoresdisponibles').val(data.result.respiradores);
					         		$('#camasGenDispAdult').val(data.result.camasGenDispAdult);
					         		$('#camasGenOcupAdult').val(data.result.camasGenOcupAdult);
					         		$('#camasEspDispAdult').val(data.result.camasEspDispAdult);
					         		$('#camasEspOcupAdult').val(data.result.camasEspOcupAdult);
					         		$('#camasCritDispAdult').val(data.result.camasCritDispAdult);
					         		$('#camasCritOcupAdult').val(data.result.camasCritOcupAdult);
					         		$('#camasGenDispPed').val(data.result.camasGenDispPed);
					         		$('#camasGenOcupPed').val(data.result.camasGenOcupPed);
					         		$('#camasEspDispPed').val(data.result.camasEspDispPed);
					         		$('#camasEspOcupPed').val(data.result.camasEspOcupPed);
					         		$('#camasCritDispPed').val(data.result.camasCritDispPed);
					         		$('#camasCritOcupPed').val(data.result.camasCritOcupPed);

					         		//nuevas covid
					         		$('#camasGenDispAdultCovid').val(data.result.camasGenDispAdultCovid);
					         		$('#camasGenOcupAdultCovid').val(data.result.camasGenOcupAdultCovid);
					         		$('#camasEspDispAdultCovid').val(data.result.camasEspDispAdultCovid);
					         		$('#camasEspOcupAdultCovid').val(data.result.camasEspOcupAdultCovid);
					         		$('#camasCritDispAdultCovid').val(data.result.camasCritDispAdultCovid);
					         		$('#camasCritOcupAdultCovid').val(data.result.camasCritOcupAdultCovid);
					         		$('#camasGenDispPedCovid').val(data.result.camasGenDispPedCovid);
					         		$('#camasGenOcupPedCovid').val(data.result.camasGenOcupPedCovid);
					         		$('#camasEspDispPedCovid').val(data.result.camasEspDispPedCovid);
					         		$('#camasEspOcupPedCovid').val(data.result.camasEspOcupPedCovid);
					         		$('#camasCritDispPedCovid').val(data.result.camasCritDispPedCovid);
					         		$('#camasCritOcupPedCovid').val(data.result.camasCritOcupPedCovid);
				          		}
				          		
				          			if(data.uti_pediatrico)
						         	{
						         		$('#respirators_allocated_children').val(data.uti_pediatrico.respirators_allocated_children); 
										$('#respirators_available_children_count').val(data.uti_pediatrico.respirators_available_children_count); 
										$('#respirators_unavailable_children_count').val(data.uti_pediatrico.respirators_unavailable_children_count); 
										$('#uti_allocated_children').val(data.uti_pediatrico.uti_allocated_children); 
										$('#uti_allocated_children_gas').val(data.uti_pediatrico.uti_allocated_children_gas); 
										$('#uti_discharged_children_count').val(data.uti_pediatrico.uti_discharged_children_count); 
										$('#uti_discharged_dead_children_count').val(data.uti_pediatrico.uti_discharged_dead_children_count); 
										$('#uti_discharged_derivative_children_count').val(data.uti_pediatrico.uti_discharged_derivative_children_count); 
										$('#uti_gas_available_children_count').val(data.uti_pediatrico.uti_gas_available_children_count); 
										$('#uti_gas_unavailable_children_count').val(data.uti_pediatrico.uti_gas_unavailable_children_count); 
										$('#uti_hospitalized_children_count').val(data.uti_pediatrico.uti_hospitalized_children_count); 
						         		totalesConsulta();
						         	}
						         	if(data.uti_adultos)
						         	{
						         		$('#respirators_allocated_adult').val(data.uti_adultos.respirators_allocated_adult); 
										$('#respirators_available_adult_count').val(data.uti_adultos.respirators_available_adult_count); 
										$('#respirators_unavailable_adult_count').val(data.uti_adultos.respirators_unavailable_adult_count); 
										$('#uti_allocated_adult').val(data.uti_adultos.uti_allocated_adult); 
										$('#uti_allocated_adult_gas').val(data.uti_adultos.uti_allocated_adult_gas); 
										$('#uti_discharged_adult_count').val(data.uti_adultos.uti_discharged_adult_count); 
										$('#uti_discharged_dead_adult_count').val(data.uti_adultos.uti_discharged_dead_adult_count); 
										$('#uti_discharged_derivative_adult_count').val(data.uti_adultos.uti_discharged_derivative_adult_count); 
										$('#uti_gas_available_adult_count').val(data.uti_adultos.uti_gas_available_adult_count); 
										$('#uti_gas_unavailable_adult_count').val(data.uti_adultos.uti_gas_unavailable_adult_count); 
										$('#uti_hospitalized_adult_count').val(data.uti_adultos.uti_hospitalized_adult_count); 
						         		totalesConsulta();
						         	}
				          		
							}
						}
				});
    		}
	function cambio_fecha(msg) {
		//console.log('a');
		if( $('select[id=nombreestablecimiento]').val() != '') 
		{
			console.log('no esta vacio');
			//console.log($('select[id=nombreestablecimiento]').val());
			buscar_parte_diario();
		}
	}
	function totalesEnfermedad() {
    			var totint1 = 0;
	            totint1 = parseFloat($("#enf1_1").val()) + parseFloat($("#enf2_1").val()) + parseFloat($("#enf3_1").val())
	            		 + parseFloat($("#enf4_1").val()) + parseFloat($("#enf5_1").val()) + parseFloat($("#enf6_1").val())
	            		  + parseFloat($("#enf7_1").val()) + parseFloat($("#enf8_1").val()) + parseFloat($("#enf9_1").val())
	            		   + parseFloat($("#enf10_1").val()); 
	            $("#totint1").html(totint1);
	            $("#totint1hid").val(totint1);

    			var totint14 = 0;
	            totint14 = parseFloat($("#enf1_14").val()) + parseFloat($("#enf2_14").val()) + parseFloat($("#enf3_14").val())
	            		 + parseFloat($("#enf4_14").val()) + parseFloat($("#enf5_14").val()) + parseFloat($("#enf6_14").val())
	            		  + parseFloat($("#enf7_14").val()) + parseFloat($("#enf8_14").val()) + parseFloat($("#enf9_14").val())
	            		   + parseFloat($("#enf10_14").val()); 
	            $("#totint14").html(totint14);
	            $("#totint14hid").val(totint14);

    			var totint514 = 0;
	            totint514 = parseFloat($("#enf1_514").val()) + parseFloat($("#enf2_514").val()) + parseFloat($("#enf3_514").val())
	            		 + parseFloat($("#enf4_514").val()) + parseFloat($("#enf5_514").val()) + parseFloat($("#enf6_514").val())
	            		  + parseFloat($("#enf7_514").val()) + parseFloat($("#enf8_514").val()) + parseFloat($("#enf9_514").val())
	            		   + parseFloat($("#enf10_514").val()); 
	            $("#totint514").html(totint514);
	            $("#totint514hid").val(totint514);

    			var totint1519 = 0;
	            totint1519 = parseFloat($("#enf1_1519").val()) + parseFloat($("#enf2_1519").val()) + parseFloat($("#enf3_1519").val())
	            		 + parseFloat($("#enf4_1519").val()) + parseFloat($("#enf5_1519").val()) + parseFloat($("#enf6_1519").val())
	            		  + parseFloat($("#enf7_1519").val()) + parseFloat($("#enf8_1519").val()) + parseFloat($("#enf9_1519").val())
	            		   + parseFloat($("#enf10_1519").val()); 
	            $("#totint1519").html(totint1519);
	            $("#totint1519hid").val(totint1519);

    			var totint2039 = 0;
	            totint2039 = parseFloat($("#enf1_2039").val()) + parseFloat($("#enf2_2039").val()) + parseFloat($("#enf3_2039").val())
	            		 + parseFloat($("#enf4_2039").val()) + parseFloat($("#enf5_2039").val()) + parseFloat($("#enf6_2039").val())
	            		  + parseFloat($("#enf7_2039").val()) + parseFloat($("#enf8_2039").val()) + parseFloat($("#enf9_2039").val())
	            		   + parseFloat($("#enf10_2039").val()); 
	            $("#totint2039").html(totint2039);
	            $("#totint2039hid").val(totint2039);

    			var totint4064 = 0;
	            totint4064 = parseFloat($("#enf1_4064").val()) + parseFloat($("#enf2_4064").val()) + parseFloat($("#enf3_4064").val())
	            		 + parseFloat($("#enf4_4064").val()) + parseFloat($("#enf5_4064").val()) + parseFloat($("#enf6_4064").val())
	            		  + parseFloat($("#enf7_4064").val()) + parseFloat($("#enf8_4064").val()) + parseFloat($("#enf9_4064").val())
	            		   + parseFloat($("#enf10_4064").val()); 
	            $("#totint4064").html(totint4064);
	            $("#totint4064hid").val(totint4064);

    			var totint65 = 0;
	            totint65 = parseFloat($("#enf1_65").val()) + parseFloat($("#enf2_65").val()) + parseFloat($("#enf3_65").val())
	            		 + parseFloat($("#enf4_65").val()) + parseFloat($("#enf5_65").val()) + parseFloat($("#enf6_65").val())
	            		  + parseFloat($("#enf7_65").val()) + parseFloat($("#enf8_65").val()) + parseFloat($("#enf9_65").val())
	            		   + parseFloat($("#enf10_65").val()); 
	            $("#totint65").html(totint65);
	            $("#totint65hid").val(totint65);

    			var totinternados = 0;
	            totinternados = parseFloat($("#totenf1hid").val()) + parseFloat($("#totenf2hid").val()) + parseFloat($("#totenf3hid").val())
	            				 + parseFloat($("#totenf4hid").val()) + parseFloat($("#totenf5hid").val()) + parseFloat($("#totenf6hid").val())
	            				  + parseFloat($("#totenf7hid").val()) + parseFloat($("#totenf8hid").val()) + parseFloat($("#totenf9hid").val())
	            				   + parseFloat($("#totenf10hid").val()); 
	            $("#totinternados").html(totinternados);
	            $("#totinternadoshid").val(totinternados);
    		}

    function totalesConsulta() {
    			var totcon1 = 0;
	            totcon1 = parseFloat($("#ped1").val()) + parseFloat($("#cm1").val()) + parseFloat($("#guar1").val()) + parseFloat($("#covid1").val()); 
	            $("#totcon1").html(totcon1);
	            $("#totcon1hid").val(totcon1);

    			var totcon14 = 0;
	            totcon14 = parseFloat($("#ped14").val()) + parseFloat($("#cm14").val()) + parseFloat($("#guar14").val()) + parseFloat($("#covid14").val()); 
	            $("#totcon14").html(totcon14);
	            $("#totcon14hid").val(totcon14);

    			var totcon514 = 0;
	            totcon514 = parseFloat($("#ped514").val()) + parseFloat($("#cm514").val()) + parseFloat($("#guar514").val()) + parseFloat($("#covid514").val()); 
	            $("#totcon514").html(totcon514);
	            $("#totcon514hid").val(totcon514);

    			var totcon1519 = 0;
	            totcon1519 = parseFloat($("#ped1519").val()) + parseFloat($("#cm1519").val()) + parseFloat($("#guar1519").val()) + parseFloat($("#covid1519").val()); 
	            $("#totcon1519").html(totcon1519);
	            $("#totcon1519hid").val(totcon1519);

    			var totcon2039 = 0;
	            totcon2039 = parseFloat($("#ped2039").val()) + parseFloat($("#cm2039").val()) + parseFloat($("#guar2039").val()) + parseFloat($("#covid2039").val()); 
	            $("#totcon2039").html(totcon2039);
	            $("#totcon2039hid").val(totcon2039);

    			var totcon4064 = 0;
	            totcon4064 = parseFloat($("#ped4064").val()) + parseFloat($("#cm4064").val()) + parseFloat($("#guar4064").val()) + parseFloat($("#covid4064").val()); 
	            $("#totcon4064").html(totcon4064);
	            $("#totcon4064hid").val(totcon4064);

    			var totcon65 = 0;
	            totcon65 = parseFloat($("#ped65").val()) + parseFloat($("#cm65").val()) + parseFloat($("#guar65").val()) + parseFloat($("#covid65").val()); 
	            $("#totcon65").html(totcon65);
	            $("#totcon65hid").val(totcon65);

    			var totconsultas = 0;
	            totconsultas = parseFloat($("#totpedhid").val()) + parseFloat($("#totcmhid").val()) + parseFloat($("#totguarhid").val()) + parseFloat($("#totcovidhid").val()); 
	            $("#totconsultas").html(totconsultas);
	            $("#totconsultashid").val(totconsultas);

    		}
    function totalesDeTotales()
  			{
  				var totped = 0;
	            $('.sumped').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totped = totped + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totped").html(totped);
	            $("#totpedhid").val(totped);

	            var totcm = 0;
	            $('.sumcm').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totcm = totcm + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totcm").html(totcm);
	            $("#totcmhid").val(totcm);

	            var totguar = 0;
	            $("#totguarhid").val(totguar);
	            $('.sumguar').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{          
	            		//console.log(this);    
	            		totguar = totguar + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totguar").html(totguar);
	            $("#totguarhid").val(totguar);

	            var totcovid = 0;
	            $("#totcovidhid").val(totguar);
	            $('.sumcovid').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{          
	            		//console.log(this);    
	            		totcovid = totcovid + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totcovid").html(totcovid);
	            $("#totcovidhid").val(totcovid);
				//totguarhid
	            var totenf1 = 0;
	            $('.sumenf1').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf1 = totenf1 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf1").html(totenf1);
	            $("#totenf1hid").val(totenf1);

	            var totenf2 = 0;
	            $('.sumenf2').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf2 = totenf2 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf2").html(totenf2);
	            $("#totenf2hid").val(totenf2);

	            var totenf3 = 0;
	            $('.sumenf3').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf3 = totenf3 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf3").html(totenf3);
	            $("#totenf3hid").val(totenf3);

	            var totenf4 = 0;
	            $('.sumenf4').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf4 = totenf4 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf4").html(totenf4);
	            $("#totenf4hid").val(totenf4);

	            var totenf5 = 0;
	            $('.sumenf5').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf5 = totenf5 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf5").html(totenf5);
	            $("#totenf5hid").val(totenf5);

	            var totenf6 = 0;
	            $('.sumenf6').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf6 = totenf6 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf6").html(totenf6);
	            $("#totenf6hid").val(totenf6);

	            var totenf7 = 0;
	            $('.sumenf7').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf7 = totenf7 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf7").html(totenf7);
	            $("#totenf7hid").val(totenf7);

	            var totenf8 = 0;
	            $('.sumenf8').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf8 = totenf8 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf8").html(totenf8);
	            $("#totenf8hid").val(totenf8);


	            var totenf9 = 0;
	            $('.sumenf9').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf9 = totenf9 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf9").html(totenf9);
	            $("#totenf9hid").val(totenf9)

	            var totenf10 = 0;
	            $('.sumenf10').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf10 = totenf10 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf10").html(totenf10);
	            $("#totenf10hid").val(totenf10);

	            totalesConsulta();
	            totalesEnfermedad();

  			}
	function reset_inputs()
	{
		$('#respirators_allocated_children').val(''); 
		$('#respirators_available_children_count').val(''); 
		$('#respirators_unavailable_children_count').val(''); 
		$('#uti_allocated_children').val(''); 
		$('#uti_allocated_children_gas').val(''); 
		$('#uti_discharged_children_count').val(''); 
		$('#uti_discharged_dead_children_count').val(''); 
		$('#uti_discharged_derivative_children_count').val(''); 
		$('#uti_gas_available_children_count').val(''); 
		$('#uti_gas_unavailable_children_count').val(''); 
		$('#uti_hospitalized_children_count').val(''); 

		$('#respirators_allocated_adult').val(''); 
		$('#respirators_available_adult_count').val(''); 
		$('#respirators_unavailable_adult_count').val(''); 
		$('#uti_allocated_adult').val(''); 
		$('#uti_allocated_adult_gas').val(''); 
		$('#uti_discharged_adult_count').val(''); 
		$('#uti_discharged_dead_adult_count').val(''); 
		$('#uti_discharged_derivative_adult_count').val(''); 
		$('#uti_gas_available_adult_count').val(''); 
		$('#uti_gas_unavailable_adult_count').val(''); 
		$('#uti_hospitalized_adult_count').val(''); 

		$('#camasdisponibles').val('');
		$('#respiradoresdisponibles').val('');
		$('#camasGenDispAdult').val('');
		$('#camasGenOcupAdult').val('');
		$('#camasEspDispAdult').val('');
		$('#camasEspOcupAdult').val('');
		$('#camasCritDispAdult').val('');
		$('#camasCritOcupAdult').val('');
		$('#camasGenDispPed').val('');
		$('#camasGenOcupPed').val('');
		$('#camasEspDispPed').val('');
		$('#camasEspOcupPed').val('');
		$('#camasCritDispPed').val('');
		$('#camasCritOcupPed').val('');

		//nuevas covid
		$('#camasGenDispAdultCovid').val('');
		$('#camasGenOcupAdultCovid').val('');
		$('#camasEspDispAdultCovid').val('');
		$('#camasEspOcupAdultCovid').val('');
		$('#camasCritDispAdultCovid').val('');
		$('#camasCritOcupAdultCovid').val('');
		$('#camasGenDispPedCovid').val('');
		$('#camasGenOcupPedCovid').val('');
		$('#camasEspDispPedCovid').val('');
		$('#camasEspOcupPedCovid').val('');
		$('#camasCritDispPedCovid').val('');
		$('#camasCritOcupPedCovid').val('');

		$('#ped1').val('0');
		$('#ped14').val('0');
		$('#ped514').val('0');
		$('#ped1519').val('0');
		$('#ped2039').val('0');
		$('#ped4064').val('0');
		$('#cm1').val('0');
		$('#cm14').val('0');
		$('#cm514').val('0');
		$('#cm1519').val('0');
		$('#cm2039').val('0');
		$('#cm4064').val('0');
		$('#cm65').val('0');
		$('#guar1').val('0');
		$('#guar14').val('0');
		$('#guar514').val('0');
		$('#guar1519').val('0');
		$('#guar2039').val('0');
		$('#guar4064').val('0');
		$('#guar65').val('0');

		$('#covid1').val('0');
		$('#covid14').val('0');
		$('#covid514').val('0');
		$('#covid1519').val('0');
		$('#covid2039').val('0');
		$('#covid4064').val('0');
		$('#covid65').val('0');

				for(var k=1; k < 11 ; k++)
				{
				    $('#enf'+k+'_1').val('0');
					$('#enf'+k+'_14').val('0');
					$('#enf'+k+'_514').val('0');
					$('#enf'+k+'_1519').val('0');
					$('#enf'+k+'_2039').val('0');
					$('#enf'+k+'_4064').val('0');
					$('#enf'+k+'_65').val('0');
				}
				totalesEnfermedad();
				totalesConsulta();
				totalesDeTotales();

	}
	function validar_campos_uti_contra_camas()
	{
		//alert('1');
		$("#content_alert").empty();
		var listoParaEnviar = 1;
		if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var resp_adultos = 0;
	        		var resp_ped = 0;
	        		var suma = 0;
	        		if(( $("#respirators_allocated_adult").val() != '') && ( $("#respirators_allocated_children").val() != '' ) )
	        		{
	        			resp_adultos = $("#respirators_allocated_adult").val();
	        			suma = parseFloat($("#respirators_allocated_children").val()) + parseFloat($("#respirators_allocated_adult").val());
	        		}
	        		else if($("#respirators_allocated_children").val() != '')
	        		{
	        			resp_ped = $("#respirators_allocated_children").val();
	        			suma =  parseFloat($("#respirators_allocated_children").val());
	        		}
	        		else if($("#respirators_allocated_adult").val() != '')
	        		{
	        			resp_ped = $("#respirators_allocated_adult").val();
	        			suma =  parseFloat($("#respirators_allocated_adult").val());
	        		}

	        		if( parseFloat($("#respiradoresdisponibles").val() ) != ( (parseFloat(suma) )) )
	        		{
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de respiradores para adultos mas Disponibilidad de respiradores para Pediatría debe ser igual a Resp. disponibles</strong></div>');
	        			$("#button_cargar").hide();
	        			listoParaEnviar = 0;
	        			//console.log('disponibles: '+parseFloat($("#respiradoresdisponibles").val()) );
	        			//console.log('suma: '+(parseFloat(suma)));
	        		}
	        		else
	        		{
	        			$("#button_cargar").show();
	        			
	        		}
	        		// respirators_allocated_adult debe ser <= que (uti_allocated_adult)
	        		if( parseFloat($("#respirators_allocated_adult").val() ) >  parseFloat($("#uti_allocated_adult").val()) )
	        		{
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La Disponibilidad de respiradores para adultos  debe ser menor o igual Disponibilidad de camas en UTI adultos</strong></div>');
	        			$("#button_cargar").hide();
	        		}
	        		else
	        		{
	        			$("#button_cargar").show();
	        			
	        		}
					// respirators_allocated_children debe ser <= que (uti_allocated_children)
	        		if( parseFloat($("#respirators_allocated_children").val() ) >  parseFloat($("#uti_allocated_children").val()) )
	        		{
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La Disponibilidad de respiradores para Pediátricas  debe ser menor o igual Disponibilidad de camas en UTI Pediátricas</strong></div>');
	        			$("#button_cargar").hide();
	        		}
	        		else
	        		{
	        			$("#button_cargar").show();
	        			
	        		}
	        	}
	    /*if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspDispPed = parseFloat($("#camasEspDispPed").val()) + parseFloat($("#camasEspDispPedCovid").val());
	        		console.log('suma: '+acumcamasEspDispPed+' uti_allocated_children_gas: '+$("#uti_allocated_children_gas").val());
	        		if( parseFloat($("#uti_allocated_children_gas").val()) != acumcamasEspDispPed   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Pediátricas Especiales DISPONIBLES + Camas Pediátricas Especiales DISPONIBLES COVID) debe ser igual a Disponibilidad de camas con gases para Pediatría</strong></div>');
	        			listoParaEnviar = 0;
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}*/
		/*if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspDispAdult = parseFloat($("#camasEspDispAdult").val()) + parseFloat($("#camasEspDispAdultCovid").val());
	        		console.log('suma: '+acumcamasEspDispAdult+' uti_allocated_adult_gas: '+$("#uti_allocated_adult_gas").val());
	        		if( parseFloat($("#uti_allocated_adult_gas").val()) != acumcamasEspDispAdult   )
	        		{
	        			$("#button_cargar").hide();
	        			listoParaEnviar = 0;
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Adultos Especiales DISPONIBLES + Camas Adultos Especiales DISPONIBLES COVID)debe ser igual a Disponibilidad de camas con gases para adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}*/
		if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritDispAdult = parseFloat($("#camasCritDispAdult").val()) + parseFloat($("#camasCritDispAdultCovid").val());
	        		console.log('suma: '+acumcamasCritDispAdult+' uti_allocated_adult: '+$("#uti_allocated_adult").val());
	        		if( parseFloat($("#uti_allocated_adult").val()) != acumcamasCritDispAdult )
	        		{
	        			$("#button_cargar").hide();
	        			listoParaEnviar = 0;
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Adultos Críticas DISPONIBLES + Camas Adultos Críticas DISPONIBLES COVID) debe ser igual a Disponibilidad de camas en UTI adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
		if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritDispPed = parseFloat($("#camasCritDispPed").val()) + parseFloat($("#camasCritDispPedCovid").val());
	        		console.log('suma: '+acumcamasCritDispPed+' uti_allocated_children: '+$("#uti_allocated_children").val());

	        		if( parseFloat($("#uti_allocated_children").val()) != acumcamasCritDispPed   )
	        		{
	        			$("#button_cargar").hide();
	        			listoParaEnviar = 0;
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de ( Camas Pediátricas Críticas DISPONIBLES +Camas Pediátricas Críticas DISPONIBLES COVID) debe ser igual a Disponibilidad de camas en UTI Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
		/*if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspOcupAdult = parseFloat($("#camasEspOcupAdult").val()) + parseFloat($("#camasEspOcupAdultCovid").val());
	        		console.log('suma: '+acumcamasEspOcupAdult+' uti_gas_unavailable_adult_count: '+$("#uti_gas_unavailable_adult_count").val());
	        		if( parseFloat($("#uti_gas_unavailable_adult_count").val()) != acumcamasEspOcupAdult )
	        		{
	        			$("#button_cargar").hide();
	        			listoParaEnviar = 0;
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Adultos Especiales OCUPADAS + Camas Adultos Especiales OCUPADAS COVID ) debe ser igual a Cantidad de camas con gases ocupadas para adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}*/
		if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspOcupPed = parseFloat($("#camasEspOcupPed").val()) + parseFloat($("#camasEspOcupPedCovid").val());
	        		console.log('suma: '+acumcamasEspOcupPed+' uti_gas_unavailable_children_count: '+$("#uti_gas_unavailable_children_count").val());
	        		if( parseFloat($("#uti_gas_unavailable_children_count").val()) != acumcamasEspOcupPed )
	        		{
	        			$("#button_cargar").hide();
	        			listoParaEnviar = 0;
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Pediátricas Especiales OCUPADAS + Camas Pediátricas Especiales OCUPADAS COVID ) debe ser igual a Cantidad de camas con gases ocupadas para Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
		if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritOcupAdult = parseFloat($("#camasCritOcupAdult").val()) + parseFloat($("#camasCritOcupAdultCovid").val());
	        		console.log('suma: '+acumcamasCritOcupAdult+' uti_hospitalized_adult_count: '+$("#uti_hospitalized_adult_count").val());

	        		if( parseFloat($("#uti_hospitalized_adult_count").val()) != acumcamasCritOcupAdult   )
	        		{
	        			$("#button_cargar").hide();
	        			listoParaEnviar = 0;
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de ( Camas Adultos Críticas OCUPADAS + Camas Adultos Críticas OCUPADAS COVID )debe ser igual a Cantidad de internados total en UTI adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
		if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritOcupPed = parseFloat($("#camasCritOcupPed").val()) + parseFloat($("#camasCritOcupPedCovid").val());
	        		console.log('suma: '+acumcamasCritOcupPed+' uti_hospitalized_children_count: '+$("#uti_hospitalized_children_count").val());

	        		if( parseFloat($("#uti_hospitalized_children_count").val()) != acumcamasCritOcupPed   )
	        		{
	        			$("#button_cargar").hide();
	        			listoParaEnviar = 0;
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de ( Camas Pediátricas Críticas OCUPADAS + Camas Pediátricas Críticas OCUPADAS COVID ) debe ser igual a Cantidad de internados total en UTI Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}

	        	if(listoParaEnviar == 1)
	        	{
	        		console.log('ok');
	        		//$( "#formparte" ).submit();
	        	}
	        	return listoParaEnviar;
	}
	function traer_parte_de_precarga(id)
	{
		//$('#button_cargar').hide();
    	$.notify("Precarga. Por favor espere...", "success");
    	console.log(id);
    	reset_inputs();
		$.ajax({
			          	url: "ajax_consulta_parte_nuevo.php",
				        dataType: 'json',
				        delay: 250,
				        type: "GET",
			          
			          	/*data:{cod_establecimiento:$('select[id=nombreestablecimiento]').val(), fecha: $('#campofecha').val()},*/
			          	data: {id: id},
			          	beforeSend: function() 
			          	{
							
						},
				        success: function(data)
				        {
							if(data.status == 1)
				          	{
				          		//$.notify("Se Encontraron Registros", "success");
				          		if($("#corresponde_camas").val() == 1)
				          		{
				          			$('#camasdisponibles').val(data.result.camas);
					         		$('#respiradoresdisponibles').val(data.result.respiradores);
					         		$('#camasGenDispAdult').val(data.result.camasGenDispAdult);
					         		$('#camasGenOcupAdult').val(data.result.camasGenOcupAdult);
					         		$('#camasEspDispAdult').val(data.result.camasEspDispAdult);
					         		$('#camasEspOcupAdult').val(data.result.camasEspOcupAdult);
					         		$('#camasCritDispAdult').val(data.result.camasCritDispAdult);
					         		$('#camasCritOcupAdult').val(data.result.camasCritOcupAdult);
					         		$('#camasGenDispPed').val(data.result.camasGenDispPed);
					         		$('#camasGenOcupPed').val(data.result.camasGenOcupPed);
					         		$('#camasEspDispPed').val(data.result.camasEspDispPed);
					         		$('#camasEspOcupPed').val(data.result.camasEspOcupPed);
					         		$('#camasCritDispPed').val(data.result.camasCritDispPed);
					         		$('#camasCritOcupPed').val(data.result.camasCritOcupPed);

					         		//nuevas covid
					         		$('#camasGenDispAdultCovid').val(data.result.camasGenDispAdultCovid);
					         		$('#camasGenOcupAdultCovid').val(data.result.camasGenOcupAdultCovid);
					         		$('#camasEspDispAdultCovid').val(data.result.camasEspDispAdultCovid);
					         		$('#camasEspOcupAdultCovid').val(data.result.camasEspOcupAdultCovid);
					         		$('#camasCritDispAdultCovid').val(data.result.camasCritDispAdultCovid);
					         		$('#camasCritOcupAdultCovid').val(data.result.camasCritOcupAdultCovid);
					         		$('#camasGenDispPedCovid').val(data.result.camasGenDispPedCovid);
					         		$('#camasGenOcupPedCovid').val(data.result.camasGenOcupPedCovid);
					         		$('#camasEspDispPedCovid').val(data.result.camasEspDispPedCovid);
					         		$('#camasEspOcupPedCovid').val(data.result.camasEspOcupPedCovid);
					         		$('#camasCritDispPedCovid').val(data.result.camasCritDispPedCovid);
					         		$('#camasCritOcupPedCovid').val(data.result.camasCritOcupPedCovid);
				          		}
				          		if($("#corresponde_uti").val() == 1)
				          		{
				          			if(data.uti_pediatrico)
						         	{
						         		$('#respirators_allocated_children').val(data.uti_pediatrico.respirators_allocated_children); 
										$('#respirators_available_children_count').val(data.uti_pediatrico.respirators_available_children_count); 
										$('#respirators_unavailable_children_count').val(data.uti_pediatrico.respirators_unavailable_children_count); 
										$('#uti_allocated_children').val(data.uti_pediatrico.uti_allocated_children); 
										$('#uti_allocated_children_gas').val(data.uti_pediatrico.uti_allocated_children_gas); 
										$('#uti_discharged_children_count').val(data.uti_pediatrico.uti_discharged_children_count); 
										$('#uti_discharged_dead_children_count').val(data.uti_pediatrico.uti_discharged_dead_children_count); 
										$('#uti_discharged_derivative_children_count').val(data.uti_pediatrico.uti_discharged_derivative_children_count); 
										$('#uti_gas_available_children_count').val(data.uti_pediatrico.uti_gas_available_children_count); 
										$('#uti_gas_unavailable_children_count').val(data.uti_pediatrico.uti_gas_unavailable_children_count); 
										$('#uti_hospitalized_children_count').val(data.uti_pediatrico.uti_hospitalized_children_count); 
						         		totalesConsulta();
						         	}
						         	if(data.uti_adultos)
						         	{
						         		$('#respirators_allocated_adult').val(data.uti_adultos.respirators_allocated_adult); 
										$('#respirators_available_adult_count').val(data.uti_adultos.respirators_available_adult_count); 
										$('#respirators_unavailable_adult_count').val(data.uti_adultos.respirators_unavailable_adult_count); 
										$('#uti_allocated_adult').val(data.uti_adultos.uti_allocated_adult); 
										$('#uti_allocated_adult_gas').val(data.uti_adultos.uti_allocated_adult_gas); 
										$('#uti_discharged_adult_count').val(data.uti_adultos.uti_discharged_adult_count); 
										$('#uti_discharged_dead_adult_count').val(data.uti_adultos.uti_discharged_dead_adult_count); 
										$('#uti_discharged_derivative_adult_count').val(data.uti_adultos.uti_discharged_derivative_adult_count); 
										$('#uti_gas_available_adult_count').val(data.uti_adultos.uti_gas_available_adult_count); 
										$('#uti_gas_unavailable_adult_count').val(data.uti_adultos.uti_gas_unavailable_adult_count); 
										$('#uti_hospitalized_adult_count').val(data.uti_adultos.uti_hospitalized_adult_count); 
						         		totalesConsulta();
						         	}
				          		}
				          		//validar_campos_uti_contra_camas()
							}
						}
		})
	}
	function traer_parte(id)
    {   $('#button_cargar').hide();
    	$.notify("Buscando Parte. Por favor espere...", "success");
    	console.log(id);
    	reset_inputs();
    	$.ajax({
			          	url: "ajax_consulta_parte_nuevo.php",
				        dataType: 'json',
				        delay: 250,
				        type: "GET",
			          
			          	/*data:{cod_establecimiento:$('select[id=nombreestablecimiento]').val(), fecha: $('#campofecha').val()},*/
			          	data: {id: id},
			          	beforeSend: function() 
			          	{
							
						},
				        success: function(data)
				        { 
				          //	console.log(data.result);
				          	if(data.status == 1)
				          	{
				          		//$.notify("Se Encontraron Registros", "success");
				          		if($("#corresponde_camas").val() == 1)
				          		{
				          			$('#camasdisponibles').val(data.result.camas);
					         		$('#respiradoresdisponibles').val(data.result.respiradores);
					         		$('#camasGenDispAdult').val(data.result.camasGenDispAdult);
					         		$('#camasGenOcupAdult').val(data.result.camasGenOcupAdult);
					         		$('#camasEspDispAdult').val(data.result.camasEspDispAdult);
					         		$('#camasEspOcupAdult').val(data.result.camasEspOcupAdult);
					         		$('#camasCritDispAdult').val(data.result.camasCritDispAdult);
					         		$('#camasCritOcupAdult').val(data.result.camasCritOcupAdult);
					         		$('#camasGenDispPed').val(data.result.camasGenDispPed);
					         		$('#camasGenOcupPed').val(data.result.camasGenOcupPed);
					         		$('#camasEspDispPed').val(data.result.camasEspDispPed);
					         		$('#camasEspOcupPed').val(data.result.camasEspOcupPed);
					         		$('#camasCritDispPed').val(data.result.camasCritDispPed);
					         		$('#camasCritOcupPed').val(data.result.camasCritOcupPed);

					         		$('#camasGenDispAdultCovid').val(data.result.camasGenDispAdultCovid);
					         		$('#camasGenOcupAdultCovid').val(data.result.camasGenOcupAdultCovid);
					         		$('#camasEspDispAdultCovid').val(data.result.camasEspDispAdultCovid);
					         		$('#camasEspOcupAdultCovid').val(data.result.camasEspOcupAdultCovid);
					         		$('#camasCritDispAdultCovid').val(data.result.camasCritDispAdultCovid);
					         		$('#camasCritOcupAdultCovid').val(data.result.camasCritOcupAdultCovid);
					         		$('#camasGenDispPedCovid').val(data.result.camasGenDispPedCovid);
					         		$('#camasGenOcupPedCovid').val(data.result.camasGenOcupPedCovid);
					         		$('#camasEspDispPedCovid').val(data.result.camasEspDispPedCovid);
					         		$('#camasEspOcupPedCovid').val(data.result.camasEspOcupPedCovid);
					         		$('#camasCritDispPedCovid').val(data.result.camasCritDispPedCovid);
					         		$('#camasCritOcupPedCovid').val(data.result.camasCritOcupPedCovid);
				          		}
				          		if($("#corresponde_uti").val() == 1)
				          		{
				          			if(data.uti_pediatrico)
						         	{
						         		$('#respirators_allocated_children').val(data.uti_pediatrico.respirators_allocated_children); 
										$('#respirators_available_children_count').val(data.uti_pediatrico.respirators_available_children_count); 
										$('#respirators_unavailable_children_count').val(data.uti_pediatrico.respirators_unavailable_children_count); 
										$('#uti_allocated_children').val(data.uti_pediatrico.uti_allocated_children); 
										$('#uti_allocated_children_gas').val(data.uti_pediatrico.uti_allocated_children_gas); 
										$('#uti_discharged_children_count').val(data.uti_pediatrico.uti_discharged_children_count); 
										$('#uti_discharged_dead_children_count').val(data.uti_pediatrico.uti_discharged_dead_children_count); 
										$('#uti_discharged_derivative_children_count').val(data.uti_pediatrico.uti_discharged_derivative_children_count); 
										$('#uti_gas_available_children_count').val(data.uti_pediatrico.uti_gas_available_children_count); 
										$('#uti_gas_unavailable_children_count').val(data.uti_pediatrico.uti_gas_unavailable_children_count); 
										$('#uti_hospitalized_children_count').val(data.uti_pediatrico.uti_hospitalized_children_count); 
						         		totalesConsulta();
						         	}
						         	if(data.uti_adultos)
						         	{
						         		$('#respirators_allocated_adult').val(data.uti_adultos.respirators_allocated_adult); 
										$('#respirators_available_adult_count').val(data.uti_adultos.respirators_available_adult_count); 
										$('#respirators_unavailable_adult_count').val(data.uti_adultos.respirators_unavailable_adult_count); 
										$('#uti_allocated_adult').val(data.uti_adultos.uti_allocated_adult); 
										$('#uti_allocated_adult_gas').val(data.uti_adultos.uti_allocated_adult_gas); 
										$('#uti_discharged_adult_count').val(data.uti_adultos.uti_discharged_adult_count); 
										$('#uti_discharged_dead_adult_count').val(data.uti_adultos.uti_discharged_dead_adult_count); 
										$('#uti_discharged_derivative_adult_count').val(data.uti_adultos.uti_discharged_derivative_adult_count); 
										$('#uti_gas_available_adult_count').val(data.uti_adultos.uti_gas_available_adult_count); 
										$('#uti_gas_unavailable_adult_count').val(data.uti_adultos.uti_gas_unavailable_adult_count); 
										$('#uti_hospitalized_adult_count').val(data.uti_adultos.uti_hospitalized_adult_count); 
						         		totalesConsulta();
						         	}
				          		}
					          		
					         	
					         	if(data.parte_detalle)
					            {
					            	for(var i=0; i < data.parte_detalle.length ; i++)
					            	{
					            		
					            		if(data.parte_detalle[i]['id_enfermedad'] ==  280)
					            		{
					            			
					            			if(data.parte_detalle[i]['id_grupoetario'] == 1)
					            			{
					            				$('#ped1').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 2)
					            			{
					            				$('#ped14').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 3)
					            			{
					            				$('#ped514').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 4)
					            			{
					            				$('#ped1519').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 5)
					            			{
					            				$('#ped2039').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] ==6)
					            			{
					            				$('#ped4064').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 7)
					            			{
					            				$('#ped65').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			
					            		}
					            		else if(data.parte_detalle[i]['id_enfermedad'] ==  281)
					            		{
					            			//console.log(data.parte_detalle[i]['id_grupoetario']);
					            			if(data.parte_detalle[i]['id_grupoetario'] == 1)
					            			{
					            				$('#cm1').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 2)
					            			{
					            				$('#cm14').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 3)
					            			{
					            				$('#cm514').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 4)
					            			{
					            				$('#cm1519').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 5)
					            			{
					            				$('#cm2039').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] ==6)
					            			{
					            				$('#cm4064').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 7)
					            			{
					            				$('#cm65').val(data.parte_detalle[i]['cantidad']);
					            			}
					            		}
					            		else if(data.parte_detalle[i]['id_enfermedad'] ==  282)
					            		{
					            			//console.log(data.parte_detalle[i]['id_grupoetario']);
					            			if(data.parte_detalle[i]['id_grupoetario'] == 1)
					            			{
					            				$('#guar1').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 2)
					            			{
					            				$('#guar14').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 3)
					            			{
					            				$('#guar514').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 4)
					            			{
					            				$('#guar1519').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 5)
					            			{
					            				$('#guar2039').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] ==6)
					            			{
					            				$('#guar4064').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 7)
					            			{
					            				$('#guar65').val(data.parte_detalle[i]['cantidad']);
					            			}
					            		}
					            		else if(data.parte_detalle[i]['id_enfermedad'] ==  286)
					            		{
					            			//console.log(data.parte_detalle[i]['id_grupoetario']);
					            			if(data.parte_detalle[i]['id_grupoetario'] == 1)
					            			{
					            				$('#covid1').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 2)
					            			{
					            				$('#covid14').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 3)
					            			{
					            				$('#covid514').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 4)
					            			{
					            				$('#covid1519').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 5)
					            			{
					            				$('#covid2039').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] ==6)
					            			{
					            				$('#covid4064').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 7)
					            			{
					            				$('#covid65').val(data.parte_detalle[i]['cantidad']);
					            			}
					            		}
					            		else if(data.parte_detalle[i]['tipo'] ==  'I')
					            		{

					            		}
					            		
					            	}
					            	for(var j=0; j < data.enfermedades.length; j++)
					            	{
					            		var enfermedad = data.enfermedades[j]['fk_enfermedad'].trim();
					            		
					            		if((enfermedad != '280') && (enfermedad != '281') &&(enfermedad != '282') &&(enfermedad != '286'))
					            		{	//selenf2
					            			var k = j+1;
					            			for(var i=0; i < data.parte_detalle.length ; i++)
					            			{
					            				
					            				var enfermedad_parte = data.parte_detalle[i]['id_enfermedad'].trim();
					            				if(enfermedad_parte == enfermedad)
					            				{
					            					
												    $("#selenf"+k).html('').select2({data: [
													 {id: enfermedad, text: enfermedad+' - '+data.enfermedades[j]['nomb_enf']},
													]});
													
													for(var e=0; e < data.enfermedades_all.length; e++)
													{
														if(enfermedad != data.enfermedades_all[e]['codigo'].trim() )
														{
															$("#selenf"+k).append('').select2({data: [
															 {id: data.enfermedades_all[e]['codigo'], text: data.enfermedades_all[e]['nombre']},
															]});
														}
															
													}
					            					if(data.parte_detalle[i]['id_grupoetario'] == 1)
							            			{
							            				$('#enf'+k+'_1').val(data.parte_detalle[i]['cantidad']);
							            			}
							            			if(data.parte_detalle[i]['id_grupoetario'] == 2)
							            			{
							            				$('#enf'+k+'_14').val(data.parte_detalle[i]['cantidad']);
							            			}
							            			if(data.parte_detalle[i]['id_grupoetario'] == 3)
							            			{
							            				$('#enf'+k+'_514').val(data.parte_detalle[i]['cantidad']);
							            			}
							            			if(data.parte_detalle[i]['id_grupoetario'] == 4)
							            			{
							            				$('#enf'+k+'_1519').val(data.parte_detalle[i]['cantidad']);
							            			}
							            			if(data.parte_detalle[i]['id_grupoetario'] == 5)
							            			{
							            				$('#enf'+k+'_2039').val(data.parte_detalle[i]['cantidad']);
							            			}
							            			if(data.parte_detalle[i]['id_grupoetario'] ==6)
							            			{
							            				$('#enf'+k+'_4064').val(data.parte_detalle[i]['cantidad']);
							            			}
							            			if(data.parte_detalle[i]['id_grupoetario'] == 7)
							            			{
							            				$('#enf'+k+'_65').val(data.parte_detalle[i]['cantidad']);
							            			}
					            				}
					            			}
					            		}
					            		
					            	}
					            	

					            }
					            totalesConsulta();
					            totalesEnfermedad();
					            totalesDeTotales();
					            
					            /*totalesEnfermedad();
					            totalesConsulta();*/
				          	}
				          	else
				          	{
				          		$.notify("No Existen Datos Cargados del Centro de Salud y fecha Seleccionada", "danger");
				          	}
					          	
				        },
				        error: function(response)
				        {
				            console.log(response);
				            $.notify("No Existen Datos Cargados del Centro de Salud y fecha Seleccionada", "danger");
				                     
				        }
			        });
    			
                
				
    }
	jQuery(document).ready(function($) {

			//validar suma de respiradores
			$("#respiradoresdisponibles").change(function() {
				$("#content_alert").empty();
				//console.log('respiradoresdisponibles');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var resp_adultos = 0;
	        		var resp_ped = 0;
	        		var suma = 0;
	        		if(( $("#respirators_allocated_adult").val() != '') && ( $("#respirators_allocated_children").val() != '' ) )
	        		{
	        			resp_adultos = $("#respirators_allocated_adult").val();
	        			suma = parseFloat($("#respirators_allocated_children").val()) + parseFloat($("#respirators_allocated_adult").val());
	        		}
	        		else if($("#respirators_allocated_children").val() != '')
	        		{
	        			resp_ped = $("#respirators_allocated_children").val();
	        			suma =  parseFloat($("#respirators_allocated_children").val());
	        		}
	        		else if($("#respirators_allocated_adult").val() != '')
	        		{
	        			resp_ped = $("#respirators_allocated_adult").val();
	        			suma =  parseFloat($("#respirators_allocated_adult").val());
	        		}

	        		console.log('var respirators_allocated_children '+$("#respirators_allocated_children").val());
	        		console.log('var respirators_allocated_adult '+$("#respirators_allocated_adult").val());
	        		console.log('var suma '+suma);
	        		console.log('respiradoresdisponibles '+$("#respiradoresdisponibles").val());
	        		if( parseFloat($("#respiradoresdisponibles").val() ) != ( (parseFloat(suma) )) )
	        		{
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de respiradores para adultos mas Disponibilidad de respiradores para Pediatría debe ser igual a Resp. disponibles</strong></div>');
	        			$("#button_cargar").hide();
	        			//console.log('disponibles: '+parseFloat($("#respiradoresdisponibles").val()) );
	        			//console.log('suma: '+(parseFloat(suma)));
	        		}
	        		else
	        		{
	        			$("#button_cargar").show();
	        			
	        		}
	        		

	        	}
	        });
	        $("#respirators_allocated_adult").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var resp_adultos = 0;
	        		var resp_ped = 0;
	        		var suma = 0;
	        		if(( $("#respirators_allocated_adult").val() != '') && ( $("#respirators_allocated_children").val() != '' ) )
	        		{
	        			resp_adultos = $("#respirators_allocated_adult").val();
	        			suma = parseFloat($("#respirators_allocated_children").val()) + parseFloat($("#respirators_allocated_adult").val());
	        		}
	        		else if($("#respirators_allocated_children").val() != '')
	        		{
	        			resp_ped = $("#respirators_allocated_children").val();
	        			suma =  parseFloat($("#respirators_allocated_children").val());
	        		}
	        		else if($("#respirators_allocated_adult").val() != '')
	        		{
	        			resp_ped = $("#respirators_allocated_adult").val();
	        			suma =  parseFloat($("#respirators_allocated_adult").val());
	        		}

	        		console.log('var respirators_allocated_children '+$("#respirators_allocated_children").val());
	        		console.log('var respirators_allocated_adult '+$("#respirators_allocated_adult").val());
	        		console.log('var suma '+suma);
	        		console.log('respiradoresdisponibles '+$("#respiradoresdisponibles").val());
	        		if( parseFloat($("#respiradoresdisponibles").val() ) != ( (parseFloat(suma) )) )
	        		{
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de respiradores para adultos mas Disponibilidad de respiradores para Pediatría debe ser igual a Resp. disponibles</strong></div>');
	        			$("#button_cargar").hide();
	        			//console.log('disponibles: '+parseFloat($("#respiradoresdisponibles").val()) );
	        			//console.log('suma: '+(parseFloat(suma)));
	        		}
	        		else
	        		{
	        			$("#button_cargar").show();
	        			
	        		}

	        		// respirators_allocated_adult debe ser <= que (uti_allocated_adult)
	        		if( parseFloat($("#respirators_allocated_adult").val() ) >  parseFloat($("#uti_allocated_adult").val()) )
	        		{
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La Disponibilidad de respiradores para adultos  debe ser menor o igual Disponibilidad de camas en UTI adultos</strong></div>');
	        			$("#button_cargar").hide();
	        		}
	        		else
	        		{
	        			$("#button_cargar").show();
	        			
	        		}


	        	}
	        });
	        $("#respirators_allocated_children").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresped');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var resp_adultos = 0;
	        		var resp_ped = 0;
	        		var suma = 0;
	        		if(( $("#respirators_allocated_adult").val() != '') && ( $("#respirators_allocated_children").val() != '' ) )
	        		{
	        			resp_adultos = $("#respirators_allocated_adult").val();
	        			suma = parseFloat($("#respirators_allocated_children").val()) + parseFloat($("#respirators_allocated_adult").val());
	        		}
	        		else if($("#respirators_allocated_children").val() != '')
	        		{
	        			resp_ped = $("#respirators_allocated_children").val();
	        			suma =  parseFloat($("#respirators_allocated_children").val());
	        		}
	        		else if($("#respirators_allocated_adult").val() != '')
	        		{
	        			resp_ped = $("#respirators_allocated_adult").val();
	        			suma =  parseFloat($("#respirators_allocated_adult").val());
	        		}

	        		console.log('var respirators_allocated_children '+$("#respirators_allocated_children").val());
	        		console.log('var respirators_allocated_adult '+$("#respirators_allocated_adult").val());
	        		console.log('var suma '+suma);
	        		console.log('respiradoresdisponibles '+$("#respiradoresdisponibles").val());
	        		if( parseFloat($("#respiradoresdisponibles").val() ) != ( (parseFloat(suma) )) )
	        		{
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de respiradores para adultos mas Disponibilidad de respiradores para Pediatría debe ser igual a Resp. disponibles</strong></div>');
	        			$("#button_cargar").hide();
	        			//console.log('disponibles: '+parseFloat($("#respiradoresdisponibles").val()) );
	        			//console.log('suma: '+(parseFloat(suma)));
	        		}
	        		else
	        		{
	        			$("#button_cargar").show();
	        			
	        		}
	        		// respirators_allocated_children debe ser <= que (uti_allocated_children)
	        		if( parseFloat($("#respirators_allocated_children").val() ) >  parseFloat($("#uti_allocated_children").val()) )
	        		{
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La Disponibilidad de respiradores para Pediátricas  debe ser menor o igual Disponibilidad de camas en UTI Pediátricas</strong></div>');
	        			$("#button_cargar").hide();
	        		}
	        		else
	        		{
	        			$("#button_cargar").show();
	        			
	        		}
	        	}
	        });
	        // Disponibilidad de respiradores para adultos (respirators_allocated_adult) debe ser menor o igual Disponibilidad de camas en UTI adultos (uti_allocated_adult)

	        //validadcion Camas Pediátricas Especiales DISPONIBLES, Camas Pediátricas Especiales DISPONIBLES COVID y camas con gases para Pediatría
	        /*$("#uti_allocated_children_gas").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspDispPed = parseFloat($("#camasEspDispPed").val()) + parseFloat($("#camasEspDispPedCovid").val());
	        		console.log('suma: '+acumcamasEspDispPed+' uti_allocated_children_gas: '+$("#uti_allocated_children_gas").val());
	        		if( parseFloat($("#uti_allocated_children_gas").val()) != acumcamasEspDispPed   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Pediátricas Especiales DISPONIBLES + Camas Pediátricas Especiales DISPONIBLES COVID) debe ser igual a Disponibilidad de camas con gases para Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});*/
			
			/*$("#camasEspDispPed").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspDispPed = parseFloat($("#camasEspDispPed").val()) + parseFloat($("#camasEspDispPedCovid").val());
	        		console.log('suma: '+acumcamasEspDispPed+' uti_allocated_children_gas: '+$("#uti_allocated_children_gas").val());
	        		if( parseFloat($("#uti_allocated_children_gas").val()) != acumcamasEspDispPed   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Pediátricas Especiales DISPONIBLES + Camas Pediátricas Especiales DISPONIBLES COVID) debe ser igual a Disponibilidad de camas con gases para Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});*/
			
			//validacion nueva cama covid
			/*$("#camasEspDispPedCovid").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspDispPed = parseFloat($("#camasEspDispPed").val()) + parseFloat($("#camasEspDispPedCovid").val());
	        		console.log('suma: '+acumcamasEspDispPed+' uti_allocated_children_gas: '+$("#uti_allocated_children_gas").val());
	        		if( parseFloat($("#uti_allocated_children_gas").val()) != acumcamasEspDispPed   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Pediátricas Especiales DISPONIBLES + Camas Pediátricas Especiales DISPONIBLES COVID) debe ser igual a Disponibilidad de camas con gases para Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});*/
			//fin validacion

			//validacion Camas Adultos Especiales DISPONIBLES, Camas Adultos Especiales DISPONIBLES COVID, Disponibilidad de camas con gases para adultos
			/*$("#uti_allocated_adult_gas").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspDispAdult = parseFloat($("#camasEspDispAdult").val()) + parseFloat($("#camasEspDispAdultCovid").val());
	        		console.log('suma: '+acumcamasEspDispAdult+' uti_allocated_adult_gas: '+$("#uti_allocated_adult_gas").val());
	        		if( parseFloat($("#uti_allocated_adult_gas").val()) != acumcamasEspDispAdult   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Adultos Especiales DISPONIBLES + Camas Adultos Especiales DISPONIBLES COVID)debe ser igual a Disponibilidad de camas con gases para adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});*/
			
			/*$("#camasEspDispAdult").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspDispAdult = parseFloat($("#camasEspDispAdult").val()) + parseFloat($("#camasEspDispAdultCovid").val());
	        		console.log('suma: '+acumcamasEspDispAdult+' uti_allocated_adult_gas: '+$("#uti_allocated_adult_gas").val());
	        		if( parseFloat($("#uti_allocated_adult_gas").val()) != acumcamasEspDispAdult   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Adultos Especiales DISPONIBLES + Camas Adultos Especiales DISPONIBLES COVID)debe ser igual a Disponibilidad de camas con gases para adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
		        	
			});*/
			// validacion nueva cama covid
			
			/*$("#camasEspDispAdultCovid").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspDispAdult = parseFloat($("#camasEspDispAdult").val()) + parseFloat($("#camasEspDispAdultCovid").val());
	        		console.log('suma: '+acumcamasEspDispAdult+' uti_allocated_adult_gas: '+$("#uti_allocated_adult_gas").val());
	        		if( parseFloat($("#uti_allocated_adult_gas").val()) != acumcamasEspDispAdult   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Adultos Especiales DISPONIBLES + Camas Adultos Especiales DISPONIBLES COVID)debe ser igual a Disponibilidad de camas con gases para adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
		        	
			});*/
			//fin validacion
			//validacion Camas Adultos Críticas DISPONIBLES, Camas Adultos Críticas DISPONIBLES COVID, Disponibilidad de camas en UTI adultos
			$("#uti_allocated_adult").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritDispAdult = parseFloat($("#camasCritDispAdult").val()) + parseFloat($("#camasCritDispAdultCovid").val());
	        		console.log('suma: '+acumcamasCritDispAdult+' uti_allocated_adult: '+$("#uti_allocated_adult").val());
	        		if( parseFloat($("#uti_allocated_adult").val()) != acumcamasCritDispAdult )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Adultos Críticas DISPONIBLES + Camas Adultos Críticas DISPONIBLES COVID) debe ser igual a Disponibilidad de camas en UTI adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}

		        	// respirators_allocated_adult debe ser <= que (uti_allocated_adult)
	        		if( parseFloat($("#respirators_allocated_adult").val() ) >  parseFloat($("#uti_allocated_adult").val()) )
	        		{
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La Disponibilidad de respiradores para adultos  debe ser menor o igual Disponibilidad de camas en UTI adultos</strong></div>');
	        			$("#button_cargar").hide();
	        		}
	        		else
	        		{
	        			$("#button_cargar").show();
	        			
	        		}
	        	}
		        	
			});
			$("#camasCritDispAdult").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritDispAdult = parseFloat($("#camasCritDispAdult").val()) + parseFloat($("#camasCritDispAdultCovid").val());
	        		console.log('suma: '+acumcamasCritDispAdult+' uti_allocated_adult: '+$("#uti_allocated_adult").val());
	        		if( parseFloat($("#uti_allocated_adult").val()) != acumcamasCritDispAdult   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Adultos Críticas DISPONIBLES + Camas Adultos Críticas DISPONIBLES COVID) debe ser igual a Disponibilidad de camas en UTI adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
		        	
			});
			
			$("#camasCritDispAdultCovid").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritDispAdult = parseFloat($("#camasCritDispAdult").val()) + parseFloat($("#camasCritDispAdultCovid").val());
	        		console.log('suma: '+acumcamasCritDispAdult+' uti_allocated_adult: '+$("#uti_allocated_adult").val());
	        		if( parseFloat($("#uti_allocated_adult").val()) != acumcamasCritDispAdult   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Adultos Críticas DISPONIBLES + Camas Adultos Críticas DISPONIBLES COVID) debe ser igual a Disponibilidad de camas en UTI adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
		        	
			});
			//fin validacion
			//validacion Camas Pediátricas Críticas DISPONIBLES, Camas Pediátricas Críticas DISPONIBLES COVID, Disponibilidad de camas en UTI Pediátricas
			
			$("#uti_allocated_children").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritDispPed = parseFloat($("#camasCritDispPed").val()) + parseFloat($("#camasCritDispPedCovid").val());
	        		console.log('suma: '+acumcamasCritDispPed+' uti_allocated_children: '+$("#uti_allocated_children").val());

	        		if( parseFloat($("#uti_allocated_children").val()) != acumcamasCritDispPed   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de ( Camas Pediátricas Críticas DISPONIBLES +Camas Pediátricas Críticas DISPONIBLES COVID) debe ser igual a Disponibilidad de camas en UTI Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
		        	// respirators_allocated_children debe ser <= que (uti_allocated_children)
	        		if( parseFloat($("#respirators_allocated_children").val() ) >  parseFloat($("#uti_allocated_children").val()) )
	        		{
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La Disponibilidad de respiradores para Pediátricas  debe ser menor o igual Disponibilidad de camas en UTI Pediátricas</strong></div>');
	        			$("#button_cargar").hide();
	        		}
	        		else
	        		{
	        			$("#button_cargar").show();
	        			
	        		}
	        	}
		        	
			});
			$("#camasCritDispPed").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritDispPed = parseFloat($("#camasCritDispPed").val()) + parseFloat($("#camasCritDispPedCovid").val());
	        		console.log('suma: '+acumcamasCritDispPed+' uti_allocated_children: '+$("#uti_allocated_children").val());
	        		if( parseFloat($("#uti_allocated_children").val()) != acumcamasCritDispPed   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de ( Camas Pediátricas Críticas DISPONIBLES +Camas Pediátricas Críticas DISPONIBLES COVID) debe ser igual a Disponibilidad de camas en UTI Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
		        	
			});
			$("#camasCritDispPedCovid").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritDispPed = parseFloat($("#camasCritDispPed").val()) + parseFloat($("#camasCritDispPedCovid").val());
	        		console.log('suma: '+acumcamasCritDispPed+' uti_allocated_children: '+$("#uti_allocated_children").val());
	        		if( parseFloat($("#uti_allocated_children").val()) != acumcamasCritDispPed   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de ( Camas Pediátricas Críticas DISPONIBLES +Camas Pediátricas Críticas DISPONIBLES COVID) debe ser igual a Disponibilidad de camas en UTI Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
		        	
			});
			//fin validacion
			//Validacion Disponibilidad de Camas Adultos Especiales OCUPADAS, Disponibilidad de Camas Adultos Especiales OCUPADAS covid, camas con gases ocupadas para adultos
			/*$("#uti_gas_unavailable_adult_count").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspOcupAdult = parseFloat($("#camasEspOcupAdult").val()) + parseFloat($("#camasEspOcupAdultCovid").val());
	        		console.log('suma: '+acumcamasEspOcupAdult+' uti_gas_unavailable_adult_count: '+$("#uti_gas_unavailable_adult_count").val());
	        		if( parseFloat($("#uti_gas_unavailable_adult_count").val()) != acumcamasEspOcupAdult )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Adultos Especiales OCUPADAS + Camas Adultos Especiales OCUPADAS COVID ) debe ser igual a Cantidad de camas con gases ocupadas para adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});*/
			/*$("#camasEspOcupAdult").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspOcupAdult = parseFloat($("#camasEspOcupAdult").val()) + parseFloat($("#camasEspOcupAdultCovid").val());
	        		console.log('suma: '+acumcamasEspOcupAdult+' uti_gas_unavailable_adult_count: '+$("#uti_gas_unavailable_adult_count").val());

	        		if( parseFloat($("#uti_gas_unavailable_adult_count").val()) != acumcamasEspOcupAdult   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Adultos Especiales OCUPADAS + Camas Adultos Especiales OCUPADAS COVID ) debe ser igual a Cantidad de camas con gases ocupadas para adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});*/
			/*$("#camasEspOcupAdultCovid").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspOcupAdult = parseFloat($("#camasEspOcupAdult").val()) + parseFloat($("#camasEspOcupAdultCovid").val());
	        		console.log('suma: '+acumcamasEspOcupAdult+' uti_gas_unavailable_adult_count: '+$("#uti_gas_unavailable_adult_count").val());
	        		
	        		if( parseFloat($("#uti_gas_unavailable_adult_count").val()) != acumcamasEspOcupAdult   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Adultos Especiales OCUPADAS + Camas Adultos Especiales OCUPADAS COVID ) debe ser igual a Cantidad de camas con gases ocupadas para adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});*/
			//fin validacion
			//validacion Disponibilidad de Camas Pediátricas Especiales OCUPADAS, Disponibilidad de Camas Pediátricas Especiales OCUPADAS covid, camas con gases ocupadas para Pediatría 
			$("#uti_gas_unavailable_children_count").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspOcupPed = parseFloat($("#camasEspOcupPed").val()) + parseFloat($("#camasEspOcupPedCovid").val());
	        		console.log('suma: '+acumcamasEspOcupPed+' uti_gas_unavailable_children_count: '+$("#uti_gas_unavailable_children_count").val());
	        		if( parseFloat($("#uti_gas_unavailable_children_count").val()) != acumcamasEspOcupPed )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Pediátricas Especiales OCUPADAS + Camas Pediátricas Especiales OCUPADAS COVID ) debe ser igual a Cantidad de camas con gases ocupadas para Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});
			$("#camasEspOcupPed").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspOcupPed = parseFloat($("#camasEspOcupPed").val()) + parseFloat($("#camasEspOcupPedCovid").val());
	        		console.log('suma: '+acumcamasEspOcupPed+' uti_gas_unavailable_children_count: '+$("#uti_gas_unavailable_children_count").val());

	        		if( parseFloat($("#uti_gas_unavailable_children_count").val()) != acumcamasEspOcupPed   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Pediátricas Especiales OCUPADAS + Camas Pediátricas Especiales OCUPADAS COVID ) debe ser igual a Cantidad de camas con gases ocupadas para Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});
			$("#camasEspOcupPedCovid").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasEspOcupPed = parseFloat($("#camasEspOcupPed").val()) + parseFloat($("#camasEspOcupPedCovid").val());
	        		console.log('suma: '+acumcamasEspOcupPed+' uti_gas_unavailable_children_count: '+$("#uti_gas_unavailable_children_count").val());
	        		
	        		if( parseFloat($("#uti_gas_unavailable_children_count").val()) != acumcamasEspOcupPed   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de (Camas Pediátricas Especiales OCUPADAS + Camas Pediátricas Especiales OCUPADAS COVID ) debe ser igual a Cantidad de camas con gases ocupadas para Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});
			//FIN VALIDACION
			//validacion Camas Adultos Críticas OCUPADAS, Camas Adultos Críticas OCUPADAS COVID, Cantidad de internados total en UTI adultos
			$("#uti_hospitalized_adult_count").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritOcupAdult = parseFloat($("#camasCritOcupAdult").val()) + parseFloat($("#camasCritOcupAdultCovid").val());
	        		console.log('suma: '+acumcamasCritOcupAdult+' uti_hospitalized_adult_count: '+$("#uti_hospitalized_adult_count").val());

	        		if( parseFloat($("#uti_hospitalized_adult_count").val()) != acumcamasCritOcupAdult   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de ( Camas Adultos Críticas OCUPADAS + Camas Adultos Críticas OCUPADAS COVID )debe ser igual a Cantidad de internados total en UTI adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});
			$("#camasCritOcupAdult").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritOcupAdult = parseFloat($("#camasCritOcupAdult").val()) + parseFloat($("#camasCritOcupAdultCovid").val());
	        		console.log('suma: '+acumcamasCritOcupAdult+' uti_hospitalized_adult_count: '+$("#uti_hospitalized_adult_count").val());
	        		if(parseFloat($("#uti_hospitalized_adult_count").val()) != acumcamasCritOcupAdult   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de ( Camas Adultos Críticas OCUPADAS + Camas Adultos Críticas OCUPADAS COVID )debe ser igual a Cantidad de internados total en UTI adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});
			$("#camasCritOcupAdultCovid").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritOcupAdult = parseFloat($("#camasCritOcupAdult").val()) + parseFloat($("#camasCritOcupAdultCovid").val());
	        		console.log('suma: '+acumcamasCritOcupAdult+' uti_hospitalized_adult_count: '+$("#uti_hospitalized_adult_count").val());
	        		if( parseFloat($("#uti_hospitalized_adult_count").val()) != acumcamasCritOcupAdult   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de ( Camas Adultos Críticas OCUPADAS + Camas Adultos Críticas OCUPADAS COVID )debe ser igual a Cantidad de internados total en UTI adultos</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});
			//FIN VALIDACION
			//validacion Disponibilidad de Camas Pediátricas Críticas OCUPADAS, Disponibilidad de Camas Pediátricas Críticas OCUPADAS COVID, Cantidad de internados total en UTI Pediatría
			$("#uti_hospitalized_children_count").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritOcupPed = parseFloat($("#camasCritOcupPed").val()) + parseFloat($("#camasCritOcupPedCovid").val());
	        		console.log('suma: '+acumcamasCritOcupPed+' uti_hospitalized_children_count: '+$("#uti_hospitalized_children_count").val());

	        		if( parseFloat($("#uti_hospitalized_children_count").val()) != acumcamasCritOcupPed   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de ( Camas Pediátricas Críticas OCUPADAS + Camas Pediátricas Críticas OCUPADAS COVID ) debe ser igual a Cantidad de internados total en UTI Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});
			$("#camasCritOcupPed").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritOcupPed = parseFloat($("#camasCritOcupPed").val()) + parseFloat($("#camasCritOcupPedCovid").val());
	        		console.log('suma: '+acumcamasCritOcupPed+' uti_hospitalized_children_count: '+$("#uti_hospitalized_children_count").val());

	        		if( parseFloat($("#uti_hospitalized_children_count").val()) != acumcamasCritOcupPed   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de ( Camas Pediátricas Críticas OCUPADAS + Camas Pediátricas Críticas OCUPADAS COVID ) debe ser igual a Cantidad de internados total en UTI Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});
			$("#camasCritOcupPedCovid").change(function() {
	        	$("#content_alert").empty();
	        	//console.log('respiradoresadultos');
	        	if(($("#corresponde_uti").val() == 1) && ($("#corresponde_camas").val() == 1) )
	        	{
	        		var acumcamasCritOcupPed = parseFloat($("#camasCritOcupPed").val()) + parseFloat($("#camasCritOcupPedCovid").val());
	        		console.log('suma: '+acumcamasCritOcupPed+' uti_hospitalized_children_count: '+$("#uti_hospitalized_children_count").val());

	        		if( parseFloat($("#uti_hospitalized_children_count").val()) != acumcamasCritOcupPed   )
	        		{
	        			$("#button_cargar").hide();
	        			$("#content_alert").append('<div id="alert_resp" class="alert alert-secondary mb-4" role="alert"> <strong>La suma de Disponibilidad de ( Camas Pediátricas Críticas OCUPADAS + Camas Pediátricas Críticas OCUPADAS COVID ) debe ser igual a Cantidad de internados total en UTI Pediatría</strong></div>');
	        		}
	        		else
		        	{
		        		$("#button_cargar").show();
		        	}
	        	}
	        	
			});
			//fin validacion
			//fin de validacion d suma de respiradores
			function cambio_fecha(msg) {
			 //   console.log('a');
			    if( $('select[id=nombreestablecimiento]').val() != '') 
			    {
			    	console.log('no esta vacio');
			    	//console.log($('select[id=nombreestablecimiento]').val());
			    	buscar_parte_diario();
			    	reset_inputs();
			    }
			    
			}
        	$(".sumped").change(function() {
	        	var totped = 0;
	            $('.sumped').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totped = totped + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totped").html(totped);
	            $("#totpedhid").val(totped);
	            totalesConsulta();
	        });

        	$(".sumcm").change(function() {
	        	var totcm = 0;
	            $('.sumcm').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totcm = totcm + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totcm").html(totcm);
	            $("#totcmhid").val(totcm);
	            totalesConsulta();
	        });

        	$(".sumguar").change(function() {
	        	var totguar = 0;
	            $('.sumguar').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totguar = totguar + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totguar").html(totguar);
	            $("#totguarhid").val(totguar);
	            totalesConsulta();
	        });

	        $(".sumcovid").change(function() {
	        	var totcovid = 0;
	            $('.sumcovid').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totcovid = totcovid + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totcovid").html(totcovid);
	            $("#totcovidhid").val(totcovid);
	            totalesConsulta();
	        });

        	$(".sumenf1").change(function() {
	        	var totenf1 = 0;
	            $('.sumenf1').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf1 = totenf1 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf1").html(totenf1);
	            $("#totenf1hid").val(totenf1);
	            totalesEnfermedad();
	        });

        	$(".sumenf2").change(function() {
	        	var totenf2 = 0;
	            $('.sumenf2').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf2 = totenf2 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf2").html(totenf2);
	            $("#totenf2hid").val(totenf2);
	            totalesEnfermedad();
	        });

        	$(".sumenf3").change(function() {
	        	var totenf3 = 0;
	            $('.sumenf3').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf3 = totenf3 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf3").html(totenf3);
	            $("#totenf3hid").val(totenf3);
	            totalesEnfermedad();
	        });

        	$(".sumenf4").change(function() {
	        	var totenf4 = 0;
	            $('.sumenf4').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf4 = totenf4 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf4").html(totenf4);
	            $("#totenf4hid").val(totenf4);
	            totalesEnfermedad();
	        });

        	$(".sumenf5").change(function() {
	        	var totenf5 = 0;
	            $('.sumenf5').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf5 = totenf5 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf5").html(totenf5);
	            $("#totenf5hid").val(totenf5);
	            totalesEnfermedad();
	        });

        	$(".sumenf6").change(function() {
	        	var totenf6 = 0;
	            $('.sumenf6').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf6 = totenf6 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf6").html(totenf6);
	            $("#totenf6hid").val(totenf6);
	            totalesEnfermedad();
	        });

        	$(".sumenf7").change(function() {
	        	var totenf7 = 0;
	            $('.sumenf7').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf7 = totenf7 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf7").html(totenf7);
	            $("#totenf7hid").val(totenf7);
	            totalesEnfermedad();
	        });

        	$(".sumenf8").change(function() {
	        	var totenf8 = 0;
	            $('.sumenf8').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf8 = totenf8 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf8").html(totenf8);
	            $("#totenf8hid").val(totenf8);
	            totalesEnfermedad();
	        });

        	$(".sumenf9").change(function() {
	        	var totenf9 = 0;
	            $('.sumenf9').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf9 = totenf9 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf9").html(totenf9);
	            $("#totenf9hid").val(totenf9);
	            totalesEnfermedad();
	        });

        	$(".sumenf10").change(function() {
	        	var totenf10 = 0;
	            $('.sumenf10').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf10 = totenf10 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf10").html(totenf10);
	            $("#totenf10hid").val(totenf10);
	            totalesEnfermedad();
	        });

        
        	$(".icant").on('focus click touchstart', function(e){
        		$(this).get(0).setSelectionRange(0,9999);
        		e.preventDefault();
        	});
        
        	$(".icant").off('keyup').on('keyup', function(e) {
        	    if (e.which === 13) {
        	    	
        	    	e.preventDefault();
    				$(this).next().find('input[type=text]').focus();
        	    }
			});

    		$(".icant").keypress(function (e) {
    			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    				// $("#errmsg").html("Solo numeros").stop().show().fadeOut("slow");
    				return false;
    			}
    		});

    		$("#nombreestablecimiento").select2({
    			placeholder: "Buscar Centro de Salud",
    			minimumInputLength: 3,
    			theme: "bootstrap4",
    			ajax: {
			        url: "ajax_establecimientos.php",
			        dataType: 'json',
			        delay: 250,
			        type: "post",
			        quietMillis: 50,
			        data: function (term) {
			            return {
			                term: term
			            };
			        },
			        processResults: function (data) {
			        	return {
	                    	results: $.map(data, function (item) {
	                        	return {
	                            	text: item.nombre,
	                                id: item.cod_establecimiento
	                            }
	                        })
	                    };
	                },
			        cache: true
			    }
    		});
    		$("#campofecha").datepicker({
    			useCurrent:false,
    		    autoclose: true,
    		    todayHighlight: true,
    		    format: "dd/mm/yyyy",
    		    language: "es",
    		    orientation: "auto",
    		    //minDate: "3M",
    		    endDate: new Date(),
    		    startDate: new Date('2020-03-17'),
			    onSelect: function(dateText) {
			      display("Selected date: " + dateText + ", Current Selected Value= " + this.value);
			      $(this).change();
			    }
			  }).on("change", function() {
			    cambio_fecha("Change event");
			}).datepicker("setDate",'now');

			$("#div_campo_fecha").change(function(e){
                	//alert('a');
                                    cambio_fecha("Change event");
                                });

			$(".selenfermedad").select2({
    			placeholder: "Enfermedad (Nombre/COD)",
    			minimumInputLength: 2,
    			theme: "bootstrap4",
    			width: "200px",
    			ajax: {
			        url: "ajax_enfermedad.php",
			        dataType: 'json',
			        delay: 250,
			        type: "post",
			        quietMillis: 50,
			        data: function (term) {
			            return {
			                term: term
			            };
			        },
			        processResults: function (data) {
			        	return {
	                    	results: $.map(data, function (item) {
	                        	return {
	                            	text: item.nombre,
	                                id: item.codigo
	                            }
	                        })
	                    };
	                },
			        cache: true
			    }
    		});

    		Parsley.addMessages('es', {
    			defaultMessage: "Este valor parece ser inválido.",
    			type: {
    				email: "Este valor debe ser un correo válido.",
    				url: "Este valor debe ser una URL válida.",
    				number: "Este valor debe ser un número válido.",
    				integer: "Este valor debe ser un número válido.",
    				digits: "Este valor debe ser un dígito válido.",
    				alphanum: "Este valor debe ser alfanumérico."
    			},
    			notblank: "Este campo no debe estar en blanco.",
    			required: "Este campo es obligatorio.",
    			pattern: "Este valor es incorrecto.",
    			min: "Este valor no debe ser menor que %s.",
    			max: "Este valor no debe ser mayor que %s.",
    			range: "Este valor debe estar entre %s y %s.",
    			minlength: "Este valor es muy corto. La longitud mínima es de %s caracteres.",
    			maxlength: "Este valor es muy largo. La longitud máxima es de %s caracteres.",
    			length: "La longitud de este valor ingresado debe estar entre %s y %s caracteres.",
    			mincheck: "Debe seleccionar al menos %s opciones.",
    			maxcheck: "Debe seleccionar %s opciones o menos.",
    			check: "Debe seleccionar entre %s y %s opciones.",
    			equalto: "Este valor debe ser idéntico."
    		});
    		Parsley.setLocale('es');		

    		$("#formparte").parsley({
    		    errorClass: 'is-invalid',
    		    successClass: 'is-valid',
    		    classHandler: function(ParsleyField) {
    		        return ParsleyField.$element.parents('.controls');
    		    },
    		    errorsContainer: function(ParsleyField) {
    		        return ParsleyField.$element.parents('.controls');
    		    },
    		    errorsWrapper: '<span class="text-danger text-help">',
    		    errorTemplate: '<div></div>',
    		    trigger: 'change'
    		});  
    		
    		
    		
    		$('#nombreestablecimiento').change(function() 
  			{
  				if( $('select[id=nombreestablecimiento]').val() != '') 
			    {
			    	//console.log('no esta vacio');
			    	//console.log($('select[id=nombreestablecimiento]').val());
			    	reset_inputs();
			    	buscar_parte_diario();
			    	buscar_formularios_x_centro_salud();
			    }
  			});
    		
    	});
 
 $( "#formparte" ).submit(function( event ) {
  if ( validar_campos_uti_contra_camas() == 1 ) {
    //$( "span" ).text( "Validated..." ).show();
    //return;
  }
  else
  {
  	event.preventDefault();
  }
 
  //$( "span" ).text( "Not valid!" ).show().fadeOut( 1000 );
  //
});
</script>