-- SEQUENCE: partediario.enfermedades_id_seq

-- DROP SEQUENCE partediario.enfermedades_id_seq;

CREATE SEQUENCE partediario.uti_adultos_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE partediario.uti_adultos_id_seq
    OWNER TO postgres;

-- Table: partediario.parte_uti_adultos

-- DROP TABLE partediario.parte_uti_adultos;

CREATE TABLE partediario.parte_uti_adultos
(
	id integer NOT NULL DEFAULT nextval('partediario.uti_adultos_id_seq'::regclass),
    id_parte integer,
    id_parte_nacion integer,
    respirators_allocated_adult integer,
    respirators_available_adult_count integer,
    respirators_unavailable_adult_count integer,
    uti_allocated_adult integer,
    uti_allocated_adult_gas integer,
    uti_discharged_adult_count integer,
    uti_discharged_dead_adult_count integer,
    uti_discharged_derivative_adult_count integer,
    uti_gas_available_adult_count integer,
    uti_gas_unavailable_adult_count integer,
    uti_hospitalized_adult_count integer
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE partediario.parte_uti_adultos
    OWNER to postgres;

-- SEQUENCE: partediario.enfermedades_id_seq

-- DROP SEQUENCE partediario.enfermedades_id_seq;

CREATE SEQUENCE partediario.uti_pediatrico_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE partediario.uti_pediatrico_id_seq
    OWNER TO postgres;

-- Table: partediario.parte_uti_pediatrico

-- DROP TABLE partediario.parte_uti_pediatrico;

CREATE TABLE partediario.parte_uti_pediatrico
(
	id integer NOT NULL DEFAULT nextval('partediario.uti_pediatrico_id_seq'::regclass),
    id_parte integer,
    id_parte_nacion integer,
    respirators_allocated_children integer,
    respirators_available_children_count integer,
    respirators_unavailable_children_count integer,
    uti_allocated_children integer,
    uti_allocated_children_gas integer,
    uti_discharged_children_count integer,
    uti_discharged_dead_children_count integer,
    uti_discharged_derivative_children_count integer,
    uti_gas_available_children_count integer,
    uti_gas_unavailable_children_count integer,
    uti_hospitalized_children_count integer
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE partediario.parte_uti_pediatrico
    OWNER to postgres;


CREATE TABLE partediario.jobs_reporte
(
  id integer,
  fecha text
)
WITH (
  OIDS=FALSE
);
ALTER TABLE partediario.jobs_reporte
  OWNER TO e_becerra;

CREATE SEQUENCE partediario.jobs_reporte_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE partediario.jobs_reporte_id_seq
    OWNER TO e_becerra;

CREATE TABLE partediario.jobs_detalles
(
  id integer,
  id_job integer,
  id_establecimiento_nacion integer,
  id_uti_adulto integer,
  id_uti_pediatria integer,
  nombre_establecimiento text,
  estado_job text
)
WITH (
  OIDS=FALSE
);
ALTER TABLE partediario.jobs_detalles
  OWNER TO e_becerra;

CREATE SEQUENCE partediario.jobs_detalles_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE partediario.jobs_detalles_id_seq
    OWNER TO e_becerra;

CREATE SEQUENCE partediario.users_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE partediario.users_id_seq
    OWNER TO e_becerra;
    
-- Table: partediario.users

-- DROP TABLE partediario.users;

CREATE TABLE partediario.users
(
    id integer NOT NULL DEFAULT nextval('partediario.users_id_seq'::regclass),
    documento text COLLATE pg_catalog."default",
    CONSTRAINT users_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE partediario.users
 OWNER TO e_becerra;