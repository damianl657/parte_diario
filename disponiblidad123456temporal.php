<?php
	//este archivo es para el formulario de derivaciones de pacientes
	// Conecta a PostgreSQL
	require 'classPgSql.php';
	$pg = new PgSql();
	
	$sql = "SELECT id, cod_establecimiento, nombre 
			FROM partediario.establecimientos
			WHERE mostrar = '1'";
	$json = [];
	?>
	<table width="100%" border="1" cellpadding="1" cellspacing="0" bordercolor="#000000">
		<thead>
			<tr>
				<th>CAPS</th>
				<th>Ultimo Parte</th>
				<th>Resp</th>
				<th>GenDispAdult</th>
				<th>GenOcupAdult</th>
				<th>EspDispAdult</th>
				<th>EspOcupAdult</th>
				<th>CritDispAdult</th>
				<th>CritOcupAdult</th>
				<th>GenDispPed</th>
				<th>GenOcupPed</th>
				<th>EspDispPed</th>
				<th>EspOcupPed</th>
				<th>CritDispPed</th>
				<th>CritOcupPed</th>
				<th>GenDispAdultCovid</th>
				<th>GenOcupAdultCovid</th>
				<th>EspDispAdultCovid</th>
				<th>EspOcupAdultCovid</th>
				<th>CritDispAdultCovid</th>
				<th>CritOcupAdultCovid</th>
				<th>GenDispPedCovid</th>
				<th>GenOcupPedCovid</th>
				<th>EspDispPedCovid</th>
				<th>EspOcupPedCovid</th>
				<th>CritDispPedCovid</th>
				<th>CritOcupPedCovid</th>
			</tr>
		</thead>
		<tbody>
			
		
	
	<?
	foreach($pg->getRows($sql) as $row) 
	{

		/*$respiradoresdisponibles = $_POST["respiradoresdisponibles"];
		$camasGenDispAdult= $_POST["camasGenDispAdult"];
		$camasGenOcupAdult= $_POST["camasGenOcupAdult"];
		$camasEspDispAdult= $_POST["camasEspDispAdult"];
		$camasEspOcupAdult= $_POST["camasEspOcupAdult"];
		$camasCritDispAdult= $_POST["camasCritDispAdult"];
		$camasCritOcupAdult= $_POST["camasCritOcupAdult"];
		$camasGenDispPed= $_POST["camasGenDispPed"];
		$camasGenOcupPed= $_POST["camasGenOcupPed"];
		$camasEspDispPed= $_POST["camasEspDispPed"];
		$camasEspOcupPed= $_POST["camasEspOcupPed"];
		$camasCritDispPed= $_POST["camasCritDispPed"];
		$camasCritOcupPed= $_POST["camasCritOcupPed"];
		$camasGenDispAdultCovid= $_POST["camasGenDispAdultCovid"];
		$camasGenOcupAdultCovid= $_POST["camasGenOcupAdultCovid"];
		$camasEspDispAdultCovid= $_POST["camasEspDispAdultCovid"];
		$camasEspOcupAdultCovid= $_POST["camasEspOcupAdultCovid"];
		$camasCritDispAdultCovid= $_POST["camasCritDispAdultCovid"];
		$camasCritOcupAdultCovid= $_POST["camasCritOcupAdultCovid"];
		$camasGenDispPedCovid= $_POST["camasGenDispPedCovid"];
		$camasGenOcupPedCovid= $_POST["camasGenOcupPedCovid"];
		$camasEspDispPedCovid= $_POST["camasEspDispPedCovid"];
		$camasEspOcupPedCovid= $_POST["camasEspOcupPedCovid"];
		$camasCritDispPedCovid= $_POST["camasCritDispPedCovid"];
		$camasCritOcupPedCovid= $_POST["camasCritOcupPedCovid"];*/
		$sql_parte = "
		SELECT parte.*, establecimientos.nombre
			FROM partediario.parte
			join partediario.establecimientos on establecimientos.cod_establecimiento = parte.cod_establecimiento
			WHERE parte.cod_establecimiento = '$row->cod_establecimiento' ORDER BY parte.idparte DESC LIMIT 1";
		$ultimo_parte =$pg->getRow($sql_parte);
		
				
		?>
			<tr>
				<td><?php echo $row->nombre; ?></td>
				<?php
					if(isset($ultimo_parte->idparte))
					{
						?>
							<td><?php echo $ultimo_parte->fecha_carga; ?></td>
							<td><?php echo $ultimo_parte->respiradores; ?></td>
							<td><?php echo $ultimo_parte->camasGenDispAdult; ?></td>
							<td><?php echo $ultimo_parte->camasGenOcupAdult; ?></td>
							<td><?php echo $ultimo_parte->camasEspDispAdult; ?></td>
							<td><?php echo $ultimo_parte->camasEspOcupAdult; ?></td>
							<td><?php echo $ultimo_parte->camasCritDispAdult; ?></td>
							<td><?php echo $ultimo_parte->camasCritOcupAdult; ?></td>
							<td><?php echo $ultimo_parte->camasGenDispPed; ?></td>
							<td><?php echo $ultimo_parte->camasGenOcupPed; ?></td>
							<td><?php echo $ultimo_parte->camasEspDispPed; ?></td>
							<td><?php echo $ultimo_parte->camasEspOcupPed; ?></td>
							<td><?php echo $ultimo_parte->camasCritDispPed; ?></td>
							<td><?php echo $ultimo_parte->camasCritOcupPed; ?></td>
							<td><?php echo $ultimo_parte->camasGenDispAdultCovid; ?></td>
							<td><?php echo $ultimo_parte->camasGenOcupAdultCovid; ?></td>
							<td><?php echo $ultimo_parte->camasEspDispAdultCovid; ?></td>
							<td><?php echo $ultimo_parte->camasEspOcupAdultCovid; ?></td>
							<td><?php echo $ultimo_parte->camasCritDispAdultCovid; ?></td>
							<td><?php echo $ultimo_parte->camasCritOcupAdultCovid; ?></td>
							<td><?php echo $ultimo_parte->camasGenDispPedCovid; ?></td>
							<td><?php echo $ultimo_parte->camasGenOcupPedCovid; ?></td>
							<td><?php echo $ultimo_parte->camasEspDispPedCovid; ?></td>
							<td><?php echo $ultimo_parte->camasEspOcupPedCovid; ?></td>
							<td><?php echo $ultimo_parte->camasCritDispPedCovid; ?></td>
							<td><?php echo $ultimo_parte->camasCritOcupPedCovid; ?></td>
						<?php
						
					}
					else
					{
						?>
							<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
							
						<?php
					}
				?>


				
			</tr>
		<?php
		
	}
	//echo json_encode($json);
?>
</tbody>
</table>