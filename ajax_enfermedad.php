<?php
	// Conecta a PostgreSQL
	require 'classPgSql.php';
	$pg = new PgSql();

	$busca = $_POST["term"]["term"];
	
	$sql = "
		SELECT id, codigo, nombre 
			FROM partediario.enfermedades
			WHERE ((nombre ILIKE '%$busca%') OR (codigo ILIKE '%$busca%')) AND (mostrar = '1')
			LIMIT 50
	";
	$json = [];
	foreach($pg->getRows($sql) as $row) {
		$json[] = ['codigo'=>$row->codigo, 'nombre'=>$row->codigo.' - '.$row->nombre];
	}
	echo json_encode($json);
?>