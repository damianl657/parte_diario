<?php


testProcess();
//$res = ok_parte('pediatrico', 126,250 );
//print_r($res);


//Nacion login Response token
function login($json_param) { 
    
    $url_test = 'https://reportecamas.msal.gov.ar';
    $url_test_port = '444';
    $url_login_route = 'auth/login';


    $url = $url_test.':'.$url_test_port.'/'.$url_login_route;
     
    $payload = json_encode($json_param);
     
    // Prepare new cURL resource
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); //CURLOPT_SSL_VERIFYPEER
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
     
    // Set HTTP Header for POST request 
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($payload))
    );
     
    // Submit the POST request
    $result = curl_exec($ch);
     
    // Close cURL session handle
    curl_close($ch);
    $result = json_decode($result, TRUE);
    return $result;
}

//Service de Damian
function establecimientos(){

    $ch = curl_init('https://covid19salud.sanjuan.gov.ar/service.php?get=establecimientos'); // Initialise cURL
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' ));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($result, TRUE);
    return $result;
}

//Service de Damian detalles
function establecimientos_uti($tipo, $cod_establecimiento){

    $ch = curl_init('https://covid19salud.sanjuan.gov.ar/service.php?get=get_uti_'.$tipo.'&cod_establecimiento='.$cod_establecimiento); // Initialise cURL
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' ));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($result, TRUE);
    return $result;
}

//Nacion Post adult data from local data -> adults
function adults($token,$res_uti_adultos){
    
    
        $data = array(
            "respirators_allocated_adult" => intval($res_uti_adultos['respirators_allocated_adult']),
            "respirators_available_adult_count" => intval($res_uti_adultos['respirators_available_adult_count']),
            "respirators_unavailable_adult_count" => intval($res_uti_adultos['respirators_unavailable_adult_count']),
            "uti_allocated_adult" => intval($res_uti_adultos['uti_allocated_adult']),
            "uti_allocated_adult_gas" => intval($res_uti_adultos['uti_allocated_adult_gas']),
            "uti_discharged_adult_count" => intval($res_uti_adultos['uti_discharged_adult_count']),
            "uti_discharged_dead_adult_count" => intval($res_uti_adultos['uti_discharged_dead_adult_count']) ,
            "uti_discharged_derivative_adult_count" => intval($res_uti_adultos['uti_discharged_derivative_adult_count']),
            "uti_gas_available_adult_count" => intval($res_uti_adultos['uti_gas_available_adult_count']),
            "uti_gas_unavailable_adult_count" => intval($res_uti_adultos['uti_gas_unavailable_adult_count']),
            "uti_hospitalized_adult_count" => intval($res_uti_adultos['uti_hospitalized_adult_count'])
        );  

        
        $authorization = 'Authorization: Bearer '.$token;

        $url_test = 'https://reportecamas.msal.gov.ar';
        $url_test_port = '444';
        $url_login_route = 'api/v1/reports?validation_type=adults';
        $url = $url_test.':'.$url_test_port.'/'.$url_login_route;
    
         
        $payload = json_encode($data,TRUE);

        // Prepare new cURL resource
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); //CURLOPT_SSL_VERIFYPEER
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
         
             
        // Submit the POST request
        $result = curl_exec($ch);
         
        // Close cURL session handle
        curl_close($ch);
        
        return $result;

}

//Nacion Post adult data from local data -> children
function children($token,$res_uti_children){
    
    $data = array(
            "respirators_allocated_children" => intval($res_uti_children['respirators_allocated_children']),
            "respirators_available_children_count" => intval($res_uti_children['respirators_available_children_count']),
            "respirators_unavailable_children_count" => intval($res_uti_children['respirators_unavailable_children_count']),
            "uti_allocated_children" => intval($res_uti_children['uti_allocated_children']),
            "uti_allocated_children_gas" => intval($res_uti_children['uti_allocated_children_gas']),
            "uti_discharged_children_count" => intval($res_uti_children['uti_discharged_children_count']),
            "uti_discharged_dead_children_count" => intval($res_uti_children['uti_discharged_dead_children_count']),
            "uti_discharged_derivative_children_count" => intval($res_uti_children['uti_discharged_derivative_children_count']),
            "uti_gas_available_children_count" => intval($res_uti_children['uti_gas_available_children_count']),
            "uti_gas_unavailable_children_count" => intval($res_uti_children['uti_gas_unavailable_children_count']),
            "uti_hospitalized_children_count" => intval($res_uti_children['uti_hospitalized_children_count'])
    );  


    $authorization = "Authorization: Bearer ".$token;

    $url_test = 'https://reportecamas.msal.gov.ar';
    $url_test_port = '444';
    $url_login_route = 'api/v1/reports?validation_type=children';
    $url = $url_test.':'.$url_test_port.'/'.$url_login_route;
    

     
    $payload = json_encode($data);
     
    // Prepare new cURL resource
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); //CURLOPT_SSL_VERIFYPEER
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
     
    // Submit the POST request
    $result = curl_exec($ch);
     
    // Close cURL session handle
    curl_close($ch);
    
    return $result;

}

function testProcess(){

    $establecimientos = establecimientos();
    
    
    for($i=0; $i < count($establecimientos); $i++)
    {
        $data = array(
            'email' => $establecimientos[$i]['username_colaborador'],
            'password' => $establecimientos[$i]['pass_colaborador']
            );

                        
            $res_uti_adultos = establecimientos_uti('adultos', $establecimientos[$i]['cod_establecimiento_sj'], $establecimientos[$i]['idJobDetalle']);

            $token = login($data);
            

            // /* Post a nacion  */
            if(isset($token['access_token'] )) {
                if(is_array($res_uti_adultos))
                {
                    if(count($res_uti_adultos) > 0)
                    {
                        openGuard($token['access_token']);
                        $res_adults = adults($token['access_token'], $res_uti_adultos);
                        $res_adults = json_decode($res_adults,true);
                        
                        //Enviar a damian
                        //if(isset($res_adults['report'])) {
                        $res = ok_parte('adulto', $res_uti_adultos['id'],$res_adults['report']['id'], $establecimientos[$i]['idJobDetalle'] );
                        //print_r($res);
                        closeGuard($token['access_token']);
                        //}
                    }
                }
            }
            /* Solo si fuera necesaria la espera. */
            //sleep(30);


            $res_uti_pediatrico = establecimientos_uti('pediatrico', $establecimientos[$i]['cod_establecimiento_sj']);
            
            /* Post a nacion */
            if(isset($token['access_token'] )) {
                if(is_array($res_uti_pediatrico))
                {
                    if(count($res_uti_pediatrico) > 0)
                    {
                        openGuard($token['access_token']);
                        $res_children = children($token['access_token'], $res_uti_pediatrico);
                        $res_children = json_decode($res_children,true);

                        //Enviar a damian
                        //if(isset($res_children['report'])){
                        $res= ok_parte('pediatrico', $res_uti_pediatrico['id'], $res_children['report']['id'], $establecimientos[$i]['idJobDetalle'] );
                        //print_r($res);
                        closeGuard($token['access_token']);
                        //}
                    }
                }
            }
    }

}


function ok_parte($tipo, $idutisj, $idutinc,$idJobDetalle) {

    
    $ch = curl_init('https://covid19salud.sanjuan.gov.ar/service.php?get=ok_parte&idutisj='.$idutisj.'&idutinc='.$idutinc.'&tipo='.$tipo.'&idJobDetalle='.$idJobDetalle); // Initialise cURL

    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($result);
    return $result;

}

function openGuard($token) {
    
    $authorization = 'Authorization: Bearer '.$token;

    $url_test = 'https://reportecamas.msal.gov.ar';
    $url_test_port = '444';
    $url_login_route = 'api/v1/guards';
    $url = $url_test.':'.$url_test_port.'/'.$url_login_route;


    // Prepare new cURL resource
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_POST, true);
    //curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); //CURLOPT_SSL_VERIFYPEER
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
     
         
    // Submit the POST request
    $result = curl_exec($ch);
    
    if($result === false)
    {
        echo 'Curl error: ' . curl_error($ch);
    }
    else
    {
        //echo 'Operación completada sin errores';
    }

    // Close cURL session handle
    curl_close($ch);
    
    return $result;
}

function closeGuard($token) {

    $authorization = 'Authorization: Bearer '.$token;

    $url_test = 'https://reportecamas.msal.gov.ar';
    $url_test_port = '444';
    $url_login_route = 'api/v1/guards';
    $url = $url_test.':'.$url_test_port.'/'.$url_login_route;


    // Prepare new cURL resource
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    //curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); //CURLOPT_SSL_VERIFYPEER
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
     
         
    // Submit the POST request
    $result = curl_exec($ch);
    
    if($result === false)
    {
        echo 'Curl error: ' . curl_error($ch);
    }
    else
    {
        //echo 'Operación completada sin errores';
    }

    // Close cURL session handle
    curl_close($ch);
    
    return $result;

}