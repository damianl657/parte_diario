<?php include('views/header.php');?>
<body>

	<header>
	    <nav class="navbar navbar-light navbar-expand-md fixed-top">
	        <div class="container">
	            <a href="http://www.sanjuan.gob.ar" class="navbar-brand padre-img1">
	                <img src="img/logo.png" alt="Isologo Gobierno de San Juan" width="150">
	            </a>
	            <a href="http://salud.sanjuan.gob.ar" class="navbar-brand padre-img2">
	                <img src="img/logosalud.png" alt="Ministerio de Salud" width="150" class="img2">
	            </a>
	            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#menu-principal"
	                aria-controls="menu-principal" aria-expanded="false" aria-label="Desplegar menú de navegación">
	                <span class="navbar-toggler-icon"></span>
	            </button>
	            <div class="collapse navbar-collapse" id="menu-principal">
	                <ul class="navbar-nav ml-auto">  
	                    <!--<li class="nav-item"><a href="#" class="nav-link active">Inicio</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 1</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 2</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 3</a></li>
	                    -->
	                </ul>
	            </div>
	        </div>
	    </nav>
	</header>
	<main role="main" style="display: block;width: 100%;">
	    <section class="jumbotron jumbotron-home text-light text-center" 
	    		style="background-image:linear-gradient(rgba(89,19,28,0.9),rgba(0,0,0,0)),url('img/doctor.jpg')">
	        <div class="container">
	            <nav class="row">
	                <div class="col-12 col-md-8 mx-auto"><!--col-md-8 m-auto-->
	                    <h3 class="pb-2">Parte diario de Consultas e Internación</h3>
	                </div>
	            </nav>
	        </div>
	    </section>
	</main> 
	<!-- Button trigger modal -->
	
    <div class="container-fluid" id="sy-contenido">
        <section class="container">
        	<br>
        	<!--<div class="alert alert-secondary mb-4" role="alert">
				 <strong><h1>ESTE SITIO ESTA SIENDO ACTUALIZADO EN ESTE MOMENTO. ESPERE UNOS MINUTOS POR FAVOR</h1></strong>
			</div>-->
			<div class="alert alert-secondary mb-4" role="alert">
				Bienvenido: <strong><?php echo $_GET["cidNombre"]; ?></strong>
			</div>
			<h4 class="text-center">Complete el siguiente formulario</h4>
			<?php				
				// Conecta a PostgreSQL
				/*require 'classPgSql.php';
				$pg = new PgSql();
				
				
				$sql = "SELECT * FROM partediario.enfermedades";
				foreach($pg->getRows($sql) as $row) {
				    echo $row->codigo." - ".$row->nombre;
				    echo '<br>';
				}
				echo '<br>';*/
				
			?>
			<p class="mb-4">&nbsp;</p>
			<form name="formparte" id="formparte" method="post" action="cargar_d.php">
				<input type="hidden" id="users_cd" name="users_cd" value="0" />
				<input type="hidden" id="corresponde_uti" name="corresponde_uti" value="0" />
				<input type="hidden" id="corresponde_camas" name="corresponde_camas" value="0" />
				<div class="form-row">
						
					<div class="col-lg-6">
						<div class="form-row">
							<div class="alert alert-success" role="alert">
							Ingrese el Nombre del Establecimiento
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-row">
							<!--<button type="button" id="button_abrir_modal_job" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalScrollable">
						  Partes Enviados a Nación
						</button>  -->
						</div>
					</div>
				


				</div>
				<div class="form-row">
					<div class="form-group col-md-2" id="div_campo_fecha">
						<label for="campofecha">Fecha: (dd/mm/aaaa)</label>
						<input readonly  type="text" class="form-control" id="campofecha" name="campofecha" data-parsley-required="true" data-parsley-maxlength="12">
					</div>
					<div class="form-group col-md-8">
						<label>Nombre del Establecimiento:</label>
						<select class="form-control" name="nombreestablecimiento" id="nombreestablecimiento" data-parsley-required="true">
							<option value=""></option>
						</select>
					</div>
					<div class="form-group col-md-2" id="div_respiradoresdisponibles">
						<label>Resp. disponibles:</label>
						<input type="number" class=" form-control" 
								 name="respiradoresdisponibles" id="respiradoresdisponibles">
					</div>
				</div>
				<div class="form-row" id="div_partes_cargados">
				</div>
				<?php //include('views/views_job.php');?>
				<?php include('views/views_form_camas_comunes.php');?>
				<?php include('views/views_form_uti_adultos.php');?>
				<?php include('views/views_form_uti_pediatrico.php');?>
				<?php include('views/views_form_consultas_dia_informado.php');?>
				<?php include('views/views_form_internados.php');?>
				<div class="form-row" >
					<div class="form-group col-md-12">
						<button type="submit" id="button_cargar" class="btn btn-primary">Cargar Información</button>
					</div>						
				</div>
				<div class="form-row" id="content_alert">
										
				</div>

				<div class="form-row">
					<p>&nbsp;</p>
				</div>
			</form>
		</section>
	</div>
</body>
<?php include('views/footer.php');?>
<?php include('token_CD.php');?>


