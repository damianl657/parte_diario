<script>
	var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwZXJzb25hIjoiMzQ5MTYzMzAuRE5JLk0iLCJkYXRhIjoie1wibm9tYnJlX2FwZWxsaWRvXCI6XCJWaWN0b3IgRGFtaWFuIExFQUxcIn0iLCJpc3MiOiJhdXRoMCIsImV4cCI6MTU4NTkzMDYxMH0.yz5bUsfa0HN48YVyTEm2nMovB6tLgKVHDucIMPbzEEo';


var users_cd = jwt_decode(token);

if(users_cd)
{
	if(users_cd.persona)
	{
		//console.log(users_cd.persona);
		var arre = Array();
        arre=users_cd.persona.split('.');
        var dni = arre[0];
        console.log(dni);
        $('#users_cd').val(dni);
        $.ajax({
			          	url: "ajax_permiso_job.php",
				        dataType: 'json',
				        delay: 250,
				        type: "GET",
			          
			          	data:{documento:dni},
			          	beforeSend: function() 
			          	{
							
						},
						success: function(data)
				        {
				        	if(data.status == 1)
				          	{
				          		var result = data.result;
				          		if(result.documento)
				          		{
				          			//console.log('si');
				          			traer_jobs_registrados();
				          		}
				          		else
				          		{
				          			//console.log('no');
				          		}
				          	
				          	}
				        }
				});
	}
}
function traer_jobs_detalle(id)
{
	$('#table_job_x_id_detalle').empty();
	$('#div_job_registrados').hide();
	$('#div_job_x_id_detalle').show();
	$.ajax({
			    url: "ajax_traer_jobs_detalle.php",
				dataType: 'json',
				delay: 250,
				type: "GET",
			          
			    data:{id:id},
			    beforeSend: function() 
			    {
							
					
				},
				success: function(data)
				{
					if(data.status == 1)
				    {
				        var jobs_detalle = data.jobs_detalle;
				        if(jobs_detalle)
				        {
				        	var filas = '';
				        	var uti_ped = 'No enviado';
				        	var uti_adult = 'No enviado';
				          	for(var i=0; i < jobs_detalle.length ; i++)
				          	{
				          		if(jobs_detalle[i]['id_uti_adulto'] != null)
				          		{
				          			uti_adult = 'Enviado';
				          		}
				          		if(jobs_detalle[i]['id_uti_pediatria'] != null)
				          		{
				          			uti_ped = 'Enviado';
				          		}
				          		filas = filas + '<tr>'+
								      '<td scope="row">'+jobs_detalle[i]['nombre_establecimiento']+'</td>'+
								      '<td>'+uti_ped+'</td>'+
								      '<td>'+uti_adult+'</td>'+
								    '</tr>';
				          	}
				          	$('#table_job_x_id_detalle').html(filas);
						}
				          	
				    }
				}
			});
}
function ver_all_job()
{
	$('#div_job_registrados').show();
	$('#div_job_x_id_detalle').hide();
	
}
function traer_jobs_registrados()
{
	$('#div_job_x_id_detalle').hide();
	$.ajax({
			    url: "ajax_jobs.php",
				dataType: 'json',
				delay: 250,
				type: "GET",
			          
			    
			    beforeSend: function() 
			    {
							
				},
				success: function(data)
				{
				    if(data.status == 1)
				    {
				        var jobs = data.jobs;
				        if(jobs)
				        {	var filas = '';
				          	for(var i=0; i < jobs.length ; i++)
				          		{
				          			var fecha =  jobs[i]['fecha'].substr(8,2)+'/'+jobs[i]['fecha'].substr(5,2)+'/'+jobs[i]['fecha'].substr(0,4)+' - '+jobs[i]['fecha'].substr(11,5);
				          			
				          			//console.log(partes[i]);
				          			filas = filas + '<tr>'+
								      '<th scope="row">'+jobs[i]['id']+'</th>'+
								      '<td>'+fecha+'</td>'+
								     
								      
								      '<td><strong> <a class="btn btn-outline-info" onclick="traer_jobs_detalle('+jobs[i]['id']+')">Ver Parte</a> </strong></td>'+
								/*     '<td><strong> <a class="btn btn-outline-info" href="consulta_parte/'+partes[i]['idparte']+'" target="_blank" >Ver Parte</a> </strong></td>'+*/
								    '</tr>';

								    

				          		}
				          		
				          		$('#tbody_job').html(filas);
				        }
				        else
				        {
				          		
				        }
				          	
				    }
				}
			});
}

</script>