<?php
	$url_redirect="https://autenticar.sanjuan.gob.ar/?callback=ukrl1gWrDigzKWwmrIGjbnuvF4C6iGCGhHqX69SSSZxafWa6o8hyqYjaosY8MtATbZBYYXj9BhyPZPnAMf384AUx4dqKNOBJiJYXNIspZdxQJ1rPuVnIf1bBMA4h3mIokHKNMcm7gqtK3%2F9Fq1FEVQhOrLsM2ndPk%2F4gavvTM8E%3D";
	/*if((!isset($_GET["sessionToken"])) || (empty($_GET["sessionToken"]))) {
		// Hago el redirect al login
		header("HTTP/1.1 302 Moved Temporarily");
		header("Location: $url_redirect"); 
		exit();
	}*/

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <title>Ministerio de Salud - Parte Diario de Consultas e Internación</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link type="text/css" rel="stylesheet" href="css/custom-bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/patron.css">
    <link type="text/css" rel="stylesheet" href="css/patron-iconos.css">
    <link rel="stylesheet" type="text/css" href="css/salud/estilo_salud.css" />
    <link href="bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Parte Diario de Consultas e Internación - Ministerio de Salud de San Juan">
    <meta name="keywords" content="salud, parte diario, consultas, internaciones, gobierno, san juan">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js" type="text/javascript" charset="utf-8" async defer></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" 
    	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    <!-- select2 -->
    <link href="select2/css/select2.min.css" rel="stylesheet" />
    <link href="select2/css/select2-bootstrap4.min.css" rel="stylesheet" />
	<script src="select2/js/select2.min.js"></script>
	<script src="select2/js/i18n/es.js"></script>
	<script src="js/notify.js"></script>

	<style type="text/css" media="screen">
		.select2-container{
			width: 100%!important;
		}
		.select2-search--dropdown .select2-search__field {
			width: 98%;
		}
		.icant {
			text-align: center;
			font-weight: bold;
			font-size:14px;
		}
	</style>
	<script src="js/jwt-decode.js"></script>
</head>

<body>

	<header>
	    <nav class="navbar navbar-light navbar-expand-md fixed-top">
	        <div class="container">
	            <a href="http://www.sanjuan.gob.ar" class="navbar-brand padre-img1">
	                <img src="img/logo.png" alt="Isologo Gobierno de San Juan" width="150">
	            </a>
	            <a href="http://salud.sanjuan.gob.ar" class="navbar-brand padre-img2">
	                <img src="img/logosalud.png" alt="Ministerio de Salud" width="150" class="img2">
	            </a>
	            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#menu-principal"
	                aria-controls="menu-principal" aria-expanded="false" aria-label="Desplegar menú de navegación">
	                <span class="navbar-toggler-icon"></span>
	            </button>
	            <div class="collapse navbar-collapse" id="menu-principal">
	                <ul class="navbar-nav ml-auto">  
	                    <!--<li class="nav-item"><a href="#" class="nav-link active">Inicio</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 1</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 2</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 3</a></li>
	                    -->
	                </ul>
	            </div>
	        </div>
	    </nav>
	</header>
	<main role="main" style="display: block;width: 100%;">
	    <section class="jumbotron jumbotron-home text-light text-center" 
	    		style="background-image:linear-gradient(rgba(89,19,28,0.9),rgba(0,0,0,0)),url('img/doctor.jpg')">
	        <div class="container">
	            <nav class="row">
	                <div class="col-12 col-md-8 mx-auto"><!--col-md-8 m-auto-->
	                    <h3 class="pb-2">Parte diario de Consultas e Internación</h3>
	                </div>
	            </nav>
	        </div>
	    </section>
	</main>    

    <div class="container-fluid" id="sy-contenido">

        <section class="container">

        	<br>
			<div class="alert alert-secondary mb-4" role="alert">
				Bienvenido: <strong><?php echo $_GET["cidNombre"]; ?></strong>
			</div>

			<h4 class="text-center">Complete el siguiente formulario</h4>
			<p class="mb-4">&nbsp;</p>

			<?php				
				// Conecta a PostgreSQL
				require 'classPgSql.php';
				$pg = new PgSql();
				
				/*
				$sql = "SELECT * FROM partediario.enfermedades";
				foreach($pg->getRows($sql) as $row) {
				    echo $row->codigo." - ".$row->nombre;
				    echo '<br>';
				}
				echo '<br>';
				*/
			?>

			<form name="formparte" id="formparte" method="post" action="cargar.php">
				<input type="hidden" id="users_cd" name="users_cd" value="0" />
				<div class="form-row">
					<div class="form-group col-md-2">
						<label for="campofecha">Fecha: (dd/mm/aaaa)</label>
						<input readonly  type="text" class="form-control" id="campofecha" name="campofecha" data-parsley-required="true" data-parsley-maxlength="12">
					</div>
					<div class="form-group col-md-8">
						<label>Nombre del Establecimiento:</label>
						<select class="form-control" name="nombreestablecimiento" id="nombreestablecimiento" data-parsley-required="true">
							<option value=""></option>
						</select>
					</div>
					
					<div class="form-group col-md-2">
						<label>Resp. disponibles:</label>
						<input type="text" class="icant form-control"  
								maxlength="4" data-parsley-type="digits" name="respiradoresdisponibles" id="respiradoresdisponibles">
					</div>
				</div>
						
				<!--<div class="form-group col-md-2">
						<label>Camas disponibles:</label>
						<input type="text" class="icant form-control" required 
								maxlength="4" data-parsley-type="digits" name="camasdisponibles" id="camasdisponibles">
					</div>-->
				<div class="card ">
				  	<div class="card-header bg-primary text-white"><strong><center>Camas Adultos</center></strong></div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="row">
									<div class="col-lg-12">
										<div class="alert alert-info text-center" role="alert">
											DISPONIBLES							  	
										</div>									  	
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Generales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class="icant form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasGenDispAdult" id="camasGenDispAdult">
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Especiales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class="icant form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasEspDispAdult" id="camasEspDispAdult">
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Críticas:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class="icant form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasCritDispAdult" id="camasCritDispAdult">
									</div>									
								</div>

							</div>
							<div class="col-lg-6">
								<div class="row">
									<div class="col-lg-12">
										<div class="alert alert-warning text-center" role="alert">
											OCUPADAS							  	
										</div>									  	
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Generales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class="icant form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasGenOcupAdult" id="camasGenOcupAdult">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Especiales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class="icant form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasEspOcupAdult" id="camasEspOcupAdult">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Críticas:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class="icant form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasCritOcupAdult" id="camasCritOcupAdult">
									</div>								
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				
				<div class="card ">
				  	<div class="card-header bg-success text-white"><strong><center>Camas Pedi&aacute;tricas</center></strong></div>
					<div class="card-body">

						<div class="row">
							<div class="col-lg-6">
								<div class="row">
									<div class="col-lg-12">
										<div class="alert alert-info text-center" role="alert">
											DISPONIBLES							  	
										</div>									  	
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Generales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class="icant form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasGenDispPed" id="camasGenDispPed">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Especiales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class="icant form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasEspDispPed" id="camasEspDispPed">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Críticas:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class="icant form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasCritDispPed" id="camasCritDispPed">
									</div>

								</div>
							  	
							</div>
							<div class="col-lg-6">
							  	<div class="row">
									<div class="col-lg-12">
										<div class="alert alert-warning text-center" role="alert">
											OCUPADAS							  	
										</div>									  	
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Generales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class="icant form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasGenOcupPed" id="camasGenOcupPed">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Especiales:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class="icant form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasEspOcupPed" id="camasEspOcupPed">
									</div>

									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<label>Críticas:</label>
									</div>
									<div class="form-group col-md-2 col-sm-2 col-xs-2">
										<input type="text" class="icant form-control" required 
												maxlength="4" data-parsley-type="digits" name="camasCritOcupPed" id="camasCritOcupPed">
									</div>

								</div>

							</div>
						</div>

					</div>
					
				</div>

				<div class="form-row">
					<div class="form-group col-md-12">
						<div class="alert alert-info mt-3" role="alert">Cantidad de consultas del día informado</div>
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-sm table-striped">
								<thead>
									<tr>
										<th width="120">Servicio donde se realizó la consulta</th>
										<th class="text-center">menores de 1 año</th>
										<th class="text-center">1 a 4 años</th>
										<th class="text-center">5 a 14 años</th>
										<th class="text-center">15 a 19 años</th>
										<th class="text-center">20 a 39 años</th>
										<th class="text-center">40 a 64 años</th>
										<th class="text-center">65 y mas años</th>
										<th class="text-center">total</th>

									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Pediatría</td>
										<td>
											<input type="text" class="icant sumped form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="ped1" id="ped1">
										</td>
										<td>
											<input type="text" class="icant sumped form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="ped14" id="ped14">
										</td>
										<td>
											<input type="text" class="icant sumped form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="ped514" id="ped514">
										</td>
										<td>
											<input type="text" class="icant sumped form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="ped1519" id="ped1519">
										</td>
										<td>
											<input type="text" class="icant sumped form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="ped2039" id="ped2039">
										</td>
										<td>
											<input type="text" class="icant sumped form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="ped4064" id="ped4064">
										</td>
										<td>
											<input type="text" class="icant sumped form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="ped65" id="ped65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totpedhid" id="totpedhid" value="0">
												<strong><span id="totped">0</span></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td>Clínica Médica</td>
										<td>
											<input type="text" class="icant sumcm form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="cm1" id="cm1">
										</td>
										<td>
											<input type="text" class="icant sumcm form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="cm14" id="cm14">
										</td>
										<td>
											<input type="text" class="icant sumcm form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="cm514" id="cm514">
										</td>
										<td>
											<input type="text" class="icant sumcm form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="cm1519" id="cm1519">
										</td>
										<td>
											<input type="text" class="icant sumcm form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="cm2039" id="cm2039">
										</td>
										<td>
											<input type="text" class="icant sumcm form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="cm4064" id="cm4064">
										</td>
										<td>
											<input type="text" class="icant sumcm form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="cm65" id="cm65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcmhid" id="totcmhid" value="0">
												<strong><span id="totcm">0</span></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td>Guardia</td>
										<td>
											<input type="text" class="icant sumguar form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="guar1" id="guar1">
										</td>
										<td>
											<input type="text" class="icant sumguar form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="guar14" id="guar14">
										</td>
										<td>
											<input type="text" class="icant sumguar form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="guar514" id="guar514">
										</td>
										<td>
											<input type="text" class="icant sumguar form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="guar1519" id="guar1519">
										</td>
										<td>
											<input type="text" class="icant sumguar form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="guar2039" id="guar2039">
										</td>
										<td>
											<input type="text" class="icant sumguar form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="guar4064" id="guar4064">
										</td>
										<td>
											<input type="text" class="icant sumguar form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="guar65" id="guar65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totguarhid" id="totguarhid" value="0">
												<strong><span id="totguar">0</span></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td>Total</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcon1hid" id="totcon1hid" value="0">
												<strong><span id="totcon1">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcon14hid" id="totcon14hid" value="0">
												<strong><span id="totcon14">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcon514hid" id="totcon514hid" value="0">
												<strong><span id="totcon514">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcon1519hid" id="totcon1519hid" value="0">
												<strong><span id="totcon1519">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcon2039hid" id="totcon2039hid" value="0">
												<strong><span id="totcon2039">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcon4064hid" id="totcon4064hid" value="0">
												<strong><span id="totcon4064">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totcon65hid" id="totcon65hid" value="0">
												<strong><span id="totcon65">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totconsultashid" id="totconsultashid" value="0">
												<strong><span id="totconsultas">0</span></strong>
											</div>
										</td>
									</tr>
								</tbody>
							</table>							
						</div>
						
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-12">
						<div class="alert alert-info mt-3" role="alert">Cantidad de pacientes internados por enfermedades respiratorias según diagnóstico por grupo etario</div>
					</div>
				</div>

				<div class="form-row">
					<div class="form-group col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered table-sm table-striped">
								<thead>
									<tr>
										<th width="200">Diagnóstico de Internación (Enfermedades respiratorias)</th>
										<th class="text-center">menores de 1 año</th>
										<th class="text-center">1 a 4 años</th>
										<th class="text-center">5 a 14 años</th>
										<th class="text-center">15 a 19 años</th>
										<th class="text-center">20 a 39 años</th>
										<th class="text-center">40 a 64 años</th>
										<th class="text-center">65 y mas años</th>
										<th class="text-center">total</th>

									</tr>
								</thead>
								<tbody>
									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf1" id="selenf1">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf1 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf1_1" id="enf1_1">
										</td>
										<td>
											<input type="text" class="icant sumenf1 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf1_14" id="enf1_14">
										</td>
										<td>
											<input type="text" class="icant sumenf1 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf1_514" id="enf1_514">
										</td>
										<td>
											<input type="text" class="icant sumenf1 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf1_1519" id="enf1_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf1 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf1_2039" id="enf1_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf1 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf1_4064" id="enf1_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf1 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf1_65" id="enf1_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf1hid" id="totenf1hid" value="0">
												<strong><span id="totenf1">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf2" id="selenf2">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf2 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf2_1" id="enf2_1">
										</td>
										<td>
											<input type="text" class="icant sumenf2 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf2_14" id="enf2_14">
										</td>
										<td>
											<input type="text" class="icant sumenf2 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf2_514" id="enf2_514">
										</td>
										<td>
											<input type="text" class="icant sumenf2 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf2_1519" id="enf2_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf2 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf2_2039" id="enf2_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf2 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf2_4064" id="enf2_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf2 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf2_65" id="enf2_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf2hid" id="totenf2hid" value="0">
												<strong><span id="totenf2">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf3" id="selenf3">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf3 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf3_1" id="enf3_1">
										</td>
										<td>
											<input type="text" class="icant sumenf3 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf3_14" id="enf3_14">
										</td>
										<td>
											<input type="text" class="icant sumenf3 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf3_514" id="enf3_514">
										</td>
										<td>
											<input type="text" class="icant sumenf3 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf3_1519" id="enf3_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf3 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf3_2039" id="enf3_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf3 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf3_4064" id="enf3_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf3 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf3_65" id="enf3_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf3hid" id="totenf3hid" value="0">
												<strong><span id="totenf3">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf4" id="selenf4">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf4 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf4_1" id="enf4_1">
										</td>
										<td>
											<input type="text" class="icant sumenf4 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf4_14" id="enf4_14">
										</td>
										<td>
											<input type="text" class="icant sumenf4 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf4_514" id="enf4_514">
										</td>
										<td>
											<input type="text" class="icant sumenf4 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf4_1519" id="enf4_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf4 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf4_2039" id="enf4_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf4 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf4_4064" id="enf4_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf4 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf4_65" id="enf4_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf4hid" id="totenf4hid" value="0">
												<strong><span id="totenf4">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf5" id="selenf5">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf5 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf5_1" id="enf5_1">
										</td>
										<td>
											<input type="text" class="icant sumenf5 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf5_14" id="enf5_14">
										</td>
										<td>
											<input type="text" class="icant sumenf5 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf5_514" id="enf5_514">
										</td>
										<td>
											<input type="text" class="icant sumenf5 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf5_1519" id="enf5_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf5 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf5_2039" id="enf5_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf5 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf5_4064" id="enf5_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf5 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf5_65" id="enf5_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf5hid" id="totenf5hid" value="0">
												<strong><span id="totenf5">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf6" id="selenf6">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf6 sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf6_1" id="enf6_1">
										</td>
										<td>
											<input type="text" class="icant sumenf6 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf6_14" id="enf6_14">
										</td>
										<td>
											<input type="text" class="icant sumenf6 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf6_514" id="enf6_514">
										</td>
										<td>
											<input type="text" class="icant sumenf6 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf6_1519" id="enf6_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf6 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf6_2039" id="enf6_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf6 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf6_4064" id="enf6_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf6 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf6_65" id="enf6_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf6hid" id="totenf6hid" value="0">
												<strong><span id="totenf6">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf7" id="selenf7">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf7_1" id="enf7_1">
										</td>
										<td>
											<input type="text" class="icant sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf7_14" id="enf7_14">
										</td>
										<td>
											<input type="text" class="icant sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf7_514" id="enf7_514">
										</td>
										<td>
											<input type="text" class="icant sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf7_1519" id="enf7_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf7_2039" id="enf7_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf7_4064" id="enf7_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf7 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf7_65" id="enf7_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf7hid" id="totenf7hid" value="0">
												<strong><span id="totenf7">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf8" id="selenf8">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf8 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf8_1" id="enf8_1">
										</td>
										<td>
											<input type="text" class="icant sumenf8 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf8_14" id="enf8_14">
										</td>
										<td>
											<input type="text" class="icant sumenf8 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf8_514" id="enf8_514">
										</td>
										<td>
											<input type="text" class="icant sumenf8 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf8_1519" id="enf8_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf8 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf8_2039" id="enf8_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf8 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf8_4064" id="enf8_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf8 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf8_65" id="enf8_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf8hid" id="totenf8hid" value="0">
												<strong><span id="totenf8">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf9" id="selenf9">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf9 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf9_1" id="enf9_1">
										</td>
										<td>
											<input type="text" class="icant sumenf9 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf9_14" id="enf9_14">
										</td>
										<td>
											<input type="text" class="icant sumenf9 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf9_514" id="enf9_514">
										</td>
										<td>
											<input type="text" class="icant sumenf9 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf9_1519" id="enf9_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf9 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf9_2039" id="enf9_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf9 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf9_4064" id="enf9_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf9 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf9_65" id="enf9_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf9hid" id="totenf9hid" value="0">
												<strong><span id="totenf9">0</span></strong>
											</div>
										</td>
									</tr>

									<tr>
										<td width="300">
											<div style="width:300px;">
												<select class="selenfermedad form-control-sm" name="selenf10" id="selenf10">
													<option value=""></option>
												</select>
											</div>
										</td>
										<td>
											<input type="text" class="icant sumenf10 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf10_1" id="enf10_1">
										</td>
										<td>
											<input type="text" class="icant sumenf10 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf10_14" id="enf10_14">
										</td>
										<td>
											<input type="text" class="icant sumenf10 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf10_514" id="enf10_514">
										</td>
										<td>
											<input type="text" class="icant sumenf10 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf10_1519" id="enf10_1519">
										</td>
										<td>
											<input type="text" class="icant sumenf10 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf10_2039" id="enf10_2039">
										</td>
										<td>
											<input type="text" class="icant sumenf10 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf10_4064" id="enf10_4064">
										</td>
										<td>
											<input type="text" class="icant sumenf10 form-control form-control-sm" 
											maxlength="4" data-parsley-type="digits" value="0" name="enf10_65" id="enf10_65">
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totenf10hid" id="totenf10hid" value="0">
												<strong><span id="totenf10">0</span></strong>
											</div>
										</td>
									</tr>


									<tr>
										<td>Total</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totint1hid" id="totint1hid" value="0">
												<strong><span id="totint1">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totint14hid" id="totint14hid" value="0">
												<strong><span id="totint14">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totint514hid" id="totint514hid" value="0">
												<strong><span id="totint514">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totint1519hid" id="totint1519hid" value="0">
												<strong><span id="totint1519">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totint2039hid" id="totint2039hid" value="0">
												<strong><span id="totint2039">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totint4064hid" id="totint4064hid" value="0">
												<strong><span id="totint4064">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totint65hid" id="totint65hid" value="0">
												<strong><span id="totint65">0</span></strong>
											</div>
										</td>
										<td>
											<div class="alert alert-info my-0 mx-0 py-0 px-0 text-center" style="font-size:20px;" role="alert">
												<input type="hidden" name="totinternadoshid" id="totinternadoshid" value="0">
												<strong><span id="totinternados">0</span></strong>
											</div>
										</td>
									</tr>
								</tbody>
							</table>							
						</div>						
					</div>
				</div>
				
				<div class="form-row">
					<div class="form-group col-md-12">
						<button type="submit" class="btn btn-primary">Cargar Información</button>
					</div>						
				</div>

				<div class="form-row">
					<p>&nbsp;</p>
				</div>

			</form>

        </section>

    </div>

    <footer class="container-fluid bg-primary py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md">
                    <p>
                        <a href="http://salud.sanjuan.gob.ar" target="_self" title="Ministerio de Salud">
                            <img src="img/logosalud.png" alt="Gobierno de San Juan" width="100%">
                        </a>
                    </p>
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <ul class="list-unstyled text-small">
                                    <li class="titulosclaros corregir p-0">
                                    	Dirección: – Av. Libertador Gral. San Martín 750 Oeste - Capital.
                                	</li>
                                    <li class="titulosclaros corregir p-0">
                                    	Centro Cívico, 3° Piso, Núcleo 1.
                                    </li>
                                    <li class="titulosclaros corregir p-0 w3-padding-top">
                                    	<i class="fa fa-phone fa-1x"></i> Conmutador: 
                                    	<a href="tel:2644305000">264 430 5000</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-phone fa-1x"></i> Teléfono: 
                                        <a href="tel:2644306125">264 430 7290</a>.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md">
                    <h5>Teléfonos Útiles</h5>
                    <ul class="list-unstyled text-small">
                        <li>
                            <a href="tel:911">
                                &nbsp;<i class="fa fa-phone fa-1x"></i> <strong>911</strong>&nbsp;
                                Emergencias
                            </a>
                        </li>
                        <li>
                            <a href="tel:101">
                                &nbsp;<i class="fa fa-phone fa-1x"></i> <strong>101</strong>&nbsp;
                                Policía
                            </a>
                        </li>
                        <li>
                            <a href="tel:107">
                                &nbsp;<i class="fa fa-phone fa-1x"></i> <strong>107</strong>&nbsp;
                                Emergencias Médicas
                            </a>
                        </li>
                        <a href="tel:100">
                            &nbsp;<i class="fa fa-phone fa-1x"></i> <strong>100</strong>&nbsp;
                            Bomberos
                        </a>
                    </ul>
                </div>
                <div class="col-12 col-md">
                    <h5>Trámites y servicios</h5>
                    <ul class="list-unstyled text-small">
                        <li><a href="https://webmail.sanjuan.gov.ar/owa">Acceso a Webmail </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- ARCHIVOS BOOTSTRAP JAVASCRIPT -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="moment/moment.js"></script>
    <script src="bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="bootstrap-datepicker/bootstrap-datepicker.es.min.js"></script>    

    <script type="text/javascript">
    	jQuery(document).ready(function($) {

    		function totalesEnfermedad() {
    			var totint1 = 0;
	            totint1 = parseFloat($("#enf1_1").val()) + parseFloat($("#enf2_1").val()) + parseFloat($("#enf3_1").val())
	            		 + parseFloat($("#enf4_1").val()) + parseFloat($("#enf5_1").val()) + parseFloat($("#enf6_1").val())
	            		  + parseFloat($("#enf7_1").val()) + parseFloat($("#enf8_1").val()) + parseFloat($("#enf9_1").val())
	            		   + parseFloat($("#enf10_1").val()); 
	            $("#totint1").html(totint1);
	            $("#totint1hid").val(totint1);

    			var totint14 = 0;
	            totint14 = parseFloat($("#enf1_14").val()) + parseFloat($("#enf2_14").val()) + parseFloat($("#enf3_14").val())
	            		 + parseFloat($("#enf4_14").val()) + parseFloat($("#enf5_14").val()) + parseFloat($("#enf6_14").val())
	            		  + parseFloat($("#enf7_14").val()) + parseFloat($("#enf8_14").val()) + parseFloat($("#enf9_14").val())
	            		   + parseFloat($("#enf10_14").val()); 
	            $("#totint14").html(totint14);
	            $("#totint14hid").val(totint14);

    			var totint514 = 0;
	            totint514 = parseFloat($("#enf1_514").val()) + parseFloat($("#enf2_514").val()) + parseFloat($("#enf3_514").val())
	            		 + parseFloat($("#enf4_514").val()) + parseFloat($("#enf5_514").val()) + parseFloat($("#enf6_514").val())
	            		  + parseFloat($("#enf7_514").val()) + parseFloat($("#enf8_514").val()) + parseFloat($("#enf9_514").val())
	            		   + parseFloat($("#enf10_514").val()); 
	            $("#totint514").html(totint514);
	            $("#totint514hid").val(totint514);

    			var totint1519 = 0;
	            totint1519 = parseFloat($("#enf1_1519").val()) + parseFloat($("#enf2_1519").val()) + parseFloat($("#enf3_1519").val())
	            		 + parseFloat($("#enf4_1519").val()) + parseFloat($("#enf5_1519").val()) + parseFloat($("#enf6_1519").val())
	            		  + parseFloat($("#enf7_1519").val()) + parseFloat($("#enf8_1519").val()) + parseFloat($("#enf9_1519").val())
	            		   + parseFloat($("#enf10_1519").val()); 
	            $("#totint1519").html(totint1519);
	            $("#totint1519hid").val(totint1519);

    			var totint2039 = 0;
	            totint2039 = parseFloat($("#enf1_2039").val()) + parseFloat($("#enf2_2039").val()) + parseFloat($("#enf3_2039").val())
	            		 + parseFloat($("#enf4_2039").val()) + parseFloat($("#enf5_2039").val()) + parseFloat($("#enf6_2039").val())
	            		  + parseFloat($("#enf7_2039").val()) + parseFloat($("#enf8_2039").val()) + parseFloat($("#enf9_2039").val())
	            		   + parseFloat($("#enf10_2039").val()); 
	            $("#totint2039").html(totint2039);
	            $("#totint2039hid").val(totint2039);

    			var totint4064 = 0;
	            totint4064 = parseFloat($("#enf1_4064").val()) + parseFloat($("#enf2_4064").val()) + parseFloat($("#enf3_4064").val())
	            		 + parseFloat($("#enf4_4064").val()) + parseFloat($("#enf5_4064").val()) + parseFloat($("#enf6_4064").val())
	            		  + parseFloat($("#enf7_4064").val()) + parseFloat($("#enf8_4064").val()) + parseFloat($("#enf9_4064").val())
	            		   + parseFloat($("#enf10_4064").val()); 
	            $("#totint4064").html(totint4064);
	            $("#totint4064hid").val(totint4064);

    			var totint65 = 0;
	            totint65 = parseFloat($("#enf1_65").val()) + parseFloat($("#enf2_65").val()) + parseFloat($("#enf3_65").val())
	            		 + parseFloat($("#enf4_65").val()) + parseFloat($("#enf5_65").val()) + parseFloat($("#enf6_65").val())
	            		  + parseFloat($("#enf7_65").val()) + parseFloat($("#enf8_65").val()) + parseFloat($("#enf9_65").val())
	            		   + parseFloat($("#enf10_65").val()); 
	            $("#totint65").html(totint65);
	            $("#totint65hid").val(totint65);

    			var totinternados = 0;
	            totinternados = parseFloat($("#totenf1hid").val()) + parseFloat($("#totenf2hid").val()) + parseFloat($("#totenf3hid").val())
	            				 + parseFloat($("#totenf4hid").val()) + parseFloat($("#totenf5hid").val()) + parseFloat($("#totenf6hid").val())
	            				  + parseFloat($("#totenf7hid").val()) + parseFloat($("#totenf8hid").val()) + parseFloat($("#totenf9hid").val())
	            				   + parseFloat($("#totenf10hid").val()); 
	            $("#totinternados").html(totinternados);
	            $("#totinternadoshid").val(totinternados);
    		}

    		function totalesConsulta() {
    			var totcon1 = 0;
	            totcon1 = parseFloat($("#ped1").val()) + parseFloat($("#cm1").val()) + parseFloat($("#guar1").val()); 
	            $("#totcon1").html(totcon1);
	            $("#totcon1hid").val(totcon1);

    			var totcon14 = 0;
	            totcon14 = parseFloat($("#ped14").val()) + parseFloat($("#cm14").val()) + parseFloat($("#guar14").val()); 
	            $("#totcon14").html(totcon14);
	            $("#totcon14hid").val(totcon14);

    			var totcon514 = 0;
	            totcon514 = parseFloat($("#ped514").val()) + parseFloat($("#cm514").val()) + parseFloat($("#guar514").val()); 
	            $("#totcon514").html(totcon514);
	            $("#totcon514hid").val(totcon514);

    			var totcon1519 = 0;
	            totcon1519 = parseFloat($("#ped1519").val()) + parseFloat($("#cm1519").val()) + parseFloat($("#guar1519").val()); 
	            $("#totcon1519").html(totcon1519);
	            $("#totcon1519hid").val(totcon1519);

    			var totcon2039 = 0;
	            totcon2039 = parseFloat($("#ped2039").val()) + parseFloat($("#cm2039").val()) + parseFloat($("#guar2039").val()); 
	            $("#totcon2039").html(totcon2039);
	            $("#totcon2039hid").val(totcon2039);

    			var totcon4064 = 0;
	            totcon4064 = parseFloat($("#ped4064").val()) + parseFloat($("#cm4064").val()) + parseFloat($("#guar4064").val()); 
	            $("#totcon4064").html(totcon4064);
	            $("#totcon4064hid").val(totcon4064);

    			var totcon65 = 0;
	            totcon65 = parseFloat($("#ped65").val()) + parseFloat($("#cm65").val()) + parseFloat($("#guar65").val()); 
	            $("#totcon65").html(totcon65);
	            $("#totcon65hid").val(totcon65);

    			var totconsultas = 0;
	            totconsultas = parseFloat($("#totpedhid").val()) + parseFloat($("#totcmhid").val()) + parseFloat($("#totguarhid").val()); 
	            $("#totconsultas").html(totconsultas);
	            $("#totconsultashid").val(totconsultas);

    		}

        	$(".sumped").change(function() {
	        	var totped = 0;
	            $('.sumped').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totped = totped + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totped").html(totped);
	            $("#totpedhid").val(totped);
	            totalesConsulta();
	        });

        	$(".sumcm").change(function() {
	        	var totcm = 0;
	            $('.sumcm').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totcm = totcm + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totcm").html(totcm);
	            $("#totcmhid").val(totcm);
	            totalesConsulta();
	        });

        	$(".sumguar").change(function() {
	        	var totguar = 0;
	            $('.sumguar').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totguar = totguar + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totguar").html(totguar);
	            $("#totguarhid").val(totguar);
	            totalesConsulta();
	        });

        	$(".sumenf1").change(function() {
	        	var totenf1 = 0;
	            $('.sumenf1').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf1 = totenf1 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf1").html(totenf1);
	            $("#totenf1hid").val(totenf1);
	            totalesEnfermedad();
	        });

        	$(".sumenf2").change(function() {
	        	var totenf2 = 0;
	            $('.sumenf2').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf2 = totenf2 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf2").html(totenf2);
	            $("#totenf2hid").val(totenf2);
	            totalesEnfermedad();
	        });

        	$(".sumenf3").change(function() {
	        	var totenf3 = 0;
	            $('.sumenf3').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf3 = totenf3 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf3").html(totenf3);
	            $("#totenf3hid").val(totenf3);
	            totalesEnfermedad();
	        });

        	$(".sumenf4").change(function() {
	        	var totenf4 = 0;
	            $('.sumenf4').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf4 = totenf4 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf4").html(totenf4);
	            $("#totenf4hid").val(totenf4);
	            totalesEnfermedad();
	        });

        	$(".sumenf5").change(function() {
	        	var totenf5 = 0;
	            $('.sumenf5').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf5 = totenf5 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf5").html(totenf5);
	            $("#totenf5hid").val(totenf5);
	            totalesEnfermedad();
	        });

        	$(".sumenf6").change(function() {
	        	var totenf6 = 0;
	            $('.sumenf6').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf6 = totenf6 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf6").html(totenf6);
	            $("#totenf6hid").val(totenf6);
	            totalesEnfermedad();
	        });

        	$(".sumenf7").change(function() {
	        	var totenf7 = 0;
	            $('.sumenf7').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf7 = totenf7 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf7").html(totenf7);
	            $("#totenf7hid").val(totenf7);
	            totalesEnfermedad();
	        });

        	$(".sumenf8").change(function() {
	        	var totenf8 = 0;
	            $('.sumenf8').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf8 = totenf8 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf8").html(totenf8);
	            $("#totenf8hid").val(totenf8);
	            totalesEnfermedad();
	        });

        	$(".sumenf9").change(function() {
	        	var totenf9 = 0;
	            $('.sumenf9').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf9 = totenf9 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf9").html(totenf9);
	            $("#totenf9hid").val(totenf9);
	            totalesEnfermedad();
	        });

        	$(".sumenf10").change(function() {
	        	var totenf10 = 0;
	            $('.sumenf10').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf10 = totenf10 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf10").html(totenf10);
	            $("#totenf10hid").val(totenf10);
	            totalesEnfermedad();
	        });

        
        	$(".icant").on('focus click touchstart', function(e){
        		$(this).get(0).setSelectionRange(0,9999);
        		e.preventDefault();
        	});
        
        	$(".icant").off('keyup').on('keyup', function(e) {
        	    if (e.which === 13) {
        	    	/*
        	    	$(this).next('input').focus();
        	    	$(this).next().trigger('focus');
        	    	*/
        	    	e.preventDefault();
    				$(this).next().find('input[type=text]').focus();
        	    }
			});

    		$(".icant").keypress(function (e) {
    			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    				// $("#errmsg").html("Solo numeros").stop().show().fadeOut("slow");
    				return false;
    			}
    		});

    		$("#nombreestablecimiento").select2({
    			placeholder: "Buscar Centro de Salud",
    			minimumInputLength: 3,
    			theme: "bootstrap4",
    			ajax: {
			        url: "ajax_establecimientos.php",
			        dataType: 'json',
			        delay: 250,
			        type: "post",
			        quietMillis: 50,
			        data: function (term) {
			            return {
			                term: term
			            };
			        },
			        processResults: function (data) {
			        	return {
	                    	results: $.map(data, function (item) {
	                        	return {
	                            	text: item.nombre,
	                                id: item.cod_establecimiento
	                            }
	                        })
	                    };
	                },
			        cache: true
			    }
    		});

    		$( ".selenfermedad" ).keyup(function() {
    			console.log('s');
		        	$(".selenfermedad").select2({
	    			placeholder: "Enfermedad (Nombre/COD)",
	    			minimumInputLength: 2,
	    			theme: "bootstrap4",
	    			width: "200px",
	    			ajax: {
				        url: "ajax_enfermedad.php",
				        dataType: 'json',
				        delay: 250,
				        type: "post",
				        quietMillis: 50,
				        data: function (term) {
				            return {
				                term: term
				            };
				        },
				        processResults: function (data) {
				        	return {
		                    	results: $.map(data, function (item) {
		                        	return {
		                            	text: item.nombre,
		                                id: item.codigo
		                            }
		                        })
		                    };
		                },
				        cache: true
				    }
	    		});
	        });

    		$(".selenfermedad").select2({
    			placeholder: "Enfermedad (Nombre/COD)",
    			minimumInputLength: 2,
    			theme: "bootstrap4",
    			width: "200px",
    			ajax: {
			        url: "ajax_enfermedad.php",
			        dataType: 'json',
			        delay: 250,
			        type: "post",
			        quietMillis: 50,
			        data: function (term) {
			            return {
			                term: term
			            };
			        },
			        processResults: function (data) {
			        	return {
	                    	results: $.map(data, function (item) {
	                        	return {
	                            	text: item.nombre,
	                                id: item.codigo
	                            }
	                        })
	                    };
	                },
			        cache: true
			    }
    		});

    		/*$("#campofecha").datepicker({
    		    useCurrent:false,
    		    autoclose: true,
    		    todayHighlight: true,
    		    format: "dd/mm/yyyy",
    		    language: "es",
    		    orientation: "auto",
    		}).datepicker("setDate",'now');*/
    		$("#campofecha").datepicker({
    			useCurrent:false,
    		    autoclose: true,
    		    todayHighlight: true,
    		    format: "dd/mm/yyyy",
    		    language: "es",
    		    orientation: "auto",
    		    //minDate: "3M",
    		    endDate: new Date(),
    		    startDate: new Date('2020-03-17'),
			    onSelect: function(dateText) {
			      display("Selected date: " + dateText + ", Current Selected Value= " + this.value);
			      $(this).change();
			    }
			  }).on("change", function() {
			    cambio_fecha("Change event");
			}).datepicker("setDate",'now');
			$('#nombreestablecimiento').change(function() 
  			{
  				if( $('select[id=nombreestablecimiento]').val() != '') 
			    {
			    	//console.log('no esta vacio');
			    	//console.log($('select[id=nombreestablecimiento]').val());
			    	traer_parte_diario();
			    }
  			});
  			function totalesDeTotales()
  			{
  				var totped = 0;
	            $('.sumped').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totped = totped + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totped").html(totped);
	            $("#totpedhid").val(totped);

	            var totcm = 0;
	            $('.sumcm').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totcm = totcm + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totcm").html(totcm);
	            $("#totcmhid").val(totcm);

	            var totguar = 0;
	            $("#totguarhid").val(totguar);
	            $('.sumguar').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{          
	            		//console.log(this);    
	            		totguar = totguar + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totguar").html(totguar);
	            $("#totguarhid").val(totguar);
				//totguarhid
	            var totenf1 = 0;
	            $('.sumenf1').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf1 = totenf1 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf1").html(totenf1);
	            $("#totenf1hid").val(totenf1);

	            var totenf2 = 0;
	            $('.sumenf2').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf2 = totenf2 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf2").html(totenf2);
	            $("#totenf2hid").val(totenf2);

	            var totenf3 = 0;
	            $('.sumenf3').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf3 = totenf3 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf3").html(totenf3);
	            $("#totenf3hid").val(totenf3);

	            var totenf4 = 0;
	            $('.sumenf4').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf4 = totenf4 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf4").html(totenf4);
	            $("#totenf4hid").val(totenf4);

	            var totenf5 = 0;
	            $('.sumenf5').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf5 = totenf5 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf5").html(totenf5);
	            $("#totenf5hid").val(totenf5);

	            var totenf6 = 0;
	            $('.sumenf6').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf6 = totenf6 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf6").html(totenf6);
	            $("#totenf6hid").val(totenf6);

	            var totenf7 = 0;
	            $('.sumenf7').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf7 = totenf7 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf7").html(totenf7);
	            $("#totenf7hid").val(totenf7);

	            var totenf8 = 0;
	            $('.sumenf8').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf8 = totenf8 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf8").html(totenf8);
	            $("#totenf8hid").val(totenf8);


	            var totenf9 = 0;
	            $('.sumenf9').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf9 = totenf9 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf9").html(totenf9);
	            $("#totenf9hid").val(totenf9)

	            var totenf10 = 0;
	            $('.sumenf10').each(function() {
	            	if((!(isNaN($(this).val()))) && $(this).val())
	            	{              
	            		totenf10 = totenf10 + parseFloat($(this).val()); 
	            	}
	            });
	            $("#totenf10").html(totenf10);
	            $("#totenf10hid").val(totenf10);

	            totalesConsulta();
	            totalesEnfermedad();

  			}
			function traer_parte_diario()
			{
				$.notify("Buscando Datos Cargados", "info");
				$('#camasdisponibles').val('');
				$('#respiradoresdisponibles').val('');
				$('#camasGenDispAdult').val('');
				$('#camasGenOcupAdult').val('');
				$('#camasEspDispAdult').val('');
				$('#camasEspOcupAdult').val('');
				$('#camasCritDispAdult').val('');
				$('#camasCritOcupAdult').val('');
				$('#camasGenDispPed').val('');
				$('#camasGenOcupPed').val('');
				$('#camasEspDispPed').val('');
				$('#camasEspOcupPed').val('');
				$('#camasCritDispPed').val('');
				$('#camasCritOcupPed').val('');
				$('#ped1').val('0');
				$('#ped14').val('0');
				$('#ped514').val('0');
				$('#ped1519').val('0');
				$('#ped2039').val('0');
				$('#ped4064').val('0');
				$('#cm1').val('0');
				$('#cm14').val('0');
				$('#cm514').val('0');
				$('#cm1519').val('0');
				$('#cm2039').val('0');
				$('#cm4064').val('0');
				$('#cm65').val('0');
				$('#guar1').val('0');
				$('#guar14').val('0');
				$('#guar514').val('0');
				$('#guar1519').val('0');
				$('#guar2039').val('0');
				$('#guar4064').val('0');
				$('#guar65').val('0');

				for(var k=1; k < 11 ; k++)
				{
				    $('#enf'+k+'_1').val('0');
					$('#enf'+k+'_14').val('0');
					$('#enf'+k+'_514').val('0');
					$('#enf'+k+'_1519').val('0');
					$('#enf'+k+'_2039').val('0');
					$('#enf'+k+'_4064').val('0');
					$('#enf'+k+'_65').val('0');
				}
				totalesEnfermedad();
				totalesConsulta();
				totalesDeTotales();

			    $.ajax({
			          	url: "ajax_consulta_parte.php",
				        dataType: 'json',
				        delay: 250,
				        type: "GET",
			          
			          	data:{cod_establecimiento:$('select[id=nombreestablecimiento]').val(), fecha: $('#campofecha').val()},
			          	beforeSend: function() 
			          	{
							
						},
				        success: function(data)
				        { 
				          //	console.log(data.result);
				          	if(data.status == 1)
				          	{
				          		$.notify("Se Encontraron Registros", "success");
				          		if(data.result)
					         	{
					         		//console.log('ok');
					         		$('#camasdisponibles').val(data.result.camas);
					         		$('#respiradoresdisponibles').val(data.result.respiradores);
					         		$('#camasGenDispAdult').val(data.result.camasGenDispAdult);
					         		$('#camasGenOcupAdult').val(data.result.camasGenOcupAdult);
					         		$('#camasEspDispAdult').val(data.result.camasEspDispAdult);
					         		$('#camasEspOcupAdult').val(data.result.camasEspOcupAdult);
					         		$('#camasCritDispAdult').val(data.result.camasCritDispAdult);
					         		$('#camasCritOcupAdult').val(data.result.camasCritOcupAdult);
					         		$('#camasGenDispPed').val(data.result.camasGenDispPed);
					         		$('#camasGenOcupPed').val(data.result.camasGenOcupPed);
					         		$('#camasEspDispPed').val(data.result.camasEspDispPed);
					         		$('#camasEspOcupPed').val(data.result.camasEspOcupPed);
					         		$('#camasCritDispPed').val(data.result.camasCritDispPed);
					         		$('#camasCritOcupPed').val(data.result.camasCritOcupPed);
					         		
					         			
					         		totalesConsulta();

					         	}
					         	
					         	if(data.parte_detalle)
					            {
					            	for(var i=0; i < data.parte_detalle.length ; i++)
					            	{
					            		//Cantidad de consultas del día informado pediatria 280 Clinica medica 281 y guardia 282
					            		if(data.parte_detalle[i]['id_enfermedad'] ==  280)
					            		{
					            			//console.log(data.parte_detalle[i]['id_grupoetario']);
					            			if(data.parte_detalle[i]['id_grupoetario'] == 1)
					            			{
					            				$('#ped1').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 2)
					            			{
					            				$('#ped14').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 3)
					            			{
					            				$('#ped514').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 4)
					            			{
					            				$('#ped1519').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 5)
					            			{
					            				$('#ped2039').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] ==6)
					            			{
					            				$('#ped4064').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 7)
					            			{
					            				$('#ped65').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			
					            		}
					            		else if(data.parte_detalle[i]['id_enfermedad'] ==  281)
					            		{
					            			//console.log(data.parte_detalle[i]['id_grupoetario']);
					            			if(data.parte_detalle[i]['id_grupoetario'] == 1)
					            			{
					            				$('#cm1').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 2)
					            			{
					            				$('#cm14').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 3)
					            			{
					            				$('#cm514').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 4)
					            			{
					            				$('#cm1519').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 5)
					            			{
					            				$('#cm2039').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] ==6)
					            			{
					            				$('#cm4064').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 7)
					            			{
					            				$('#cm65').val(data.parte_detalle[i]['cantidad']);
					            			}
					            		}
					            		else if(data.parte_detalle[i]['id_enfermedad'] ==  282)
					            		{
					            			//console.log(data.parte_detalle[i]['id_grupoetario']);
					            			if(data.parte_detalle[i]['id_grupoetario'] == 1)
					            			{
					            				$('#guar1').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 2)
					            			{
					            				$('#guar14').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 3)
					            			{
					            				$('#guar514').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 4)
					            			{
					            				$('#guar1519').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 5)
					            			{
					            				$('#guar2039').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] ==6)
					            			{
					            				$('#guar4064').val(data.parte_detalle[i]['cantidad']);
					            			}
					            			if(data.parte_detalle[i]['id_grupoetario'] == 7)
					            			{
					            				$('#guar65').val(data.parte_detalle[i]['cantidad']);
					            			}
					            		}
					            		else if(data.parte_detalle[i]['tipo'] ==  'I')
					            		{

					            		}
					            		
					            	}
					            	for(var j=0; j < data.enfermedades.length; j++)
					            	{
					            		var enfermedad = data.enfermedades[j]['fk_enfermedad'].trim();
					            		//enfermedad = enfermedad.trim();
					            		//console.log(enfermedad);
					            		
					            		if((enfermedad != '280') && (enfermedad != '281') &&(enfermedad != '282'))
					            		{	//selenf2
					            			var k = j+1;
					            			for(var i=0; i < data.parte_detalle.length ; i++)
					            			{
					            				//console.log((data.parte_detalle[i]['id_enfermedad']).trim());
					            				var enfermedad_parte = data.parte_detalle[i]['id_enfermedad'].trim();
					            				if(enfermedad_parte == enfermedad)
					            				{
					            					//console.log(data.parte_detalle[i]);
					            					//console.log(i);
					            					/*var data = {
													    id: enfermedad,
													    text: enfermedad
													};
					            					var newOption = new Option(data.text, data.id, true, true);
												    // Append it to the select
												    $('#selenf1').append(newOption).trigger('change');*/
												    //$('#selenf'+k).removeClass("select2");
												  // $("#selenf"+k).select2('data', {id: 'newID', text: newText});  
												    $("#selenf"+k).html('').select2({data: [
													 {id: enfermedad, text: enfermedad+' - '+data.enfermedades[j]['nomb_enf']},
													]});
													
													for(var e=0; e < data.enfermedades_all.length; e++)
													{
														if(enfermedad != data.enfermedades_all[e]['codigo'].trim() )
														{
															$("#selenf"+k).append('').select2({data: [
															 {id: data.enfermedades_all[e]['codigo'], text: data.enfermedades_all[e]['nombre']},
															]});
														}
															
													}
					            					if(data.parte_detalle[i]['id_grupoetario'] == 1)
							            			{
							            				$('#enf'+k+'_1').val(data.parte_detalle[i]['cantidad']);
							            			}
							            			if(data.parte_detalle[i]['id_grupoetario'] == 2)
							            			{
							            				$('#enf'+k+'_14').val(data.parte_detalle[i]['cantidad']);
							            			}
							            			if(data.parte_detalle[i]['id_grupoetario'] == 3)
							            			{
							            				$('#enf'+k+'_514').val(data.parte_detalle[i]['cantidad']);
							            			}
							            			if(data.parte_detalle[i]['id_grupoetario'] == 4)
							            			{
							            				$('#enf'+k+'_1519').val(data.parte_detalle[i]['cantidad']);
							            			}
							            			if(data.parte_detalle[i]['id_grupoetario'] == 5)
							            			{
							            				$('#enf'+k+'_2039').val(data.parte_detalle[i]['cantidad']);
							            			}
							            			if(data.parte_detalle[i]['id_grupoetario'] ==6)
							            			{
							            				$('#enf'+k+'_4064').val(data.parte_detalle[i]['cantidad']);
							            			}
							            			if(data.parte_detalle[i]['id_grupoetario'] == 7)
							            			{
							            				$('#enf'+k+'_65').val(data.parte_detalle[i]['cantidad']);
							            			}
					            				}
					            			}
					            		}
					            		
					            	}
					            	

					            }
					            totalesConsulta();
					            totalesEnfermedad();
					            totalesDeTotales();
					            
					            /*totalesEnfermedad();
					            totalesConsulta();*/
				          	}
				          	else
				          	{
				          		$.notify("No Existen Datos Cargados del Centro de Salud y fecha Seleccionada", "danger");
				          	}
					          	
				        },
				        error: function(response)
				        {
				            console.log(response);
				            $.notify("No Existen Datos Cargados del Centro de Salud y fecha Seleccionada", "danger");
				                     
				        }
			        });
			    
			}
			function cambio_fecha(msg) {
			 //   console.log('a');
			    if( $('select[id=nombreestablecimiento]').val() != '') 
			    {
			    	//console.log('no esta vacio');
			    	//console.log($('select[id=nombreestablecimiento]').val());
			    	traer_parte_diario();
			    }
			    else
			    {
			    	//console.log('vacio el select');
			    }
			}
    		Parsley.addMessages('es', {
    			defaultMessage: "Este valor parece ser inválido.",
    			type: {
    				email: "Este valor debe ser un correo válido.",
    				url: "Este valor debe ser una URL válida.",
    				number: "Este valor debe ser un número válido.",
    				integer: "Este valor debe ser un número válido.",
    				digits: "Este valor debe ser un dígito válido.",
    				alphanum: "Este valor debe ser alfanumérico."
    			},
    			notblank: "Este campo no debe estar en blanco.",
    			required: "Este campo es obligatorio.",
    			pattern: "Este valor es incorrecto.",
    			min: "Este valor no debe ser menor que %s.",
    			max: "Este valor no debe ser mayor que %s.",
    			range: "Este valor debe estar entre %s y %s.",
    			minlength: "Este valor es muy corto. La longitud mínima es de %s caracteres.",
    			maxlength: "Este valor es muy largo. La longitud máxima es de %s caracteres.",
    			length: "La longitud de este valor ingresado debe estar entre %s y %s caracteres.",
    			mincheck: "Debe seleccionar al menos %s opciones.",
    			maxcheck: "Debe seleccionar %s opciones o menos.",
    			check: "Debe seleccionar entre %s y %s opciones.",
    			equalto: "Este valor debe ser idéntico."
    		});
    		Parsley.setLocale('es');		

    		$("#formparte").parsley({
    		    errorClass: 'is-invalid',
    		    successClass: 'is-valid',
    		    classHandler: function(ParsleyField) {
    		        return ParsleyField.$element.parents('.controls');
    		    },
    		    errorsContainer: function(ParsleyField) {
    		        return ParsleyField.$element.parents('.controls');
    		    },
    		    errorsWrapper: '<span class="text-danger text-help">',
    		    errorTemplate: '<div></div>',
    		    trigger: 'change'
    		});            
    		
    		
    		/*$('.campofecha').datepicker({
			   onSelect: function(dateText, inst) { console.log('a'); }
			});*/
    		
    	});
var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwZXJzb25hIjoiMzQ5MTYzMzAuRE5JLk0iLCJkYXRhIjoie1wibm9tYnJlX2FwZWxsaWRvXCI6XCJWaWN0b3IgRGFtaWFuIExFQUxcIn0iLCJpc3MiOiJhdXRoMCIsImV4cCI6MTU4NTkzMDYxMH0.yz5bUsfa0HN48YVyTEm2nMovB6tLgKVHDucIMPbzEEo';


var users_cd = jwt_decode(token);

if(users_cd)
{
	if(users_cd.persona)
	{
		//console.log(users_cd.persona);
		var arre = Array();
        arre=users_cd.persona.split('.');
        var dni = arre[0];
        console.log(dni);
        $('#users_cd').val(dni);
	}
}
</script>
<div class="wrapper wrapper-content animated fadeInRight" id="">
	<!--<iframe id="inlineFrameExample"
    title="Inline Frame Example"
    width="100%"
    height="500"
    src="https://reportecamas.msal.gov.ar/">
</iframe>-->
<script>
	//var centros_de_salud = '{ "centros": [{"name": "CLINICA EL CASTA\u00d1O", "id": 173, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 0, "address": "AVENIDA LIBERTADOR GENERAL SAN MARTIN -ESTE- 952", "longitude": -68.5127469999999, "respirators_allocated_children": 0, "uti_allocated_adult": 16, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282377006, "latitude": -31.533475, "state": "San Juan", "salesforce_id": "0013h000008kjv1AAA", "dialysis_available": null},						{"name": "INSTITUTO MEDICO SAN JUAN", "id": 174, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 0, "address": "CATAMARCA -SUR- 471", "longitude": -68.5298649999999, "respirators_allocated_children": 0, "uti_allocated_adult": 8, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282377026, "latitude": -31.540189, "state": "San Juan", "salesforce_id": "0013h000008kjqWAAQ", "dialysis_available": null}, 						{"name": "SANATORIO SANTA CLARA RED DE CLINICAS", "id": 176, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 0, "address": "MENDOZA -SUR- 612", "longitude": -68.5253549999999, "respirators_allocated_children": 0, "uti_allocated_adult": 22, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282377027, "latitude": -31.541658, "state": "San Juan", "salesforce_id": "0013h000008kjvBAAQ", "dialysis_available": null}, 						{"name": "INSTITUTO DE CARDIOLOGIA", "id": 182, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 0, "address": "Peatonal Bernardino Rivadavia Este 444", "longitude": -68.5196176, "respirators_allocated_children": 0, "uti_allocated_adult": 0, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 51700282377326, "latitude": -31.5362865, "state": "San Juan", "salesforce_id": "0013h000008kjvfAAA", "dialysis_available": null}, 						{"name": "SANATORIO SAN JUAN", "id": 172, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-23 09:02:20.783919", "uti_allocated_children": 0, "address": "LAVALLE SUR 735", "longitude": null, "respirators_allocated_children": 0, "uti_allocated_adult": 8, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282378369, "latitude": null, "state": "San Juan", "salesforce_id": "0013h000008kjrUAAQ", "dialysis_available": false},						{"name": "HOSPITAL SAN ROQUE", "id": 183, "created_on": null, "city": "J\u00c1CHAL", "updated_on": "2020-04-22 04:35:13.692743", "uti_allocated_children": 0, "address": "25 DE MAYO 835 ENTRE GRAL PAZ Y ABERASTAIN", "longitude": -68.7485259999999, "respirators_allocated_children": 0, "uti_allocated_adult": 0, "funding_source": "PUBLICO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700562177024, "latitude": -30.236303, "state": "San Juan", "salesforce_id": "0013h000008kjvpAAA", "dialysis_available": false},						{"name": "CENTRO INTEGRAL DE LA MUJER Y EL NI\u00d1O CIMYN", "id": 178, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 4, "address": "CATAMARCA -SUR- 417", "longitude": -68.5299069999999, "respirators_allocated_children": 0, "uti_allocated_adult": 0, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 12700282377031, "latitude": -31.539429, "state": "San Juan", "salesforce_id": "0013h000008kjvLAAQ", "dialysis_available": null}, 						{"name": "SANATORIO ARGENTINO S.R.L.", "id": 179, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 6, "address": "SAN LUIS OESTE 432", "longitude": -68.5325409999999, "respirators_allocated_children": 0, "uti_allocated_adult": 0, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 12700282377032, "latitude": -31.533636, "state": "San Juan", "salesforce_id": "0013h000008kjvQAAQ", "dialysis_available": null},						{"name": "SANATORIO MAYO S.A.", "id": 177, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 0, "address": "MITRE -OESTE- 192", "longitude": -68.528447, "respirators_allocated_children": 0, "uti_allocated_adult": 13, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282377028, "latitude": -31.538044, "state": "San Juan", "salesforce_id": "0013h000008kjrPAAQ", "dialysis_available": null}, 						{"name": "HOSPITAL PRIVADO DEL COLEGIO MEDICO DE SAN JUAN", "id": 181, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 0, "address": "RIVADAVIA ESTE 542", "longitude": -68.5181519999999, "respirators_allocated_children": 0, "uti_allocated_adult": 16, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282377023, "latitude": -31.536098, "state": "San Juan", "salesforce_id": "0013h000008kjixAAA", "dialysis_available": null}, 						{"name": "CLINICA SANTA MARIA", "id": 175, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 6, "address": "TUCUMAN -NORTE- 784", "longitude": -68.524115, "respirators_allocated_children": 0, "uti_allocated_adult": 0, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282377008, "latitude": -31.524747, "state": "San Juan", "salesforce_id": "0013h000008kjuxAAA", "dialysis_available": null}, 						{"name": "CENTRO ESPA\u00d1OL PARA EL MANEJO DE ENFERMEDADES CRONICAS CEMEC", "id": 171, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-23 09:02:20.783919", "uti_allocated_children": 0, "address": "AGUSTIN GOMEZ OESTE 468", "longitude": -68.5299719999999, "respirators_allocated_children": 0, "uti_allocated_adult": 12, "funding_source": "PUBLICO", "respirators_allocated_adult": 0, "principal": 19, "cod": 14700282178244, "latitude": -31.553293, "state": "San Juan", "salesforce_id": "0013h000008kjlHAAQ", "dialysis_available": false}, 						{"name": "HOSPITAL PUBLICO DE GESTION DESCENTRALIZADA DR. GUILLERMO RAWSON", "id": 180, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-20 11:15:15.234323", "uti_allocated_children": 12, "address": "AVENIDA RAWSON SUR 494", "longitude": -68.514611, "respirators_allocated_children": 0, "uti_allocated_adult": 34, "funding_source": "PUBLICO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282177016, "latitude": -31.539636, "state": "San Juan", "salesforce_id": "0013h000008kjvaAAA", "dialysis_available": true}, 						{"name": "HOSPITAL DR. MARCIAL V. QUIROGA", "id": 184, "created_on": null, "city": "RIVADAVIA", "updated_on": "2020-04-22 04:35:13.692743", "uti_allocated_children": 0, "address": "Av. Libertador Gral. San Mart\u00edn Oeste 5499", "longitude": -68.5148801, "respirators_allocated_children": 0, "uti_allocated_adult": 24, "funding_source": "PUBLICO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700842177017, "latitude": -31.5334284, "state": "San Juan", "salesforce_id": "0013h000008kjusAAA", "dialysis_available": false}		]}}';
	//centros_de_salud = jQuery.parseJSON( centros_de_salud );
	//console.log(centros_de_salud);
	var text = '{ "centros" : [{"name": "CLINICA EL CASTA\u00d1O", "id": 173, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 0, "address": "AVENIDA LIBERTADOR GENERAL SAN MARTIN -ESTE- 952", "longitude": -68.5127469999999, "respirators_allocated_children": 0, "uti_allocated_adult": 16, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282377006, "latitude": -31.533475, "state": "San Juan", "salesforce_id": "0013h000008kjv1AAA", "dialysis_available": null},{"name": "INSTITUTO MEDICO SAN JUAN", "id": 174, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 0, "address": "CATAMARCA -SUR- 471", "longitude": -68.5298649999999, "respirators_allocated_children": 0, "uti_allocated_adult": 8, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282377026, "latitude": -31.540189, "state": "San Juan", "salesforce_id": "0013h000008kjqWAAQ", "dialysis_available": null}, 						{"name": "SANATORIO SANTA CLARA RED DE CLINICAS", "id": 176, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 0, "address": "MENDOZA -SUR- 612", "longitude": -68.5253549999999, "respirators_allocated_children": 0, "uti_allocated_adult": 22, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282377027, "latitude": -31.541658, "state": "San Juan", "salesforce_id": "0013h000008kjvBAAQ", "dialysis_available": null}, 						{"name": "INSTITUTO DE CARDIOLOGIA", "id": 182, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 0, "address": "Peatonal Bernardino Rivadavia Este 444", "longitude": -68.5196176, "respirators_allocated_children": 0, "uti_allocated_adult": 0, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 51700282377326, "latitude": -31.5362865, "state": "San Juan", "salesforce_id": "0013h000008kjvfAAA", "dialysis_available": null}, 						{"name": "SANATORIO SAN JUAN", "id": 172, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-23 09:02:20.783919", "uti_allocated_children": 0, "address": "LAVALLE SUR 735", "longitude": null, "respirators_allocated_children": 0, "uti_allocated_adult": 8, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282378369, "latitude": null, "state": "San Juan", "salesforce_id": "0013h000008kjrUAAQ", "dialysis_available": false},						{"name": "HOSPITAL SAN ROQUE", "id": 183, "created_on": null, "city": "J\u00c1CHAL", "updated_on": "2020-04-22 04:35:13.692743", "uti_allocated_children": 0, "address": "25 DE MAYO 835 ENTRE GRAL PAZ Y ABERASTAIN", "longitude": -68.7485259999999, "respirators_allocated_children": 0, "uti_allocated_adult": 0, "funding_source": "PUBLICO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700562177024, "latitude": -30.236303, "state": "San Juan", "salesforce_id": "0013h000008kjvpAAA", "dialysis_available": false},						{"name": "CENTRO INTEGRAL DE LA MUJER Y EL NI\u00d1O CIMYN", "id": 178, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 4, "address": "CATAMARCA -SUR- 417", "longitude": -68.5299069999999, "respirators_allocated_children": 0, "uti_allocated_adult": 0, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 12700282377031, "latitude": -31.539429, "state": "San Juan", "salesforce_id": "0013h000008kjvLAAQ", "dialysis_available": null}, 						{"name": "SANATORIO ARGENTINO S.R.L.", "id": 179, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 6, "address": "SAN LUIS OESTE 432", "longitude": -68.5325409999999, "respirators_allocated_children": 0, "uti_allocated_adult": 0, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 12700282377032, "latitude": -31.533636, "state": "San Juan", "salesforce_id": "0013h000008kjvQAAQ", "dialysis_available": null},						{"name": "SANATORIO MAYO S.A.", "id": 177, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 0, "address": "MITRE -OESTE- 192", "longitude": -68.528447, "respirators_allocated_children": 0, "uti_allocated_adult": 13, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282377028, "latitude": -31.538044, "state": "San Juan", "salesforce_id": "0013h000008kjrPAAQ", "dialysis_available": null}, 						{"name": "HOSPITAL PRIVADO DEL COLEGIO MEDICO DE SAN JUAN", "id": 181, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 0, "address": "RIVADAVIA ESTE 542", "longitude": -68.5181519999999, "respirators_allocated_children": 0, "uti_allocated_adult": 16, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282377023, "latitude": -31.536098, "state": "San Juan", "salesforce_id": "0013h000008kjixAAA", "dialysis_available": null}, 						{"name": "CLINICA SANTA MARIA", "id": 175, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-16 17:37:14.572230", "uti_allocated_children": 6, "address": "TUCUMAN -NORTE- 784", "longitude": -68.524115, "respirators_allocated_children": 0, "uti_allocated_adult": 0, "funding_source": "PRIVADO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282377008, "latitude": -31.524747, "state": "San Juan", "salesforce_id": "0013h000008kjuxAAA", "dialysis_available": null}, 						{"name": "CENTRO ESPA\u00d1OL PARA EL MANEJO DE ENFERMEDADES CRONICAS CEMEC", "id": 171, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-23 09:02:20.783919", "uti_allocated_children": 0, "address": "AGUSTIN GOMEZ OESTE 468", "longitude": -68.5299719999999, "respirators_allocated_children": 0, "uti_allocated_adult": 12, "funding_source": "PUBLICO", "respirators_allocated_adult": 0, "principal": 19, "cod": 14700282178244, "latitude": -31.553293, "state": "San Juan", "salesforce_id": "0013h000008kjlHAAQ", "dialysis_available": false}, 						{"name": "HOSPITAL PUBLICO DE GESTION DESCENTRALIZADA DR. GUILLERMO RAWSON", "id": 180, "created_on": null, "city": "CAPITAL", "updated_on": "2020-04-20 11:15:15.234323", "uti_allocated_children": 12, "address": "AVENIDA RAWSON SUR 494", "longitude": -68.514611, "respirators_allocated_children": 0, "uti_allocated_adult": 34, "funding_source": "PUBLICO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700282177016, "latitude": -31.539636, "state": "San Juan", "salesforce_id": "0013h000008kjvaAAA", "dialysis_available": true}, 						{"name": "HOSPITAL DR. MARCIAL V. QUIROGA", "id": 184, "created_on": null, "city": "RIVADAVIA", "updated_on": "2020-04-22 04:35:13.692743", "uti_allocated_children": 0, "address": "Av. Libertador Gral. San Mart\u00edn Oeste 5499", "longitude": -68.5148801, "respirators_allocated_children": 0, "uti_allocated_adult": 24, "funding_source": "PUBLICO", "respirators_allocated_adult": 0, "principal": 19, "cod": 10700842177017, "latitude": -31.5334284, "state": "San Juan", "salesforce_id": "0013h000008kjusAAA", "dialysis_available": false} ]}';
	var obj = JSON.parse(text);
//console.log(obj);
//console.log(obj.length);
//console.log(obj['centros']);
var centros = obj['centros'];
for(var i=0; i < centros.length ; i++)
{
	var a = centros[i];
	console.log(centros[i]);
	$.ajax({
			    url: "cargar_centros_de_nacion.php",
				dataType: 'json',
				        delay: 250,
				type: "POST",
			          
			    data:{centro: a},
			    beforeSend: function() 
			    {
							
				},
				success: function(data)
				{
					console.log(data);
				},
				error: function(response)
				{
				           
				                     
				}
			});

}
</script>
</body>
</html>
