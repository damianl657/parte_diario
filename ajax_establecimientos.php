<?php
	// Conecta a PostgreSQL
	require 'classPgSql.php';
	$pg = new PgSql();

	$busca = $_POST["term"]["term"];
	
	$sql = "
		SELECT id, cod_establecimiento, nombre 
			FROM partediario.establecimientos
			WHERE ((nombre ILIKE '%$busca%') OR (cod_establecimiento ILIKE '%$busca%')) AND (mostrar = '1')
			LIMIT 50
	";
	$json = [];
	foreach($pg->getRows($sql) as $row) {
		$json[] = ['cod_establecimiento'=>$row->cod_establecimiento, 'nombre'=>$row->nombre];
	}
	echo json_encode($json);
/*
$sql = "SELECT items.id, items.title FROM items 
		WHERE title LIKE '%".$_GET['q']."%'
		LIMIT 10"; 
$result = $mysqli->query($sql);
$json = [];
while($row = $result->fetch_assoc()){
     $json[] = ['id'=>$row['id'], 'text'=>$row['title']];
}
echo json_encode($json);
*/
?>