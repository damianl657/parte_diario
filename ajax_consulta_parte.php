<?php
	// Conecta a PostgreSQL
	require 'classPgSql.php';
	$pg = new PgSql();

	//$busca = $_POST["term"]["term"];
	$cod_establecimiento = $_GET["cod_establecimiento"];
	$fecha = $_GET["fecha"];
	$sql = "
		SELECT *
			FROM partediario.parte
			WHERE cod_establecimiento = '$cod_establecimiento' and fecha = '$fecha'
			LIMIT 1
	";
	$json =$pg->getRow($sql);
	$id_parte = '';
	$status = 0;
	if(isset($json->idparte))
	{
		$id_parte = $json->idparte;
		$status = 1;
	}
	$parte_detalle = '';
	if($id_parte > 0)
	{
		$sql_parte_detalle =" SELECT parte_detalle.fk_parte as id_parte, parte_detalle.fk_grupoetario as id_grupoetario, parte_detalle.cantidad, parte_detalle.tipo, parte_detalle.fk_enfermedad as id_enfermedad FROM partediario.parte_detalle
			where partediario.parte_detalle.fk_parte = '$id_parte'";
		$parte_detalle =$pg->getRows($sql_parte_detalle);

		$sql_enfermedades ="SELECT parte_detalle.fk_enfermedad,  count(*) as cantidad,  enfermedades.nombre as nomb_enf
							FROM partediario.parte_detalle
							JOIN partediario.enfermedades on partediario.enfermedades.codigo = parte_detalle.fk_enfermedad
							where parte_detalle.fk_parte = '$id_parte' and parte_detalle.fk_enfermedad <> '280' and parte_detalle.fk_enfermedad <> '281' and fk_enfermedad <> '282' 
							GROUP BY   parte_detalle.fk_enfermedad, enfermedades.nombre";
		$enfermedades =$pg->getRows($sql_enfermedades);
		
		$sql_enfermedades_all = "SELECT id, codigo, nombre  FROM partediario.enfermedades WHERE  mostrar = '1' LIMIT 100";
		$json_enfermedades_all = [];
		foreach($pg->getRows($sql_enfermedades_all) as $row) 
		{
			
			$json_enfermedades_all[] = ['codigo'=>$row->codigo, 'nombre'=>$row->codigo.' - '.$row->nombre];
		}

		$data = array('status' =>  $status, 'result' =>  $json, 'id_parte' => $id_parte, 'parte_detalle' => $parte_detalle, 'enfermedades' => $enfermedades, 'enfermedades_all' => $json_enfermedades_all );
	}
	else
	{
		$data = array('status' =>  $status, 'result' =>  $json, 'id_parte' => $id_parte);
	}
	
	
 
	echo json_encode($data);

?>