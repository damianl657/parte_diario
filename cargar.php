<?php
	// Conecta a PostgreSQL
	require 'classPgSql.php';
	$pg = new PgSql();

	$fecha = $_POST["campofecha"];
	$nombreestablecimiento = $_POST["nombreestablecimiento"];
	$camasdisponibles = 0;
	$respiradoresdisponibles = $_POST["respiradoresdisponibles"];
	$camasGenDispAdult= $_POST["camasGenDispAdult"];
	$camasGenOcupAdult= $_POST["camasGenOcupAdult"];
	$camasEspDispAdult= $_POST["camasEspDispAdult"];
	$camasEspOcupAdult= $_POST["camasEspOcupAdult"];
	$camasCritDispAdult= $_POST["camasCritDispAdult"];
	$camasCritOcupAdult= $_POST["camasCritOcupAdult"];
	$camasGenDispPed= $_POST["camasGenDispPed"];
	$camasGenOcupPed= $_POST["camasGenOcupPed"];
	$camasEspDispPed= $_POST["camasEspDispPed"];
	$camasEspOcupPed= $_POST["camasEspOcupPed"];
	$camasCritDispPed= $_POST["camasCritDispPed"];
	$camasCritOcupPed= $_POST["camasCritOcupPed"];
	$users_cd= $_POST["users_cd"];
	//validar que no exista 
	$sql_valida = "
		SELECT *
			FROM partediario.parte
			WHERE cod_establecimiento = '$nombreestablecimiento' and fecha = '$fecha'
			LIMIT 1
	";
	$parte =$pg->getRow($sql_valida);
	$id_parte = 0;
	if(isset($parte->idparte))
	{
		$id_parte = $parte->idparte;
		
	}
	
	if($id_parte > 0)//hacer update
	{
		$mensaje = "";
	 	$fecha_uptate = date("Y-m-d H:i:s");
	 	$sql_update = "UPDATE partediario.parte SET usuario_mod = '$users_cd' WHERE idparte = '$id_parte'";
		$update =$pg->exec($sql_update);
		$sql_update = "UPDATE partediario.parte SET fecha_mod = '$fecha_uptate' WHERE idparte = '$id_parte'";
		$update =$pg->exec($sql_update);
		$sql_update = "UPDATE partediario.parte SET camas = '$camasdisponibles' WHERE idparte = '$id_parte'";
		$update =$pg->exec($sql_update);
		$sql_update = "UPDATE partediario.parte SET respiradores = '$respiradoresdisponibles' WHERE idparte = '$id_parte'";
		$update =$pg->exec($sql_update);
		$sql_update = 'UPDATE partediario.parte SET "camasGenDispAdult" = '.$camasGenDispAdult.' WHERE idparte = '.$id_parte;
		$update =$pg->exec($sql_update);
		$sql_update = 'UPDATE partediario.parte SET "camasGenOcupAdult" = '.$camasGenOcupAdult.' WHERE idparte = '.$id_parte;
		$update =$pg->exec($sql_update);
		$sql_update = 'UPDATE partediario.parte SET "camasEspDispAdult" = '.$camasEspDispAdult.' WHERE idparte = '.$id_parte;
		$update =$pg->exec($sql_update);
		$sql_update = 'UPDATE partediario.parte SET "camasEspOcupAdult" = '.$camasEspOcupAdult.' WHERE idparte = '.$id_parte;
		$update =$pg->exec($sql_update);
		$sql_update = 'UPDATE partediario.parte SET "camasCritDispAdult" = '.$camasCritDispAdult.' WHERE idparte = '.$id_parte;
		$update =$pg->exec($sql_update);
		$sql_update = 'UPDATE partediario.parte SET "camasCritOcupAdult" = '.$camasCritOcupAdult.' WHERE idparte = '.$id_parte;
		$update =$pg->exec($sql_update);
		$sql_update = 'UPDATE partediario.parte SET "camasGenDispPed" = '.$camasGenDispPed.' WHERE idparte = '.$id_parte;
		$update =$pg->exec($sql_update);
		$sql_update = 'UPDATE partediario.parte SET "camasGenOcupPed" = '.$camasGenOcupPed.' WHERE idparte = '.$id_parte;
		$update =$pg->exec($sql_update);
		$sql_update = 'UPDATE partediario.parte SET "camasEspDispPed" = '.$camasEspDispPed.' WHERE idparte = '.$id_parte;
		$update =$pg->exec($sql_update);
		$sql_update = 'UPDATE partediario.parte SET "camasEspOcupPed" = '.$camasEspOcupPed.' WHERE idparte = '.$id_parte;
		$update =$pg->exec($sql_update);
		$sql_update = 'UPDATE partediario.parte SET "camasCritDispPed" = '.$camasCritDispPed.' WHERE idparte = '.$id_parte;
		$update =$pg->exec($sql_update);
		$sql_update = 'UPDATE partediario.parte SET "camasCritOcupPed" = '.$camasCritOcupPed.' WHERE idparte = '.$id_parte;
		$update =$pg->exec($sql_update);
		
	
		$sql_delete = "DELETE FROM partediario.parte_detalle WHERE fk_parte = $id_parte";
		//echo $sql_delete;
		$sql_delete =$pg->exec($sql_delete);

		$idparte =$id_parte;
		$mensaje = "";

		$mensaje.= "<h4>Código de Parte: ".$idparte."</h4><br />";
		$mensaje.= "Fecha: ".$fecha."<br />";
		$mensaje.= "Establecimiento (SISA): ".$nombreestablecimiento."<br />";
		//$mensaje.= "Camas disponibles: ".$camasdisponibles."<br />";
		$mensaje.= "Respiradores disponibles: ".$respiradoresdisponibles."<br />";
		$mensaje.= "Camas Generales Disponibles Adultos: ".$camasGenDispAdult."<br />";
		$mensaje.= "Camas Generales Ocupadas Adultos: ".$camasGenOcupAdult."<br />";
		$mensaje.= "Camas Especiales Disponibles Adultos: ".$camasEspDispAdult."<br />";
		$mensaje.= "Camas Especiales Ocupadas Adultos: ".$camasEspOcupAdult."<br />";
		$mensaje.= "Camas Críticas Disponibles Adultos: ".$camasCritDispAdult."<br />";
		$mensaje.= "Camas Críticas Ocupadas Adultos: ".$camasCritOcupAdult."<br />";
		$mensaje.= "Camas Generales Disponibles Pedi&aacute;tricas: ".$camasGenDispPed."<br />";
		$mensaje.= "Camas Generales Ocupadas Pedi&aacute;tricas: ".$camasGenOcupPed."<br />";
		$mensaje.= "Camas Especiales Disponibles Pedi&aacute;tricas: ".$camasEspDispPed."<br />";
		$mensaje.= "Camas Especiales Ocupadas Pedi&aacute;tricas: ".$camasEspOcupPed."<br />";
		$mensaje.= "Camas Críticas Disponibles Pedi&aacute;tricas: ".$camasCritDispPed."<br />";
		$mensaje.= "Camas Críticas Ocupadas Pedi&aacute;tricas: ".$camasCritOcupPed."<br />";
		
		$consped = 280;
		$conscm = 281;
		$consguar = 282;

		$mensaje.= "<br />";

		// Consultas Pediatricas
		if($_POST["ped1"] != "0") {
			$ped1 = $_POST["ped1"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consped, 1 , $ped1, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas pediátricas - Menores de 1 año: ".$ped1."<br />";
		}
		if($_POST["ped14"] != "0") {
			$ped14 = $_POST["ped14"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consped, 2 , $ped14, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas pediátricas - 1 a 4 años: ".$ped14."<br />";
		}
		if($_POST["ped514"] != "0") {
			$ped514 = $_POST["ped514"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consped, 3 , $ped514, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas pediátricas - 5 a 14 años: ".$ped514."<br />";
		}
		if($_POST["ped1519"] != "0") {
			$ped1519 = $_POST["ped1519"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consped, 4 , $ped1519, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas pediátricas - 15 a 19 años: ".$ped1519."<br />";
		}
		if($_POST["ped2039"] != "0") {
			$ped2039 = $_POST["ped2039"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consped, 5 , $ped2039, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas pediátricas - 20 a 39 años: ".$ped2039."<br />";
		}
		if($_POST["ped4064"] != "0") {
			$ped4064 = $_POST["ped4064"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consped, 6 , $ped4064, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas pediátricas - 40 a 64 años: ".$ped4064."<br />";
		}
		if($_POST["ped65"] != "0") {
			$ped65 = $_POST["ped65"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consped, 7 , $ped65, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas pediátricas - Mas de 65 años: ".$ped65."<br />";
		}

		$mensaje.= "<br />";

		// Consultas Clinica Medica
		if($_POST["cm1"] != "0") {
			$cm1 = $_POST["cm1"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $conscm, 1 , $cm1, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas Clínica Médica - Menores de 1 año: ".$cm1."<br />";
		}
		if($_POST["cm14"] != "0") {
			$cm14 = $_POST["cm14"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $conscm, 2 , $cm14, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas Clínica Médica - 1 a 4 años: ".$cm14."<br />";
		}
		if($_POST["cm514"] != "0") {
			$cm514 = $_POST["cm514"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $conscm, 3 , $cm514, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas Clínica Médica - 5 a 14 años: ".$cm514."<br />";
		}
		if($_POST["cm1519"] != "0") {
			$cm1519 = $_POST["cm1519"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $conscm, 4 , $cm1519, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas Clínica Médica - 15 a 19 años: ".$cm1519."<br />";
		}
		if($_POST["cm2039"] != "0") {
			$cm2039 = $_POST["cm2039"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $conscm, 5 , $cm2039, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas Clínica Médica - 20 a 39 años: ".$cm2039."<br />";
		}
		if($_POST["cm4064"] != "0") {
			$cm4064 = $_POST["cm4064"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $conscm, 6 , $cm4064, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas Clínica Médica - 40 a 64 años: ".$cm4064."<br />";
		}
		if($_POST["cm65"] != "0") {
			$cm65 = $_POST["cm65"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $conscm, 7 , $cm65, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas Clínica Médica - Mas de 65 años: ".$cm65."<br />";
		}

		$mensaje.= "<br />";

		// Consultas Guardia
		if($_POST["guar1"] != "0") {
			$guar1 = $_POST["guar1"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consguar, 1 , $guar1, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas de Guardia - Menores de 1 año: ".$guar1."<br />";
		}
		if($_POST["guar14"] != "0") {
			$guar14 = $_POST["guar14"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consguar, 2 , $guar14, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas de Guardia - 1 a 4 años: ".$guar14."<br />";
		}
		if($_POST["guar514"] != "0") {
			$guar514 = $_POST["guar514"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consguar, 3 , $guar514, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas de Guardia - 5 a 14 años: ".$guar514."<br />";
		}
		if($_POST["guar1519"] != "0") {
			$guar1519 = $_POST["guar1519"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consguar, 4 , $guar1519, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas de Guardia - 15 a 19 años: ".$guar1519."<br />";
		}
		if($_POST["guar2039"] != "0") {
			$guar2039 = $_POST["guar2039"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consguar, 5 , $guar2039, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas de Guardia - 20 a 39 años: ".$guar2039."<br />";
		}
		if($_POST["guar4064"] != "0") {
			$guar4064 = $_POST["guar4064"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consguar, 6 , $guar4064, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas de Guardia - 40 a 64 años: ".$guar4064."<br />";
		}
		if($_POST["guar65"] != "0") {
			$guar65 = $_POST["guar65"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consguar, 7 , $guar65, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas de Guardia - Mas de 65 años: ".$guar65."<br />";
		}

		$mensaje.= "<br />";

		// Enfermedad 1
		if((isset($_POST["selenf1"])) && (!empty($_POST["selenf1"]))) {
			$enf1 = $_POST["selenf1"];
			if($_POST["enf1_1"] != "0") { 
				$enf1_1 = $_POST["enf1_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf1', 1, $enf1_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf1 - Menores de 1 año: ".$enf1_1."<br />";
			}
			if($_POST["enf1_14"] != "0") { 
				$enf1_14 = $_POST["enf1_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf1', 2, $enf1_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf1 - 1 a 4 años: ".$enf1_14."<br />";
			}
			if($_POST["enf1_514"] != "0") { 
				$enf1_514 = $_POST["enf1_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf1', 3, $enf1_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf1 - 5 a 14 años: ".$enf1_514."<br />";
			}
			if($_POST["enf1_1519"] != "0") { 
				$enf1_1519 = $_POST["enf1_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf1', 4, $enf1_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf1 - 15 a 19 años: ".$enf1_1519."<br />";
			}
			if($_POST["enf1_2039"] != "0") { 
				$enf1_2039 = $_POST["enf1_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf1', 5, $enf1_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf1 - 20 a 39 años: ".$enf1_2039."<br />";
			}
			if($_POST["enf1_4064"] != "0") { 
				$enf1_4064 = $_POST["enf1_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf1', 6, $enf1_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf1 - 40 a 64 años: ".$enf1_4064."<br />";
			}
			if($_POST["enf1_65"] != "0") { 
				$enf1_65 = $_POST["enf1_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf1', 7, $enf1_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf1 - Mas de 65 años: ".$enf1_65."<br />";
			}
		}

		$mensaje.= "<br />";

		// Enfermedad 2
		if((isset($_POST["selenf2"])) && (!empty($_POST["selenf2"]))) {
			$enf2 = $_POST["selenf2"];
			if($_POST["enf2_1"] != "0") { 
				$enf2_1 = $_POST["enf2_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf2', 1, $enf2_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf2 - Menores de 1 año: ".$enf2_1."<br />";
			}		
			if($_POST["enf2_14"] != "0") { 
				$enf2_14 = $_POST["enf2_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf2', 2, $enf2_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf2 - 1 a 4 años: ".$enf2_14."<br />";
			}		
			if($_POST["enf2_514"] != "0") { 
				$enf2_514 = $_POST["enf2_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf2', 3, $enf2_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf2 - 5 a 14 años: ".$enf2_514."<br />";
			}		
			if($_POST["enf2_1519"] != "0") { 
				$enf2_1519 = $_POST["enf2_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf2', 4, $enf2_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf2 - 15 a 19 años: ".$enf2_1519."<br />";
			}		
			if($_POST["enf2_2039"] != "0") { 
				$enf2_2039 = $_POST["enf2_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf2', 5, $enf2_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf2 - 20 a 39 años: ".$enf2_2039."<br />";
			}		
			if($_POST["enf2_4064"] != "0") { 
				$enf2_4064 = $_POST["enf2_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf2', 6, $enf2_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf2 - 40 a 64 años: ".$enf2_4064."<br />";
			}		
			if($_POST["enf2_65"] != "0") { 
				$enf2_65 = $_POST["enf2_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf2', 7, $enf2_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf2 - Mas de 65 años: ".$enf2_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 3
		if((isset($_POST["selenf3"])) && (!empty($_POST["selenf3"]))) {
			$enf3 = $_POST["selenf3"];
			if($_POST["enf3_1"] != "0") { 
				$enf3_1 = $_POST["enf3_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf3', 1, $enf3_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf3 - Menores de 1 año: ".$enf3_1."<br />";
			}		
			if($_POST["enf3_14"] != "0") { 
				$enf3_14 = $_POST["enf3_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf3', 2, $enf3_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf3 - 1 a 4 años: ".$enf3_14."<br />";
			}		
			if($_POST["enf3_514"] != "0") { 
				$enf3_514 = $_POST["enf3_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf3', 3, $enf3_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf3 - 5 a 14 años: ".$enf3_514."<br />";
			}		
			if($_POST["enf3_1519"] != "0") { 
				$enf3_1519 = $_POST["enf3_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf3', 4, $enf3_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf3 - 15 a 19 años: ".$enf3_1519."<br />";
			}		
			if($_POST["enf3_2039"] != "0") { 
				$enf3_2039 = $_POST["enf3_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf3', 5, $enf3_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf3 - 20 a 39 años: ".$enf3_2039."<br />";
			}		
			if($_POST["enf3_4064"] != "0") { 
				$enf3_4064 = $_POST["enf3_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf3', 6, $enf3_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf3 - 40 a 64 años: ".$enf3_4064."<br />";
			}		
			if($_POST["enf3_65"] != "0") { 
				$enf3_65 = $_POST["enf3_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf3', 7, $enf3_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf3 - Mas de 65 años: ".$enf3_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 4
		if((isset($_POST["selenf4"])) && (!empty($_POST["selenf4"]))) {
			$enf4 = $_POST["selenf4"];
			if($_POST["enf4_1"] != "0") { 
				$enf4_1 = $_POST["enf4_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf4', 1, $enf4_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf4 - Menores de 1 año: ".$enf4_1."<br />";
			}		
			if($_POST["enf4_14"] != "0") { 
				$enf4_14 = $_POST["enf4_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf4', 2, $enf4_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf4 - 1 a 4 años: ".$enf4_14."<br />";
			}		
			if($_POST["enf4_514"] != "0") { 
				$enf4_514 = $_POST["enf4_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf4', 3, $enf4_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf4 - 5 a 14 años: ".$enf4_514."<br />";
			}		
			if($_POST["enf4_1519"] != "0") { 
				$enf4_1519 = $_POST["enf4_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf4', 4, $enf4_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf4 - 15 a 19 años: ".$enf4_1519."<br />";
			}		
			if($_POST["enf4_2039"] != "0") { 
				$enf4_2039 = $_POST["enf4_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf4', 5, $enf4_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf4 - 20 a 39 años: ".$enf4_2039."<br />";
			}		
			if($_POST["enf4_4064"] != "0") { 
				$enf4_4064 = $_POST["enf4_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf4', 6, $enf4_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf4 - 40 a 64 años: ".$enf4_4064."<br />";
			}		
			if($_POST["enf4_65"] != "0") { 
				$enf4_65 = $_POST["enf4_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf4', 7, $enf4_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf4 - Mas de 65 años: ".$enf4_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 5
		if((isset($_POST["selenf5"])) && (!empty($_POST["selenf5"]))) {
			$enf5 = $_POST["selenf5"];
			if($_POST["enf5_1"] != "0") { 
				$enf5_1 = $_POST["enf5_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf5', 1, $enf5_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf5 - Menores de 1 año: ".$enf5_1."<br />";
			}		
			if($_POST["enf5_14"] != "0") { 
				$enf5_14 = $_POST["enf5_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf5', 2, $enf5_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf5 - 1 a 4 años: ".$enf5_14."<br />";
			}		
			if($_POST["enf5_514"] != "0") { 
				$enf5_514 = $_POST["enf5_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf5', 3, $enf5_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf5 - 5 a 14 años: ".$enf5_514."<br />";
			}		
			if($_POST["enf5_1519"] != "0") { 
				$enf5_1519 = $_POST["enf5_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf5', 4, $enf5_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf5 - 15 a 19 años: ".$enf5_1519."<br />";
			}		
			if($_POST["enf5_2039"] != "0") { 
				$enf5_2039 = $_POST["enf5_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf5', 5, $enf5_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf5 - 20 a 39 años: ".$enf5_2039."<br />";
			}		
			if($_POST["enf5_4064"] != "0") { 
				$enf5_4064 = $_POST["enf5_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf5', 6, $enf5_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf5 - 40 a 64 años: ".$enf5_4064."<br />";
			}		
			if($_POST["enf5_65"] != "0") { 
				$enf5_65 = $_POST["enf5_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf5', 7, $enf5_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf5 - Mas de 65 años: ".$enf5_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 6
		if((isset($_POST["selenf6"])) && (!empty($_POST["selenf6"]))) {
			$enf6 = $_POST["selenf6"];
			if($_POST["enf6_1"] != "0") { 
				$enf6_1 = $_POST["enf6_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf6', 1, $enf6_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf6 - Menores de 1 año: ".$enf6_1."<br />";
			}		
			if($_POST["enf6_14"] != "0") { 
				$enf6_14 = $_POST["enf6_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf6', 2, $enf6_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf6 - 1 a 4 años: ".$enf6_14."<br />";
			}		
			if($_POST["enf6_514"] != "0") { 
				$enf6_514 = $_POST["enf6_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf6', 3, $enf6_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf6 - 5 a 14 años: ".$enf6_514."<br />";
			}		
			if($_POST["enf6_1519"] != "0") { 
				$enf6_1519 = $_POST["enf6_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf6', 4, $enf6_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf6 - 15 a 19 años: ".$enf6_1519."<br />";
			}		
			if($_POST["enf6_2039"] != "0") { 
				$enf6_2039 = $_POST["enf6_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf6', 5, $enf6_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf6 - 20 a 39 años: ".$enf6_2039."<br />";
			}		
			if($_POST["enf6_4064"] != "0") { 
				$enf6_4064 = $_POST["enf6_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf6', 6, $enf6_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf6 - 40 a 64 años: ".$enf6_4064."<br />";
			}		
			if($_POST["enf6_65"] != "0") { 
				$enf6_65 = $_POST["enf6_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf6', 7, $enf6_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf6 - Mas de 65 años: ".$enf6_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 7
		if((isset($_POST["selenf7"])) && (!empty($_POST["selenf7"]))) {
			$enf7 = $_POST["selenf7"];
			if($_POST["enf7_1"] != "0") { 
				$enf7_1 = $_POST["enf7_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf7', 1, $enf7_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf7 - Menores de 1 año: ".$enf7_1."<br />";
			}		
			if($_POST["enf7_14"] != "0") { 
				$enf7_14 = $_POST["enf7_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf7', 2, $enf7_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf7 - 1 a 4 años: ".$enf7_14."<br />";
			}		
			if($_POST["enf7_514"] != "0") { 
				$enf7_514 = $_POST["enf7_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf7', 3, $enf7_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf7 - 5 a 14 años: ".$enf7_514."<br />";
			}		
			if($_POST["enf7_1519"] != "0") { 
				$enf7_1519 = $_POST["enf7_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf7', 4, $enf7_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf7 - 15 a 19 años: ".$enf7_1519."<br />";
			}		
			if($_POST["enf7_2039"] != "0") { 
				$enf7_2039 = $_POST["enf7_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf7', 5, $enf7_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf7 - 20 a 39 años: ".$enf7_2039."<br />";
			}		
			if($_POST["enf7_4064"] != "0") { 
				$enf7_4064 = $_POST["enf7_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf7', 6, $enf7_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf7 - 40 a 64 años: ".$enf7_4064."<br />";
			}		
			if($_POST["enf7_65"] != "0") { 
				$enf7_65 = $_POST["enf7_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf7', 7, $enf7_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf7 - Mas de 65 años: ".$enf7_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 8
		if((isset($_POST["selenf8"])) && (!empty($_POST["selenf8"]))) {
			$enf8 = $_POST["selenf8"];
			if($_POST["enf8_1"] != "0") { 
				$enf8_1 = $_POST["enf8_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf8', 1, $enf8_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf8 - Menores de 1 año: ".$enf8_1."<br />";
			}		
			if($_POST["enf8_14"] != "0") { 
				$enf8_14 = $_POST["enf8_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf8', 2, $enf8_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf8 - 1 a 4 años: ".$enf8_14."<br />";
			}		
			if($_POST["enf8_514"] != "0") { 
				$enf8_514 = $_POST["enf8_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf8', 3, $enf8_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf8 - 5 a 14 años: ".$enf8_514."<br />";
			}		
			if($_POST["enf8_1519"] != "0") { 
				$enf8_1519 = $_POST["enf8_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf8', 4, $enf8_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf8 - 15 a 19 años: ".$enf8_1519."<br />";
			}		
			if($_POST["enf8_2039"] != "0") { 
				$enf8_2039 = $_POST["enf8_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf8', 5, $enf8_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf8 - 20 a 39 años: ".$enf8_2039."<br />";
			}		
			if($_POST["enf8_4064"] != "0") { 
				$enf8_4064 = $_POST["enf8_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf8', 6, $enf8_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf8 - 40 a 64 años: ".$enf8_4064."<br />";
			}		
			if($_POST["enf8_65"] != "0") { 
				$enf8_65 = $_POST["enf8_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf8', 7, $enf8_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf8 - Mas de 65 años: ".$enf8_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 9
		if((isset($_POST["selenf9"])) && (!empty($_POST["selenf9"]))) {
			$enf9 = $_POST["selenf9"];
			if($_POST["enf9_1"] != "0") { 
				$enf9_1 = $_POST["enf9_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf9', 1, $enf9_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf9 - Menores de 1 año: ".$enf9_1."<br />";
			}		
			if($_POST["enf9_14"] != "0") { 
				$enf9_14 = $_POST["enf9_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf9', 2, $enf9_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf9 - 1 a 4 años: ".$enf9_14."<br />";
			}		
			if($_POST["enf9_514"] != "0") { 
				$enf9_514 = $_POST["enf9_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf9', 3, $enf9_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf9 - 5 a 14 años: ".$enf9_514."<br />";
			}		
			if($_POST["enf9_1519"] != "0") { 
				$enf9_1519 = $_POST["enf9_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf9', 4, $enf9_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf9 - 15 a 19 años: ".$enf9_1519."<br />";
			}		
			if($_POST["enf9_2039"] != "0") { 
				$enf9_2039 = $_POST["enf9_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf9', 5, $enf9_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf9 - 20 a 39 años: ".$enf9_2039."<br />";
			}		
			if($_POST["enf9_4064"] != "0") { 
				$enf9_4064 = $_POST["enf9_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf9', 6, $enf9_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf9 - 40 a 64 años: ".$enf9_4064."<br />";
			}		
			if($_POST["enf9_65"] != "0") { 
				$enf9_65 = $_POST["enf9_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf9', 7, $enf9_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf9 - Mas de 65 años: ".$enf9_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 10
		if((isset($_POST["selenf10"])) && (!empty($_POST["selenf10"]))) {
			$enf10 = $_POST["selenf10"];
			if($_POST["enf10_1"] != "0") { 
				$enf10_1 = $_POST["enf10_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf10', 1, $enf10_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf10 - Menores de 1 año: ".$enf10_1."<br />";
			}		
			if($_POST["enf10_14"] != "0") { 
				$enf10_14 = $_POST["enf10_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf10', 2, $enf10_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf10 - 1 a 4 años: ".$enf10_14."<br />";
			}		
			if($_POST["enf10_514"] != "0") { 
				$enf10_514 = $_POST["enf10_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf10', 3, $enf10_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf10 - 5 a 14 años: ".$enf10_514."<br />";
			}		
			if($_POST["enf10_1519"] != "0") { 
				$enf10_1519 = $_POST["enf10_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf10', 4, $enf10_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf10 - 15 a 19 años: ".$enf10_1519."<br />";
			}		
			if($_POST["enf10_2039"] != "0") { 
				$enf10_2039 = $_POST["enf10_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf10', 5, $enf10_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf10 - 20 a 39 años: ".$enf10_2039."<br />";
			}		
			if($_POST["enf10_4064"] != "0") { 
				$enf10_4064 = $_POST["enf10_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf10', 6, $enf10_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf10 - 40 a 64 años: ".$enf10_4064."<br />";
			}		
			if($_POST["enf10_65"] != "0") { 
				$enf10_65 = $_POST["enf10_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf10', 7, $enf10_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf10 - Mas de 65 años: ".$enf10_65."<br />";
			}		
		}
	}
	else//insert todo
	{
		
		
		// Parte
		/*$sql = "INSERT INTO partediario.parte (\"camas\", \"cod_establecimiento\", \"fecha\", \"respiradores\", \"usuario\") 
				VALUES ($camasdisponibles, '$nombreestablecimiento' , '$fecha', $respiradoresdisponibles, '');";*/
		$sql = "INSERT INTO partediario.parte (\"camas\", \"cod_establecimiento\", \"fecha\", \"respiradores\", \"usuario\", \"camasGenDispAdult\", \"camasGenOcupAdult\", \"camasEspDispAdult\", \"camasEspOcupAdult\", \"camasCritDispAdult\", \"camasCritOcupAdult\", \"camasGenDispPed\", \"camasGenOcupPed\", \"camasEspDispPed\", \"camasEspOcupPed\", \"camasCritDispPed\", \"camasCritOcupPed\") 
				VALUES (0, '$nombreestablecimiento' , '$fecha', $respiradoresdisponibles, '$users_cd', $camasGenDispAdult, $camasGenOcupAdult, $camasEspDispAdult, $camasEspOcupAdult, $camasCritDispAdult, $camasCritOcupAdult, $camasGenDispPed, $camasGenOcupPed, $camasEspDispPed, $camasEspOcupPed, $camasCritDispPed, $camasCritOcupPed);";
		$idparte = $pg->insert($sql, "idparte");
		$mensaje = "";

		$mensaje.= "<h4>Código de Parte: ".$idparte."</h4><br />";
		$mensaje.= "Fecha: ".$fecha."<br />";
		$mensaje.= "Establecimiento (SISA): ".$nombreestablecimiento."<br />";
		//$mensaje.= "Camas disponibles: ".$camasdisponibles."<br />";
		$mensaje.= "Respiradores disponibles: ".$respiradoresdisponibles."<br />";
		$mensaje.= "Camas Generales Disponibles Adultos: ".$camasGenDispAdult."<br />";
		$mensaje.= "Camas Generales Ocupadas Adultos: ".$camasGenOcupAdult."<br />";
		$mensaje.= "Camas Especiales Disponibles Adultos: ".$camasEspDispAdult."<br />";
		$mensaje.= "Camas Especiales Ocupadas Adultos: ".$camasEspOcupAdult."<br />";
		$mensaje.= "Camas Críticas Disponibles Adultos: ".$camasCritDispAdult."<br />";
		$mensaje.= "Camas Críticas Ocupadas Adultos: ".$camasCritOcupAdult."<br />";
		$mensaje.= "Camas Generales Disponibles Pedi&aacute;tricas: ".$camasGenDispPed."<br />";
		$mensaje.= "Camas Generales Ocupadas Pedi&aacute;tricas: ".$camasGenOcupPed."<br />";
		$mensaje.= "Camas Especiales Disponibles Pedi&aacute;tricas: ".$camasEspDispPed."<br />";
		$mensaje.= "Camas Especiales Ocupadas Pedi&aacute;tricas: ".$camasEspOcupPed."<br />";
		$mensaje.= "Camas Críticas Disponibles Pedi&aacute;tricas: ".$camasCritDispPed."<br />";
		$mensaje.= "Camas Críticas Ocupadas Pedi&aacute;tricas: ".$camasCritOcupPed."<br />";
		
		$consped = 280;
		$conscm = 281;
		$consguar = 282;

		$mensaje.= "<br />";

		// Consultas Pediatricas
		if($_POST["ped1"] != "0") {
			$ped1 = $_POST["ped1"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consped, 1 , $ped1, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas pediátricas - Menores de 1 año: ".$ped1."<br />";
		}
		if($_POST["ped14"] != "0") {
			$ped14 = $_POST["ped14"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consped, 2 , $ped14, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas pediátricas - 1 a 4 años: ".$ped14."<br />";
		}
		if($_POST["ped514"] != "0") {
			$ped514 = $_POST["ped514"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consped, 3 , $ped514, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas pediátricas - 5 a 14 años: ".$ped514."<br />";
		}
		if($_POST["ped1519"] != "0") {
			$ped1519 = $_POST["ped1519"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consped, 4 , $ped1519, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas pediátricas - 15 a 19 años: ".$ped1519."<br />";
		}
		if($_POST["ped2039"] != "0") {
			$ped2039 = $_POST["ped2039"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consped, 5 , $ped2039, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas pediátricas - 20 a 39 años: ".$ped2039."<br />";
		}
		if($_POST["ped4064"] != "0") {
			$ped4064 = $_POST["ped4064"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consped, 6 , $ped4064, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas pediátricas - 40 a 64 años: ".$ped4064."<br />";
		}
		if($_POST["ped65"] != "0") {
			$ped65 = $_POST["ped65"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consped, 7 , $ped65, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas pediátricas - Mas de 65 años: ".$ped65."<br />";
		}

		$mensaje.= "<br />";

		// Consultas Clinica Medica
		if($_POST["cm1"] != "0") {
			$cm1 = $_POST["cm1"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $conscm, 1 , $cm1, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas Clínica Médica - Menores de 1 año: ".$cm1."<br />";
		}
		if($_POST["cm14"] != "0") {
			$cm14 = $_POST["cm14"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $conscm, 2 , $cm14, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas Clínica Médica - 1 a 4 años: ".$cm14."<br />";
		}
		if($_POST["cm514"] != "0") {
			$cm514 = $_POST["cm514"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $conscm, 3 , $cm514, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas Clínica Médica - 5 a 14 años: ".$cm514."<br />";
		}
		if($_POST["cm1519"] != "0") {
			$cm1519 = $_POST["cm1519"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $conscm, 4 , $cm1519, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas Clínica Médica - 15 a 19 años: ".$cm1519."<br />";
		}
		if($_POST["cm2039"] != "0") {
			$cm2039 = $_POST["cm2039"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $conscm, 5 , $cm2039, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas Clínica Médica - 20 a 39 años: ".$cm2039."<br />";
		}
		if($_POST["cm4064"] != "0") {
			$cm4064 = $_POST["cm4064"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $conscm, 6 , $cm4064, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas Clínica Médica - 40 a 64 años: ".$cm4064."<br />";
		}
		if($_POST["cm65"] != "0") {
			$cm65 = $_POST["cm65"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $conscm, 7 , $cm65, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas Clínica Médica - Mas de 65 años: ".$cm65."<br />";
		}

		$mensaje.= "<br />";

		// Consultas Guardia
		if($_POST["guar1"] != "0") {
			$guar1 = $_POST["guar1"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consguar, 1 , $guar1, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas de Guardia - Menores de 1 año: ".$guar1."<br />";
		}
		if($_POST["guar14"] != "0") {
			$guar14 = $_POST["guar14"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consguar, 2 , $guar14, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas de Guardia - 1 a 4 años: ".$guar14."<br />";
		}
		if($_POST["guar514"] != "0") {
			$guar514 = $_POST["guar514"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consguar, 3 , $guar514, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas de Guardia - 5 a 14 años: ".$guar514."<br />";
		}
		if($_POST["guar1519"] != "0") {
			$guar1519 = $_POST["guar1519"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consguar, 4 , $guar1519, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas de Guardia - 15 a 19 años: ".$guar1519."<br />";
		}
		if($_POST["guar2039"] != "0") {
			$guar2039 = $_POST["guar2039"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consguar, 5 , $guar2039, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas de Guardia - 20 a 39 años: ".$guar2039."<br />";
		}
		if($_POST["guar4064"] != "0") {
			$guar4064 = $_POST["guar4064"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consguar, 6 , $guar4064, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas de Guardia - 40 a 64 años: ".$guar4064."<br />";
		}
		if($_POST["guar65"] != "0") {
			$guar65 = $_POST["guar65"];
			$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") VALUES ($idparte, $consguar, 7 , $guar65, 'C');";
			$iddetalle = $pg->insert($sql, "iddet");
			$mensaje.= "Consultas de Guardia - Mas de 65 años: ".$guar65."<br />";
		}

		$mensaje.= "<br />";

		// Enfermedad 1
		if((isset($_POST["selenf1"])) && (!empty($_POST["selenf1"]))) {
			$enf1 = $_POST["selenf1"];
			if($_POST["enf1_1"] != "0") { 
				$enf1_1 = $_POST["enf1_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf1', 1, $enf1_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf1 - Menores de 1 año: ".$enf1_1."<br />";
			}
			if($_POST["enf1_14"] != "0") { 
				$enf1_14 = $_POST["enf1_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf1', 2, $enf1_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf1 - 1 a 4 años: ".$enf1_14."<br />";
			}
			if($_POST["enf1_514"] != "0") { 
				$enf1_514 = $_POST["enf1_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf1', 3, $enf1_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf1 - 5 a 14 años: ".$enf1_514."<br />";
			}
			if($_POST["enf1_1519"] != "0") { 
				$enf1_1519 = $_POST["enf1_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf1', 4, $enf1_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf1 - 15 a 19 años: ".$enf1_1519."<br />";
			}
			if($_POST["enf1_2039"] != "0") { 
				$enf1_2039 = $_POST["enf1_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf1', 5, $enf1_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf1 - 20 a 39 años: ".$enf1_2039."<br />";
			}
			if($_POST["enf1_4064"] != "0") { 
				$enf1_4064 = $_POST["enf1_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf1', 6, $enf1_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf1 - 40 a 64 años: ".$enf1_4064."<br />";
			}
			if($_POST["enf1_65"] != "0") { 
				$enf1_65 = $_POST["enf1_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf1', 7, $enf1_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf1 - Mas de 65 años: ".$enf1_65."<br />";
			}
		}

		$mensaje.= "<br />";

		// Enfermedad 2
		if((isset($_POST["selenf2"])) && (!empty($_POST["selenf2"]))) {
			$enf2 = $_POST["selenf2"];
			if($_POST["enf2_1"] != "0") { 
				$enf2_1 = $_POST["enf2_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf2', 1, $enf2_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf2 - Menores de 1 año: ".$enf2_1."<br />";
			}		
			if($_POST["enf2_14"] != "0") { 
				$enf2_14 = $_POST["enf2_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf2', 2, $enf2_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf2 - 1 a 4 años: ".$enf2_14."<br />";
			}		
			if($_POST["enf2_514"] != "0") { 
				$enf2_514 = $_POST["enf2_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf2', 3, $enf2_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf2 - 5 a 14 años: ".$enf2_514."<br />";
			}		
			if($_POST["enf2_1519"] != "0") { 
				$enf2_1519 = $_POST["enf2_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf2', 4, $enf2_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf2 - 15 a 19 años: ".$enf2_1519."<br />";
			}		
			if($_POST["enf2_2039"] != "0") { 
				$enf2_2039 = $_POST["enf2_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf2', 5, $enf2_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf2 - 20 a 39 años: ".$enf2_2039."<br />";
			}		
			if($_POST["enf2_4064"] != "0") { 
				$enf2_4064 = $_POST["enf2_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf2', 6, $enf2_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf2 - 40 a 64 años: ".$enf2_4064."<br />";
			}		
			if($_POST["enf2_65"] != "0") { 
				$enf2_65 = $_POST["enf2_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf2', 7, $enf2_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf2 - Mas de 65 años: ".$enf2_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 3
		if((isset($_POST["selenf3"])) && (!empty($_POST["selenf3"]))) {
			$enf3 = $_POST["selenf3"];
			if($_POST["enf3_1"] != "0") { 
				$enf3_1 = $_POST["enf3_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf3', 1, $enf3_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf3 - Menores de 1 año: ".$enf3_1."<br />";
			}		
			if($_POST["enf3_14"] != "0") { 
				$enf3_14 = $_POST["enf3_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf3', 2, $enf3_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf3 - 1 a 4 años: ".$enf3_14."<br />";
			}		
			if($_POST["enf3_514"] != "0") { 
				$enf3_514 = $_POST["enf3_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf3', 3, $enf3_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf3 - 5 a 14 años: ".$enf3_514."<br />";
			}		
			if($_POST["enf3_1519"] != "0") { 
				$enf3_1519 = $_POST["enf3_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf3', 4, $enf3_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf3 - 15 a 19 años: ".$enf3_1519."<br />";
			}		
			if($_POST["enf3_2039"] != "0") { 
				$enf3_2039 = $_POST["enf3_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf3', 5, $enf3_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf3 - 20 a 39 años: ".$enf3_2039."<br />";
			}		
			if($_POST["enf3_4064"] != "0") { 
				$enf3_4064 = $_POST["enf3_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf3', 6, $enf3_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf3 - 40 a 64 años: ".$enf3_4064."<br />";
			}		
			if($_POST["enf3_65"] != "0") { 
				$enf3_65 = $_POST["enf3_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf3', 7, $enf3_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf3 - Mas de 65 años: ".$enf3_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 4
		if((isset($_POST["selenf4"])) && (!empty($_POST["selenf4"]))) {
			$enf4 = $_POST["selenf4"];
			if($_POST["enf4_1"] != "0") { 
				$enf4_1 = $_POST["enf4_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf4', 1, $enf4_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf4 - Menores de 1 año: ".$enf4_1."<br />";
			}		
			if($_POST["enf4_14"] != "0") { 
				$enf4_14 = $_POST["enf4_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf4', 2, $enf4_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf4 - 1 a 4 años: ".$enf4_14."<br />";
			}		
			if($_POST["enf4_514"] != "0") { 
				$enf4_514 = $_POST["enf4_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf4', 3, $enf4_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf4 - 5 a 14 años: ".$enf4_514."<br />";
			}		
			if($_POST["enf4_1519"] != "0") { 
				$enf4_1519 = $_POST["enf4_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf4', 4, $enf4_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf4 - 15 a 19 años: ".$enf4_1519."<br />";
			}		
			if($_POST["enf4_2039"] != "0") { 
				$enf4_2039 = $_POST["enf4_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf4', 5, $enf4_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf4 - 20 a 39 años: ".$enf4_2039."<br />";
			}		
			if($_POST["enf4_4064"] != "0") { 
				$enf4_4064 = $_POST["enf4_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf4', 6, $enf4_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf4 - 40 a 64 años: ".$enf4_4064."<br />";
			}		
			if($_POST["enf4_65"] != "0") { 
				$enf4_65 = $_POST["enf4_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf4', 7, $enf4_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf4 - Mas de 65 años: ".$enf4_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 5
		if((isset($_POST["selenf5"])) && (!empty($_POST["selenf5"]))) {
			$enf5 = $_POST["selenf5"];
			if($_POST["enf5_1"] != "0") { 
				$enf5_1 = $_POST["enf5_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf5', 1, $enf5_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf5 - Menores de 1 año: ".$enf5_1."<br />";
			}		
			if($_POST["enf5_14"] != "0") { 
				$enf5_14 = $_POST["enf5_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf5', 2, $enf5_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf5 - 1 a 4 años: ".$enf5_14."<br />";
			}		
			if($_POST["enf5_514"] != "0") { 
				$enf5_514 = $_POST["enf5_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf5', 3, $enf5_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf5 - 5 a 14 años: ".$enf5_514."<br />";
			}		
			if($_POST["enf5_1519"] != "0") { 
				$enf5_1519 = $_POST["enf5_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf5', 4, $enf5_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf5 - 15 a 19 años: ".$enf5_1519."<br />";
			}		
			if($_POST["enf5_2039"] != "0") { 
				$enf5_2039 = $_POST["enf5_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf5', 5, $enf5_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf5 - 20 a 39 años: ".$enf5_2039."<br />";
			}		
			if($_POST["enf5_4064"] != "0") { 
				$enf5_4064 = $_POST["enf5_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf5', 6, $enf5_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf5 - 40 a 64 años: ".$enf5_4064."<br />";
			}		
			if($_POST["enf5_65"] != "0") { 
				$enf5_65 = $_POST["enf5_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf5', 7, $enf5_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf5 - Mas de 65 años: ".$enf5_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 6
		if((isset($_POST["selenf6"])) && (!empty($_POST["selenf6"]))) {
			$enf6 = $_POST["selenf6"];
			if($_POST["enf6_1"] != "0") { 
				$enf6_1 = $_POST["enf6_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf6', 1, $enf6_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf6 - Menores de 1 año: ".$enf6_1."<br />";
			}		
			if($_POST["enf6_14"] != "0") { 
				$enf6_14 = $_POST["enf6_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf6', 2, $enf6_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf6 - 1 a 4 años: ".$enf6_14."<br />";
			}		
			if($_POST["enf6_514"] != "0") { 
				$enf6_514 = $_POST["enf6_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf6', 3, $enf6_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf6 - 5 a 14 años: ".$enf6_514."<br />";
			}		
			if($_POST["enf6_1519"] != "0") { 
				$enf6_1519 = $_POST["enf6_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf6', 4, $enf6_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf6 - 15 a 19 años: ".$enf6_1519."<br />";
			}		
			if($_POST["enf6_2039"] != "0") { 
				$enf6_2039 = $_POST["enf6_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf6', 5, $enf6_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf6 - 20 a 39 años: ".$enf6_2039."<br />";
			}		
			if($_POST["enf6_4064"] != "0") { 
				$enf6_4064 = $_POST["enf6_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf6', 6, $enf6_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf6 - 40 a 64 años: ".$enf6_4064."<br />";
			}		
			if($_POST["enf6_65"] != "0") { 
				$enf6_65 = $_POST["enf6_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf6', 7, $enf6_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf6 - Mas de 65 años: ".$enf6_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 7
		if((isset($_POST["selenf7"])) && (!empty($_POST["selenf7"]))) {
			$enf7 = $_POST["selenf7"];
			if($_POST["enf7_1"] != "0") { 
				$enf7_1 = $_POST["enf7_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf7', 1, $enf7_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf7 - Menores de 1 año: ".$enf7_1."<br />";
			}		
			if($_POST["enf7_14"] != "0") { 
				$enf7_14 = $_POST["enf7_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf7', 2, $enf7_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf7 - 1 a 4 años: ".$enf7_14."<br />";
			}		
			if($_POST["enf7_514"] != "0") { 
				$enf7_514 = $_POST["enf7_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf7', 3, $enf7_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf7 - 5 a 14 años: ".$enf7_514."<br />";
			}		
			if($_POST["enf7_1519"] != "0") { 
				$enf7_1519 = $_POST["enf7_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf7', 4, $enf7_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf7 - 15 a 19 años: ".$enf7_1519."<br />";
			}		
			if($_POST["enf7_2039"] != "0") { 
				$enf7_2039 = $_POST["enf7_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf7', 5, $enf7_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf7 - 20 a 39 años: ".$enf7_2039."<br />";
			}		
			if($_POST["enf7_4064"] != "0") { 
				$enf7_4064 = $_POST["enf7_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf7', 6, $enf7_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf7 - 40 a 64 años: ".$enf7_4064."<br />";
			}		
			if($_POST["enf7_65"] != "0") { 
				$enf7_65 = $_POST["enf7_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf7', 7, $enf7_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf7 - Mas de 65 años: ".$enf7_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 8
		if((isset($_POST["selenf8"])) && (!empty($_POST["selenf8"]))) {
			$enf8 = $_POST["selenf8"];
			if($_POST["enf8_1"] != "0") { 
				$enf8_1 = $_POST["enf8_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf8', 1, $enf8_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf8 - Menores de 1 año: ".$enf8_1."<br />";
			}		
			if($_POST["enf8_14"] != "0") { 
				$enf8_14 = $_POST["enf8_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf8', 2, $enf8_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf8 - 1 a 4 años: ".$enf8_14."<br />";
			}		
			if($_POST["enf8_514"] != "0") { 
				$enf8_514 = $_POST["enf8_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf8', 3, $enf8_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf8 - 5 a 14 años: ".$enf8_514."<br />";
			}		
			if($_POST["enf8_1519"] != "0") { 
				$enf8_1519 = $_POST["enf8_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf8', 4, $enf8_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf8 - 15 a 19 años: ".$enf8_1519."<br />";
			}		
			if($_POST["enf8_2039"] != "0") { 
				$enf8_2039 = $_POST["enf8_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf8', 5, $enf8_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf8 - 20 a 39 años: ".$enf8_2039."<br />";
			}		
			if($_POST["enf8_4064"] != "0") { 
				$enf8_4064 = $_POST["enf8_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf8', 6, $enf8_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf8 - 40 a 64 años: ".$enf8_4064."<br />";
			}		
			if($_POST["enf8_65"] != "0") { 
				$enf8_65 = $_POST["enf8_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf8', 7, $enf8_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf8 - Mas de 65 años: ".$enf8_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 9
		if((isset($_POST["selenf9"])) && (!empty($_POST["selenf9"]))) {
			$enf9 = $_POST["selenf9"];
			if($_POST["enf9_1"] != "0") { 
				$enf9_1 = $_POST["enf9_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf9', 1, $enf9_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf9 - Menores de 1 año: ".$enf9_1."<br />";
			}		
			if($_POST["enf9_14"] != "0") { 
				$enf9_14 = $_POST["enf9_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf9', 2, $enf9_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf9 - 1 a 4 años: ".$enf9_14."<br />";
			}		
			if($_POST["enf9_514"] != "0") { 
				$enf9_514 = $_POST["enf9_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf9', 3, $enf9_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf9 - 5 a 14 años: ".$enf9_514."<br />";
			}		
			if($_POST["enf9_1519"] != "0") { 
				$enf9_1519 = $_POST["enf9_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf9', 4, $enf9_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf9 - 15 a 19 años: ".$enf9_1519."<br />";
			}		
			if($_POST["enf9_2039"] != "0") { 
				$enf9_2039 = $_POST["enf9_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf9', 5, $enf9_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf9 - 20 a 39 años: ".$enf9_2039."<br />";
			}		
			if($_POST["enf9_4064"] != "0") { 
				$enf9_4064 = $_POST["enf9_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf9', 6, $enf9_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf9 - 40 a 64 años: ".$enf9_4064."<br />";
			}		
			if($_POST["enf9_65"] != "0") { 
				$enf9_65 = $_POST["enf9_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf9', 7, $enf9_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf9 - Mas de 65 años: ".$enf9_65."<br />";
			}		
		}

		$mensaje.= "<br />";

		// Enfermedad 10
		if((isset($_POST["selenf10"])) && (!empty($_POST["selenf10"]))) {
			$enf10 = $_POST["selenf10"];
			if($_POST["enf10_1"] != "0") { 
				$enf10_1 = $_POST["enf10_1"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf10', 1, $enf10_1, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf10 - Menores de 1 año: ".$enf10_1."<br />";
			}		
			if($_POST["enf10_14"] != "0") { 
				$enf10_14 = $_POST["enf10_14"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf10', 2, $enf10_14, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf10 - 1 a 4 años: ".$enf10_14."<br />";
			}		
			if($_POST["enf10_514"] != "0") { 
				$enf10_514 = $_POST["enf10_514"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf10', 3, $enf10_514, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf10 - 5 a 14 años: ".$enf10_514."<br />";
			}		
			if($_POST["enf10_1519"] != "0") { 
				$enf10_1519 = $_POST["enf10_1519"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf10', 4, $enf10_1519, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf10 - 15 a 19 años: ".$enf10_1519."<br />";
			}		
			if($_POST["enf10_2039"] != "0") { 
				$enf10_2039 = $_POST["enf10_2039"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf10', 5, $enf10_2039, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf10 - 20 a 39 años: ".$enf10_2039."<br />";
			}		
			if($_POST["enf10_4064"] != "0") { 
				$enf10_4064 = $_POST["enf10_4064"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf10', 6, $enf10_4064, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf10 - 40 a 64 años: ".$enf10_4064."<br />";
			}		
			if($_POST["enf10_65"] != "0") { 
				$enf10_65 = $_POST["enf10_65"]; 
				$sql = "INSERT INTO partediario.parte_detalle (\"fk_parte\", \"fk_enfermedad\", \"fk_grupoetario\", \"cantidad\", \"tipo\") 
						VALUES ($idparte, '$enf10', 7, $enf10_65, 'I');";
				$iddetalle = $pg->insert($sql, "iddet");
				$mensaje.= "Enfermedad $enf10 - Mas de 65 años: ".$enf10_65."<br />";
			}		
		}
	}

		

	$mensaje.= "<br />";


	/*
	echo "<pre>";
	print_r($_POST);
	echo "</pre>";
	*/
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Ministerio de Salud - Parte Diario de Consultas e Internación</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link type="text/css" rel="stylesheet" href="css/custom-bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/patron.css">
    <link type="text/css" rel="stylesheet" href="css/patron-iconos.css">
    <link rel="stylesheet" type="text/css" href="css/salud/estilo_salud.css" />
    <link href="bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Parte Diario de Consultas e Internación - Ministerio de Salud de San Juan">
    <meta name="keywords" content="salud, parte diario, consultas, internaciones, gobierno, san juan">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js" type="text/javascript" charset="utf-8" async defer></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" 
    	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    <!-- select2 -->
    <link href="select2/css/select2.min.css" rel="stylesheet" />
    <link href="select2/css/select2-bootstrap4.min.css" rel="stylesheet" />
	<script src="select2/js/select2.min.js"></script>
	<script src="select2/js/i18n/es.js"></script>

	<style type="text/css" media="screen">
		.select2-container{
			width: 100%!important;
		}
		.select2-search--dropdown .select2-search__field {
			width: 98%;
		}
		.icant {
			text-align: center;
			font-weight: bold;
			font-size:14px;
		}
	</style>
</head>

<body>

	<header>
	    <nav class="navbar navbar-light navbar-expand-md fixed-top">
	        <div class="container">
	            <a href="http://www.sanjuan.gob.ar" class="navbar-brand padre-img1">
	                <img src="img/logo.png" alt="Isologo Gobierno de San Juan" width="150">
	            </a>
	            <a href="http://salud.sanjuan.gob.ar" class="navbar-brand padre-img2">
	                <img src="img/logosalud.png" alt="Ministerio de Salud" width="150" class="img2">
	            </a>
	            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#menu-principal"
	                aria-controls="menu-principal" aria-expanded="false" aria-label="Desplegar menú de navegación">
	                <span class="navbar-toggler-icon"></span>
	            </button>
	            <div class="collapse navbar-collapse" id="menu-principal">
	                <ul class="navbar-nav ml-auto">  
	                    <!--<li class="nav-item"><a href="#" class="nav-link active">Inicio</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 1</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 2</a></li>
	                    <li class="nav-item"><a href="#" class="nav-link">Link 3</a></li>
	                    -->
	                </ul>
	            </div>
	        </div>
	    </nav>
	</header>
	<main role="main" style="display: block;width: 100%;">
	    <section class="jumbotron jumbotron-home text-light text-center" 
	    		style="background-image:linear-gradient(rgba(89,19,28,0.9),rgba(0,0,0,0)),url('img/doctor.jpg')">
	        <div class="container">
	            <nav class="row">
	                <div class="col-12 col-md-8 mx-auto"><!--col-md-8 m-auto-->
	                    <h3 class="pb-2">Parte diario de Consultas e Internación</h3>
	                </div>
	            </nav>
	        </div>
	    </section>
	</main>    

    <div class="container-fluid" id="sy-contenido">

        <section class="container">

        	<br>
			<h4 class="text-center">Datos del Parte Cargados</h4>
			<p class="mb-2">&nbsp;</p>

			<div class="row mb-4">
				<div class="col-2">
					&nbsp;				  	
				</div>
				<div class="col-8">					
					<a href="Javascript:void(0);" rel="btn" onclick="location.href='https://covid19salud.sanjuan.gob.ar';" 
					class="btn btn-block btn-primary">Cargar Datos de Otro Parte</a>
				</div>
				<div class="col-2">
					&nbsp;
				</div>
			</div>
			
			<div class="row">
				<div class="col-2">
					&nbsp;				  	
				</div>
				<div class="col-8">
					<div class="alert alert-info my-0 mx-0 py-4 px-4 text-center" style="font-size:14px;" role="alert">				
						<strong><?php echo $mensaje; ?></strong>
					</div>
				</div>
				<div class="col-2">
					&nbsp;				  	
				</div>
			</div>			

			<div class="mt-5">
				<?php 
					/*
					echo "<pre>";
					print_r($_POST);
					echo "</pre>";
					*/
				?>				
			</div>

		</section>

    </div>

    <footer class="container-fluid bg-primary py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md">
                    <p>
                        <a href="http://salud.sanjuan.gob.ar" target="_self" title="Ministerio de Salud">
                            <img src="img/logosalud.png" alt="Gobierno de San Juan" width="100%">
                        </a>
                    </p>
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <ul class="list-unstyled text-small">
                                    <li class="titulosclaros corregir p-0">
                                    	Dirección: – Av. Libertador Gral. San Martín 750 Oeste - Capital.
                                	</li>
                                    <li class="titulosclaros corregir p-0">
                                    	Centro Cívico, 3° Piso, Núcleo 1.
                                    </li>
                                    <li class="titulosclaros corregir p-0 w3-padding-top">
                                    	<i class="fa fa-phone fa-1x"></i> Conmutador: 
                                    	<a href="tel:2644305000">264 430 5000</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-phone fa-1x"></i> Teléfono: 
                                        <a href="tel:2644306125">264 430 7290</a>.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md">
                    <h5>Teléfonos Útiles</h5>
                    <ul class="list-unstyled text-small">
                        <li>
                            <a href="tel:911">
                                &nbsp;<i class="fa fa-phone fa-1x"></i> <strong>911</strong>&nbsp;
                                Emergencias
                            </a>
                        </li>
                        <li>
                            <a href="tel:101">
                                &nbsp;<i class="fa fa-phone fa-1x"></i> <strong>101</strong>&nbsp;
                                Policía
                            </a>
                        </li>
                        <li>
                            <a href="tel:107">
                                &nbsp;<i class="fa fa-phone fa-1x"></i> <strong>107</strong>&nbsp;
                                Emergencias Médicas
                            </a>
                        </li>
                        <a href="tel:100">
                            &nbsp;<i class="fa fa-phone fa-1x"></i> <strong>100</strong>&nbsp;
                            Bomberos
                        </a>
                    </ul>
                </div>
                <div class="col-12 col-md">
                    <h5>Trámites y servicios</h5>
                    <ul class="list-unstyled text-small">
                        <li><a href="https://webmail.sanjuan.gov.ar/owa">Acceso a Webmail </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- ARCHIVOS BOOTSTRAP JAVASCRIPT -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>

    <script type="text/javascript">
    	jQuery(document).ready(function($) {
    		
    	});
    </script>
</body>
</html>
